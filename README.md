# Taluva

In Taluva, players attempt to successfully settle a South Sea island slowly 
emerging from the ocean waters as volcano after volcano erupts.

Each turn, players decide to either have a new volcano erupt along the shore, 
increasing the size of the island, or to have an existing volcano erupt again, 
increasing the height of the land around it (and possibly destroying parts of 
existing settlements). They do this by placing a new tile, consisting of one 
volcano and two other types of landscape. A tile must always touch at least one 
other tile, when placed at sea level, or be placed on top of at least two other 
tiles (without any gaps under the land being created), with the volcano being 
placed on top of an existing volcano.

Next, the player will place one or more wooden buildings; huts, temples or 
towers. Settlements must always start at the lowest level, by placing a single 
hut. From there on, existing settlements may expand by placing huts on all hexes 
of a single type of terrain around the settlement, with temples once the 
settlement takes up at least three hexes, or with towers, placed at level 
three or above.

The game ends when all tiles have been placed. At that point, the player who's 
placed most temples wins. Ties are broken by towers, then huts. Ultimate victory - 
and an immediate end to the game - waits for the player who manages to place all 
their buildings of two types. Immediate defeat is also possible, 
when no buildings can legally be played during a player's turn.

A lot of strategy results from the various placement rules. Volcanoes may never 
fully destroy a settlement, so single huts can block volcano placement, 
protecting other settlements. Alternatively, a well placed volcano can split a 
large settlement in two, creating the opportunity for both to expand more 
rapidly than a new settlement would. Limiting your opponent's growth potential 
is at least as important as preparing the terrain for you to expand upon...

# Notes
- F11 and F12 performs undo and redo
- User configuration is stored under $(HOME)/newSettings.cfg
- Savefiles are stored under $(HOME)/taluva-saves/
