
package taluva;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.paint.Color;

public class Configuration {
	static Configuration instance = null;
	Properties prop;
	Logger logger;

	public Properties getProp() {
		return prop;
	}

	public static Configuration instance() {
		if (instance == null)
			instance = new Configuration();
		return instance;
	}
	
	public static InputStream charge(String nom) {
		return ClassLoader.getSystemClassLoader().getResourceAsStream(nom);
	}

	public static void chargerProprietes(Properties p, InputStream in, String nom) {
		try {
			p.load(in);
		} catch (IOException e) {
			System.err.println("Impossible de charger " + nom);
			System.err.println(e.toString());
			System.exit(1);
		}
	}
	public OutputStream getOutputStream(String nom) {
		File file = new File(nom);
		try {
			return new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	public void changeProperty(String key, String value) {
		OutputStream def = getOutputStream("defaut.cfg");
		OutputStream newSettings =getOutputStream(System.getProperty("user.home")+"/newSettings.cfg");
		OutputStream out;
		if(newSettings == null) {
			out = def;
			}
		else {
			out = newSettings;
			}
		
		prop.setProperty(key, value);
		try {
			prop.store(out, "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected Configuration() {
		// On charge les propriétés
		InputStream def = charge("defaut.cfg");
		InputStream newSettings= null;
		try {
			newSettings = new FileInputStream(System.getProperty("user.home")+"/newSettings.cfg");
		} catch (FileNotFoundException e1) {
		}
		InputStream in;
		if(newSettings == null) {
			in = def;
			}
		else {
			in = newSettings;
			}
		Properties defaut = new Properties();
		chargerProprietes(defaut, in, "defaut.cfg");
		// Il faut attendre le dernier moment pour utiliser le logger
		// car celui-ci s'initialise avec les propriétés
		String nom = System.getProperty("user.home") + "/.sokoban";
		try {
			in = new FileInputStream(nom);
			prop = new Properties(defaut);
			chargerProprietes(prop, in, nom);
		} catch (FileNotFoundException e) {
			prop = defaut;
		}
	}

	public String lis(String nom) {
		String value = prop.getProperty(nom);
		if (value != null) {
			return value;
		} else {
			throw new NoSuchElementException("Propriété " + nom + " manquante");
		}
	}
	
	public Color lisCouleur(String nom) {
		String value = prop.getProperty("color_" + nom);
		if (value != null) {
			String[] channels = value.split(",");
			
			return Color.rgb(
					Integer.parseInt(channels[0]),
					Integer.parseInt(channels[1]),
					Integer.parseInt(channels[2]));
		} else {
			throw new NoSuchElementException("Propriété color_" + nom + " manquante");
		}
	}

	public Logger logger() {
		if (logger == null) {
			System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s : %5$s%n");
			logger = Logger.getLogger("Sokoban.Logger");
			logger.setLevel(Level.parse(lis("LogLevel")));
		}
		return logger;
	}
}
