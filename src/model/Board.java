package model;

import java.awt.Point;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import model.Building.BuildingType;
import model.Ground.GroundType;
import pattern.Observable;
import view.InterfaceGraphique;

/**
 * Board is a board game composed of an array of tiles.
 * A board can add a piece of tiles to a position inside the array
 * 
 * @author Loïc Grabas, Louis Boulanger, Aurélien Chosson
 */
	
public class Board extends Observable implements Serializable,Cloneable {
	/**
	 * Serial number for serialization
	 */
	private static final long serialVersionUID = 5928498505613404603L;

	public Hashtable<CubePoint, Tile> tiles;
	public Hashtable<CubePoint, PiecePoint> pieces;
	public boolean isEmpty;
	public ArrayList<City> cities;

	// Used in the view to draw piece by piece
	public List<PiecePoint> drawablePieces = new ArrayList<PiecePoint>();

	/**
	 * Describes all the reasons the placement of a {@link PiecePoint} could fail.
	 * @author Loïc Grabas
	 *
	 */
	public static enum ReasonPiece{
		/**
		 * The piece can be placed
		 */
		OK,

		/**
		 * The coordinates are malformed and do not respect 
		 * vicinity and mathematical constraints.
		 */
		POINT_MALFORMED,

		/**
		 * The hexagons heights under the piece are of different height.
		 */
		WRONG_HEIGHT,

		/**
		 * The volcano is placed directly over a non-volcano hexagon.
		 */
		VOLCANO_WRONG_PLACE,

		/**
		 * The volcano is placed in the same orientation as the underlying volcano
		 */
		VOLCANO_WRONG_DIRECTION,

		/**
		 * The piece is not adjacent to any other.
		 */
		NOT_ADJACENT,

		/**
		 * The piece would destroy a tower or a temple.
		 */
		OVER_PROTECTED_BUILDING,
		/**
		 * The piece would destroy an entire {@link City}.
		 */
		DESTROY_CITY,
		TESTIA
	}

	/**
	 * Enumerates all the reasons why placing a building would fail
	 * @author Loïc Grabas
	 *
	 */
	public static enum ReasonBuilding{

		/**
		 * The building can be placed
		 */
		OK,

		/**
		 * The hexagon already has a building
		 */
		HAS_BUILDING,

		/**
		 * The hexagon is a volcano.
		 */
		HAS_VOLCANO,

		/**
		 * A new colony is placed directly next to another of the same color.
		 */
		NOT_ISOLATED,

		/**
		 * The hexagon doesn't exist.
		 */
		TILE_DOESNT_EXIST,

		/**
		 * The hexagon is not high enough to place a tower.
		 */
		WRONG_HEIGHT,

		/**
		 * The hexagon is not neighbouring a city.
		 */
		NEXT_TO_NO_CITY,

		/**
		 * The city is too small to place a temple.
		 */
		WRONG_CITY_SIZE,

		/**
		 * The city already has a tower.
		 */
		HAS_TOWER,

		/**
		 * The city already has a temple.
		 */
		HAS_TEMPLE
	}

	public Board() {
		tiles = new Hashtable<CubePoint, Tile>();
		pieces = new Hashtable<CubePoint, PiecePoint>();
		cities = new ArrayList<City>();
		isEmpty = true;
	}	

	/**
	 * Adds a tile to the array of tiles without verification
	 * @param t the tile to add to the board
	 * @param position the cubepoint of the position to place inside the board
	 */
	public void addTile(Tile t,CubePoint position) {
		tiles.put(position,t);
	}
	
	public void addPieceToTable(PiecePoint p,CubePoint position) {
		pieces.put(position,p);
	}

	/**
	 * Returns an iterator over tiles, sorted by their height (lower is prioritized).
	 * @return A new iterator over tiles.
	 */
	public Iterator<Tile> getIterator(){
		List<Tile> tmp = new ArrayList<Tile>(tiles.values());
		Collections.sort(tmp);
		Iterator<Tile> it = tmp.iterator();
		return it;
	}

	/**
	 * Returns an iterator over the pieces stored in the board.
	 * @return A new iterator over the pieces.
	 */
	public Iterator<PiecePoint> getPieceIterator() {
		Collections.sort(drawablePieces);
		return drawablePieces.iterator();
	}

	/**
	 * Returns an iterator over the {@link CubePoint}s of the tiles.
	 * @return A new iterator
	 */
	public Iterator<CubePoint> getIteratorPoints(){
		List<CubePoint> tmp = Collections.list(tiles.keys());
		Iterator<CubePoint> it = tmp.iterator();
		return it;
	}

	/**
	 * Checks if the piece point has an adjacent tile from the board
	 * @param point the piece point to check
	 * @return boolean if the piece point has an adjacent tile from the board
	 */
	private boolean hasAdjacentTile(PiecePoint point) {
		for (CubePoint cubePoint : point.points) {
			for (CubePoint side : cubePoint.getNeighbours()) {
				Tile t = getTile(side);
				if(t!=null) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets tile from the board
	 * @param position the {@link CubePoint} of the position to get inside the board
	 * @return tile of the given position
	 */
	public Tile getTile(CubePoint position){
		return tiles.get(position);
	}

	/**
	 * Checks if a {@link PiecePoint} can be placed 
	 * @param point the {@link PiecePoint} to place the piece
	 */
	public void reset() {
		tiles = new Hashtable<CubePoint, Tile>();
		isEmpty = true;
		update();
	}
	
	public City getCityFromCubePoint(CubePoint point) {
		for (City c : cities) {
			for (BuildingPoint bp : c.buildingPoints) {
				if (bp.point.equals(point)) {
					return c;
				}
			}
		}
		return null;
	}
	
	public void generateTransient() {

	}
	
	/**
	 * Checks if a piece can be placed 
	 * @param piece the piece to place
	 * @param point the piece point (location) to place the piece
	 * @return boolean that says if the piece is placeable
	 */
	public ReasonPiece canInsertPiece(PiecePoint point){
		if(!point.checkPoints()) {
			// point malformed
			return ReasonPiece.POINT_MALFORMED;
		}

		// Tiles on the board
		Tile[] tiles = new Tile[] {
				getTile(point.points[0]),
				getTile(point.points[1]),
				getTile(point.points[2])
		};

		// Is the space under the piece empty ?
		boolean emptyPlace = (
				tiles[0]==null &&
				tiles[1]==null &&
				tiles[2]==null
				);

		// Is the space under the piece full ?
		boolean fullPlace = (
				tiles[0]!=null &&
				tiles[1]!=null &&
				tiles[2]!=null
				);

		if(fullPlace) {
			//check that the tiles are at the same height
			int tileHeight = tiles[0].getHeight();
			for (Tile tile : tiles) {
				if(tileHeight != tile.getHeight()) {
					// wrong height
					return ReasonPiece.WRONG_HEIGHT;
				}
			}
			// check that there is a volcano
			boolean isVolcano= (tiles[1].getGround().getType() == GroundType.VOLCANO);
			if(!isVolcano) {
				// volcano in wrong place
				return ReasonPiece.VOLCANO_WRONG_PLACE;
			}

			// Check the volcation orientation
			PiecePoint boardPiece = pieces.get(point.points[1]);
			boolean volcanoDirection = (point.getDirection() != boardPiece.getDirection());
			if(!volcanoDirection) {
				// volcano wrong direction
				return ReasonPiece.VOLCANO_WRONG_DIRECTION;
			}else {
				for (int i = 0; i < 3; i++) {
					Tile tile = tiles[i];
					if(tile.hasBuilding()) {
						BuildingPoint bp = new BuildingPoint(tile.getBuilding(),point.points[i]);
						City c = getCityFromBuildingPoint(bp);

						// Check if the piece would destroy an entire city
						if(c.getBuildingPoints().size()<=1) {
							return ReasonPiece.DESTROY_CITY;
						} else if(i==0 && c.getBuildingPoints().size()==2 && tiles[2].hasBuilding()){
							BuildingPoint bp2 = new BuildingPoint(tiles[2].getBuilding(),point.points[2]);
							City c2 = getCityFromBuildingPoint(bp2);
							if(c == c2) {
								return ReasonPiece.DESTROY_CITY;
							}
						}

						// Check if the piece would destroy a temple or a tower
						if(tile.getBuilding().getType() == BuildingType.TEMPLE || tile.getBuilding().getType() == BuildingType.TOWER) {
							return ReasonPiece.OVER_PROTECTED_BUILDING;
						}
					}
				}
	    		 return ReasonPiece.OK;
	    	 }
	     }else if(emptyPlace){
	    	 if(!isEmpty) {
	    		 // is adjacent or not
	    		 if(!hasAdjacentTile(point)) {
	    			 return ReasonPiece.NOT_ADJACENT;
	    		 }else {
	    			 return ReasonPiece.OK;
	    		 }
	    	 }else {
	    		 return ReasonPiece.OK;
	    	 }
	     }
	     return ReasonPiece.VOLCANO_WRONG_PLACE;
	}

	/**
	 * Adds a {@link PiecePoint} to the board if it is possible
	 * @param point the {@link PiecePoint} that determines where to place the piece
	 * @return boolean returns true if the piece has been added false otherwise
	 */
	public ReasonPiece addPiece(PiecePoint point) {
		ReasonPiece result = canInsertPiece(point);
		if(result == ReasonPiece.OK) {
			for (int i = 0; i < 3; i++){
				CubePoint cubePoint = point.points[i];
				Tile tile = getTile(cubePoint);
				if(tile == null) {
					addTile(point.tiles[i], cubePoint);
					Point offsetCoords = cubePoint.toAxial();
					point.tiles[i].setX((int) offsetCoords.getX());
					point.tiles[i].setY((int) offsetCoords.getY());
				} else {
					if(tile.hasBuilding()) {
						//delete buildings when replacing
						BuildingPoint bp = new BuildingPoint(tile.getBuilding(),cubePoint);
						City c = getCityFromBuildingPoint(bp);
						for (Iterator<BuildingPoint> iterator = c.getBuildingPoints().iterator(); iterator.hasNext(); ) {
							BuildingPoint cityElement = iterator.next();
							if(cityElement.equals(bp)) {
								iterator.remove();
							}
						}						
					}
					tile.replaceGround(point.getGround(i), point.tiles[i].getRotate());
					point.tiles[i] = tile;
				}
			}

			// Explode cities that have been destroyed by the piece
			ArrayList<City> newCities = new ArrayList<City>();
			for (City city : cities) {
				while(city.getBuildingPoints().size() > 0) {
					City newCity = new City();
					exploreCity(city, city.getBuildingPoints().get(0), newCity);
					newCities.add(newCity);
				}
			}

			cities.clear();
			cities.addAll(newCities);
			//add piece to pieces
			pieces.put(point.points[1], point);
			drawablePieces.add(point);

			if(isEmpty) {
				isEmpty = false;
			}
			return ReasonPiece.OK;
		}
		return result;
	}

	/**
	 * Explores a {@link City} and adds the found {@link BuildingPoint}s to a new one, used to
	 * explode a city when a piece separates it into multiple ones.
	 * @param city The {@link City} to explore
	 * @param startingPoint The {@link PiecePoint} from which to start the exploration
	 * @param newCity The new {@link City} in which the found {@link BuildingPoint}s are to be put.
	 */
	private void exploreCity(City city, BuildingPoint startingPoint, City newCity) {
		if (startingPoint.getType() == BuildingType.TEMPLE) {
			newCity.addTemple(startingPoint);
		} else if (startingPoint.getType() == BuildingType.TOWER) {
			newCity.addTower(startingPoint);
		} else {
			newCity.addHutt(startingPoint);
		}
		city.getBuildingPoints().remove(startingPoint);
		List<CubePoint> neighbours = startingPoint.point.getNeighbours();
		for (CubePoint neighbour : neighbours) {
			if (city.hasPoint(neighbour)) {
				exploreCity(city, city.getBuildingPoint(neighbour), newCity);
			}
		}
	}

	/**
	 * Returns all cities belonging to a {@link Color}.
	 * @param color The {@link Color} to match against
	 * @return An {@link ArrayList} containing each {@link City} of the specified color.
	 */
	public ArrayList<City> getCitiesOfColor(Color color){
		ArrayList<City> result = new ArrayList<City>();
		for (City city : cities) {
			if(city.getBuildingPoints().get(0).getColor().equals(color)) {
				result.add(city);
			}
		}
		return result;
	}

	/**
	 * Checks if the given building is a hutt and is placeable as one hutt
	 * @param buildingPoint The building to check
	 * @return a {@link ReasonBuilding} describing the result of the check.
	 */
	public ReasonBuilding canPlaceHutt(BuildingPoint buildingPoint) {
		Tile tile = getTile(buildingPoint.point);
		if (tile.getGround().getType() == GroundType.VOLCANO) {
			return ReasonBuilding.HAS_VOLCANO;
		}

		if (tile.hasBuilding()) {
			return ReasonBuilding.HAS_BUILDING;
		}

		if(tile.getHeight() > 1) {
			return ReasonBuilding.WRONG_HEIGHT;
		}

		for (CubePoint side : buildingPoint.point.getNeighbours()) {
			Tile candidate = getTile(side);

			if (candidate != null && candidate.hasBuilding()) {
				if(candidate.getBuilding().getColor().equals(buildingPoint.getColor()))
					return ReasonBuilding.NOT_ISOLATED;
			}
		}
		return ReasonBuilding.OK;
	} 

	/*
	 * Checks if the giving building can extend a city
	 * @param buildingPoint the {@link BuildingPoint} to check
	 * @return a {@link City} if the point can extend one, {@code null} otherwise.
	 */
	public City canExtendCity(BuildingPoint buildingPoint) {		
		BuildingPoint p_current = new BuildingPoint(
				new Building(buildingPoint.getType(), buildingPoint.getColor(),buildingPoint.getColorName()), 
				new CubePoint());
		Tile t_current = getTile(buildingPoint.point);

		//deja contruit/volcan ?
		if(t_current.hasBuilding() || t_current.getGround().getType() == GroundType.VOLCANO) {			
			return null;
		}

		for (CubePoint p : buildingPoint.point.getNeighbours()) {
			p_current.point.setCoordinates(
					p.getX(), 
					p.getY(), 
					p.getZ());
			for(City c : getCitiesOfColor(buildingPoint.getColor())) {
				if(c.buildingPoints.contains(p_current))
					return c;
			}
		}
		//si aucune ville allié a proximité : impossible
		return null;
	}

	/**
	 * Counts the number of hutts it would take to extend a {@link City} on a specified {@link BuildingPoint}.
	 * @param buildingPoint the {@link BuildingPoint} to check from
	 * @param city the {@link City} to extend
	 * @return the number of hutts it takes to extend the city.
	 */
	public int countExtendCityHutts(BuildingPoint buildingPoint, City city) {
		List<BuildingPoint> points = getExtendPointFromCityAndTile(city,buildingPoint);
		int totalHutts = 0;
		if(points.size() > 0) {

			for (BuildingPoint point : points) {
				Tile tile = getTile(point.point);
				if(tile != null) {
					totalHutts += tile.getHeight();
				}
			}
		}
		return totalHutts;

	}
	/**
	 * Returns a {@link City} given a {@link BuildingPoint}.
	 * @param buildingPoint the {@link BuildingPoint} to check
	 * @return A {@link City} if the {@link BuildingPoint} corresponds to one, {@code null} otherwise.
	 */
	public City getCityFromBuildingPoint(BuildingPoint buildingPoint) {
		for(City c : getCitiesOfColor(buildingPoint.getColor())) {
			if(c.buildingPoints.contains(buildingPoint)) 
				return c;
		}
		return null;
	}

	/**
	 * Returns the {@link BuildingPoint}s neighbouring a {@link City} matching the {@link Ground}
	 * of a specified {@link BuildingPoint}. 
	 * @param city A {@link City}
	 * @param buildingPoint The target {@link BuildingPoint}
	 * @return A list of {@link BuildingPoint}s neighbouring the city matching the {@link Ground}
	 * of {@code buildingPoint}.
	 */
	public List<BuildingPoint> getExtendPointFromCityAndTile(City city, BuildingPoint buildingPoint){
		List<BuildingPoint> result = new ArrayList<BuildingPoint>();
		Tile buildingTile = getTile(buildingPoint.point);

		if (buildingTile == null) {
			return result;
		}

		if (buildingTile.getGround().getType() == GroundType.VOLCANO) {
			return result;
		}

		for (BuildingPoint cityBuildingPoint : city.buildingPoints) {
			CubePoint point = new CubePoint();

			for (CubePoint neighbouringPoint : cityBuildingPoint.point.getNeighbours()) {
				point = neighbouringPoint;

				Tile tile = getTile(point);

				if (tile != null 
						&&  tile.getGround().getType() == buildingTile.getGround().getType()
						&& !tile.hasBuilding()) {
					CubePoint newPoint = new CubePoint(point);
					BuildingPoint newBuildingPoint = 
							new BuildingPoint(new Building(BuildingType.HUTT, buildingPoint.getColor(),buildingPoint.getColorName()), newPoint);
					if (!result.contains(newBuildingPoint)) {
						result.add(newBuildingPoint);
					}
				}
			}
		}

		return result;
	}


	/**
	 * Checks if a tower can be placed
	 * @param buildingPoint The {@link BuildingPoint} to check
	 * @return A {@link ReasonBuilding} describing the result of the check.
	 */
	public ReasonBuilding canPlaceTower(BuildingPoint buildingPoint) {
		Tile t = getTile(buildingPoint.point);
		if (t.getGround().getType() == GroundType.VOLCANO) {
			return ReasonBuilding.HAS_VOLCANO;
		}

		if (t.hasBuilding()) {
			return ReasonBuilding.HAS_BUILDING;
		}

		ArrayList<City> cities = new ArrayList<City>();
		BuildingPoint p_current =  new BuildingPoint(buildingPoint,new CubePoint());
		for (CubePoint p : buildingPoint.point.getNeighbours()) {
			p_current.point = p;
			for(City c : getCitiesOfColor(buildingPoint.getColor())) {
				if(c.buildingPoints.contains(p_current) && !cities.contains(c)) 
					cities.add(c);
			}
		}

		ReasonBuilding result = ReasonBuilding.TILE_DOESNT_EXIST;
		for(City c : cities) {
			if(c.hasTower) {
				result =  ReasonBuilding.HAS_TOWER;
			}else {
				if(t.getHeight() < 3) {
					result =  ReasonBuilding.WRONG_HEIGHT;
				}else {
					return ReasonBuilding.OK;
				}
			}
		}
		if(cities.size() == 0) {
			return ReasonBuilding.NEXT_TO_NO_CITY;
		}
		return result;
	}

	/**
	 * Checks if a temple can be placed
	 * @param buildingPoint The {@link BuildingPoint} to check
	 * @return A {@link ReasonBuilding} describing the result of the check
	 */
	public ReasonBuilding canPlaceTemple(BuildingPoint buildingPoint) {
		Tile t = getTile(buildingPoint.point);
		if (t.getGround().getType() == GroundType.VOLCANO) {
			return ReasonBuilding.HAS_VOLCANO;
		}

		if (t.hasBuilding()) {
			return ReasonBuilding.HAS_BUILDING;
		}

		ArrayList<City> cities = new ArrayList<City>();
		BuildingPoint p_current =  new BuildingPoint(buildingPoint,new CubePoint());
		for (CubePoint p : buildingPoint.point.getNeighbours()) {
			p_current.point = p;
			for(City c : getCitiesOfColor(buildingPoint.getColor())) {
				if(c.buildingPoints.contains(p_current) && !cities.contains(c)) 
					cities.add(c);
			}
		}

		for(City c : cities) {
			if(c.buildingPoints.size()>=3 && !c.hasTemple)
				return ReasonBuilding.OK;
		}
		return ReasonBuilding.WRONG_CITY_SIZE;
	}

	/**
	 * Adds a building without rules
	 * @param buildingPoint the building to add
	 * @param city the {@link City} to which to add the building
	 * @return A {@link ReasonBuilding} describing the outcome of the action.
	 */
	public ReasonBuilding addBuilding(BuildingPoint buildingPoint, City city) {
		ReasonBuilding result = ReasonBuilding.TILE_DOESNT_EXIST;

		Tile t = getTile(buildingPoint.point);
		if(t == null) 
			return result;

		switch (buildingPoint.getType()) {
		case HUTT:
			// Check if the player wants to extend a city
			if(city != null && canExtendCity(buildingPoint) != null) {
				List<BuildingPoint> extendPoints = getExtendPointFromCityAndTile(city, buildingPoint);
				for(BuildingPoint bp : extendPoints) {
					t = getTile(bp.point);
					t.setBuilding(bp);
					city.addHutt(bp);
					mergeCity(city, bp);
				}
				return ReasonBuilding.OK;
				// Check if the player creates a new colony
			} else if((result = canPlaceHutt(buildingPoint)) == ReasonBuilding.OK){
				t.setBuilding(buildingPoint);
				cities.add(new City(buildingPoint));
				return result;
			}		
			break;

		case TEMPLE:
			if((result = canPlaceTemple(buildingPoint)) == ReasonBuilding.OK) {
				BuildingPoint bp = new BuildingPoint(buildingPoint,new CubePoint());

				for (CubePoint side : buildingPoint.point.getNeighbours()) {
					bp.point = side;
					City otherCity = getCityFromBuildingPoint(bp);

					if(otherCity != null && !otherCity.hasTemple) {
						otherCity.addTemple(buildingPoint);
						t.setBuilding(buildingPoint);
						mergeCity(otherCity, buildingPoint);

						// Play sound
						String musicFile = "music/templeSound.mp3";

						if (InterfaceGraphique.playSounds && InterfaceGraphique.soundAvailable) {									
							Media sound = new Media(getClass().getClassLoader().getResource(musicFile).toString());
							InterfaceGraphique.effects = new MediaPlayer(sound);
							InterfaceGraphique.effects.play();
						}

						break;
					}

				}
				return ReasonBuilding.OK;
			}	
			break;

		case TOWER:
			if((result = canPlaceTower(buildingPoint))== ReasonBuilding.OK) {
				BuildingPoint bp = new BuildingPoint(buildingPoint,new CubePoint());

				for (CubePoint side : buildingPoint.point.getNeighbours()) {
					bp.point = side;
					City otherCity = getCityFromBuildingPoint(bp);

					if(otherCity != null && !otherCity.hasTower) {
						t.setBuilding(buildingPoint);
						otherCity.addTower(buildingPoint);
						mergeCity(otherCity, buildingPoint);
						break;
					}
				}
				return ReasonBuilding.OK;
			}
			break;
		}
		return result;
	}

	/**
	 * Returns the {@link CubePoint}s surrounding a city.
	 * @param c The {@link City} to check
	 * @return A {@link List} of {@link CubePoint}s surrounding the city.
	 */
	public List<CubePoint> getPointsAroundCity(City c){
		List<CubePoint> points = new ArrayList<CubePoint>();
		for (BuildingPoint cityPoint : c.getBuildingPoints()) {
			for (CubePoint side : cityPoint.point.getNeighbours()) {
				Tile t = getTile(side);
				if(t != null && !t.hasBuilding()) {
					points.add(side);
				}
			}
		}
		return points;
	}

	/**
	 * Merges a {@link City} with the cities around a specified {@link BuildingPoint}
	 * @param city The {@link City to merge}
	 * @param buildingPoint The {@link BuildingPoint} with surrounding cities
	 * @return the new, merged {@link City}
	 */
	private City mergeCity(City city, BuildingPoint buildingPoint) {
		CubePoint current = new CubePoint();
		BuildingPoint bp = new BuildingPoint(buildingPoint,current);
		for (CubePoint side : buildingPoint.point.getNeighbours()) {
			bp.point = side;
			
			//get city if not exists and not the same as c then ok
			City otherCity = getCityFromBuildingPoint(bp);
			if(otherCity != null && city != otherCity) {
				city.buildingPoints.addAll(otherCity.buildingPoints);
				if(city.hasTemple || otherCity.hasTemple) {
					city.setTemple(true);
				}
				if(city.hasTower || otherCity.hasTower) {
					city.setTower(true);
				}
				cities.remove(otherCity);
			}
		}
		return city;
	}
		
	public Board getConfiguration() {
		Board b = new Board();
		b.isEmpty = isEmpty;
		//copy cities
		for (City city : cities) {
			City c = new City();
			for (BuildingPoint bp : city.buildingPoints) {
				c.buildingPoints.add(new BuildingPoint(bp, bp.point));
			}
			c.hasTemple=city.hasTemple;
			c.hasTower=city.hasTower;
			b.cities.add(c);
		}
	
		Iterator<CubePoint> it = getIteratorPoints();
		while(it.hasNext()) {
			CubePoint p = it.next();
			Tile t = getTile(p);
			Tile newTile = new Tile(t.getGround(),t.getX(),t.getY(),t.height);
			//newTile.height = t.height;
			if (t.getBuilding()!=null)
				newTile.setBuilding(new Building(t.getBuilding().getType(),t.getBuilding().getColor(),t.getBuilding().getColorName()));
			b.addTile(newTile, p);
		}
		
		it = getIteratorPoints();
		while(it.hasNext()) {
			CubePoint p = it.next();
			PiecePoint pp = pieces.get(p);
			if(pp != null) {
				PiecePoint newPiece = new PiecePoint();
				newPiece.setPoints(pp.points);
				for (int i = 0; i < pp.tiles.length; i++) {
					CubePoint piecePoint = pp.points[i];
					Tile newTile = b.getTile(piecePoint);
					newPiece.tiles[i] = newTile;
				}
				b.addPieceToTable(newPiece, p);
			}
		}		
		return b;
	}
}
