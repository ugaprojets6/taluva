package model;

/**
 * Enumerates the different types of feedforwarding on the board
 * @author Louis Boulanger
 *
 */
public enum FeedforwardType {
	/**
	 * No feedforward.
	 */
	NONE,
	
	/**
	 * An error of some kind
	 */
	WRONGPLACEMENT,
	
	/**
	 * Validation of an action
	 */
	CORRECTPLACEMENT,
	
	/**
	 * City selection
	 */
	CITYSELECTED
}
