package model;

import java.io.Serializable;
import java.util.Iterator;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.paint.Color;
import model.Board.ReasonBuilding;
import model.Building.BuildingType;
import taluva.Configuration;

/**
 * Contains a player's buildings
 * @author Louis Boulanger
 */
public class Hand implements Serializable, Cloneable {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -145472906143556863L;
	
	public static final int HUTTS = 20;
	public static final int TEMPLES = 3;
	public static final int TOWERS = 2;
	
	protected transient IntegerProperty hutts;
	protected transient IntegerProperty temples;
	protected transient IntegerProperty towers;
	
	protected int huttsCount;
	protected int towersCount;
	protected int templesCount;
	
	protected transient Color color;
	public Board board;
	protected String colorName;
	
	/**
	 * Constructs a hand from a {@link Color} and its name
	 * @param color the {@link Player}'s {@link Color}
	 * @param colorName the {@link Player}'s color name
	 */
	public Hand(Color color, String colorName) {
		hutts = new SimpleIntegerProperty();
		temples = new SimpleIntegerProperty();
		towers = new SimpleIntegerProperty();
		setBoard(board);
		setHutts(HUTTS);
		setTemples(TEMPLES);
		setTowers(TOWERS);
		this.color = color;
		this.colorName = colorName;
	}
	
	/**
	 * Generates transient properties after deserialization
	 */
	public void generateProperties() {
		hutts = new SimpleIntegerProperty(huttsCount);
		temples = new SimpleIntegerProperty(templesCount);
		towers = new SimpleIntegerProperty(towersCount);
		setColorFromName();
	}
	
	/**
	 * Fetches and sets the color from the color name.
	 */
	public void setColorFromName() {
		Configuration conf = Configuration.instance();
		this.color = conf.lisCouleur(this.colorName);
	}
	
	/**
	 * Checks if the hand has enough buildings to place an extra one
	 * @param buildingPoint the {@link BuildingPoint} to add
	 * @param city the {@link City} to extend
	 * @return
	 */
	public boolean hasEnough(BuildingPoint buildingPoint,City city) {
		switch (buildingPoint.getType()) {
		case HUTT:
			if (city != null) {
				int countExtend = board.countExtendCityHutts(buildingPoint,city);
				if(countExtend > 0) {
					if(countExtend <= getHutts()) {
						return true;
					}
				}
			} else {
				Tile t = board.getTile(buildingPoint.point);
				if(t!=null && t.getHeight() <= getHutts()) {
					return true;
				}
			}
			break;
			
		case TEMPLE:
			if(getTemples() >= 1) {
				return true;
			}
			break;
			
		case TOWER:
			if(getTowers() >= 1) {
				return true;
			}
			break;

		default:
			break;
		}	
		return false;
	}
	
	/**
	 * Removes a certain number of buildings from the player's hand
	 * @param type the {@link BuildingType}
	 * @param number the amount to remove
	 */
	public void deleteBuildings(BuildingType type,int number) {
		switch (type) {
		case HUTT:
			setHutts(getHutts()- number);
			break;
		case TEMPLE:
			setTemples(getTemples()- number);
			break;
		case TOWER:
			setTowers(getTowers()- number);
			break;

		default:
			break;
		}
	}
	
	/**
	 * Checks if the player has lost by not being able to place any buildings
	 * @return {@code true} if the player has lost, {@code false} otherwise.
	 */
	public boolean hasLost() {
		Iterator<CubePoint> it = board.getIteratorPoints();
		while(it.hasNext()) {
			CubePoint p = it.next();
			
			// Checks if you have enough to extend the city
			BuildingPoint huttBp = new BuildingPoint(BuildingType.HUTT, color, p, colorName);
			City c = board.canExtendCity(huttBp);
			boolean extend = c == null ? false : hasEnough(huttBp, c);
			
			// Checks if you have enough to place a hutt
			boolean hutt = board.canPlaceHutt(huttBp) == ReasonBuilding.OK && getHutts() > 0;
			
			// Checks if you have enough to place a tower
			BuildingPoint towerBp = new BuildingPoint(BuildingType.TOWER, color, p, colorName);
			boolean tower = board.canPlaceTower(towerBp) == ReasonBuilding.OK && getTowers() > 0;

			// Checks if you have enough to place a temple
			BuildingPoint templeBp = new BuildingPoint(BuildingType.TEMPLE, color, p, colorName);
			boolean temple = board.canPlaceTemple(templeBp) == ReasonBuilding.OK && getTemples() > 0;
			
			if (extend || hutt || tower || temple) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks if the player has won by exhausting two of its three building types
	 * @return {@code true} if the player has won, {@code false} otherwise.
	 */
	public boolean hasWon() {
		boolean hutts = getHutts() == 0;
		boolean towers = getTowers() == 0;
		boolean temples = getTemples() == 0;
		
		return (hutts && towers) || (towers && temples) || (hutts && temples);
	}
	
	/**
	 * Returns the hand's {@link Color}
	 * @return the hand's {@link Color}
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets the hand's {@link Color} to a new one
	 * @param color the new {@link Color}.
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Returns the number of hutts the player has.
	 * @return the number of hutts the player has.
	 */
	public int getHutts() {
		return hutts.get();
	}
	
	/**
	 * Sets how many hutts the player has.
	 * @param nbHutts the number of hutts.
	 */
	public void setHutts(int nbHutts) {
		hutts.set(nbHutts);
		huttsCount = nbHutts;
	}
	
	/**
	 * Returns the number of temples the player has.
	 * @return the number of temples the player has.
	 */
	public int getTemples() {
		return temples.get();
	}
	
	/**
	 * Sets how many temples the player has.
	 * @param nbHutts the number of temples.
	 */
	public void setTemples(int nbTemple) {
		this.temples.set(nbTemple);
		templesCount = nbTemple;
	}
	
	/**
	 * Returns the number of towers the player has.
	 * @return the number of towers the player has.
	 */
	public int getTowers() {
		return towers.get();
	}
	
	/**
	 * Sets how many towers the player has.
	 * @param nbHutts the number of towers.
	 */
	public void setTowers(int nbTower) {
		this.towers.set(nbTower);
		towersCount = nbTower;
	}
	
	/**
	 * Returns a reference to the board
	 * @return a reference to the board
	 */
	public Board getBoard() {
		return board;
	}
	
	/**
	 * Sets the reference to the {@link Board}
	 * @param board the new reference to the {@link Board}
	 */
	public void setBoard(Board board) {
		this.board = board;
	}
	
	/**
	 * Returns the number of hutts as an {@link IntegerProperty}.
	 * @return the number of hutts as an {@link IntegerProperty}.
	 */
	public IntegerProperty getHuttsProperty() {
		return hutts;
	}
	
	/**
	 * Returns the number of temples as an {@link IntegerProperty}.
	 * @return the number of temples as an {@link IntegerProperty}.
	 */
	public IntegerProperty getTemplesProperty() {
		return temples;
	}
	
	/**
	 * Returns the number of towers as an {@link IntegerProperty}.
	 * @return the number of towers as an {@link IntegerProperty}.
	 */
	public IntegerProperty getTowersProperty() {
		return towers;
	}

	@Override
	public String toString() {
		return "hand: hutts: "+hutts+" temples: "+temples+" towers: "+towers;
	}
	
	
	public Object clone() {
		Hand o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (Hand) super.clone();
			
			
			//o.board= this.board.getConfiguration();
			o.hutts=new SimpleIntegerProperty(this.getHutts());
			o.temples= new SimpleIntegerProperty(this.getTemples());
			o.towers= new  SimpleIntegerProperty(this.getTowers());
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
}
