package model;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.StrokeLineJoin;

/**
 * PiecePoint is the combination of 3 CubePoints in order to identify a
 * piece without the ground information
 * 
 * @author Loïc Grabas
 */
public class PiecePoint extends Piece implements Comparable<PiecePoint>, Serializable , Cloneable{
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -8530634259361927688L;

	public CubePoint[] points;
	
	/**
	 * The coordinates of the rotation direction of a piece
	 */
	public final static CubePoint[][] rotations = new CubePoint[][] {	
		new CubePoint[] {
					new CubePoint(-1,0,1),
					new CubePoint(0,-1,1)
			},
			new CubePoint[] {
					new CubePoint(0,-1,1),
					new CubePoint(1,-1,0)
			},
			new CubePoint[] {
					new CubePoint(1,-1,0),
					new CubePoint(1,0,-1)
			},
			new CubePoint[] {
					new CubePoint(1,0,-1),
					new CubePoint(0,1,-1)
			},
			new CubePoint[] {
					new CubePoint(0,1,-1),
					new CubePoint(-1,1,0)
			},
			new CubePoint[] {
					new CubePoint(-1,1,0),
					new CubePoint(-1,0,1)
			}
		};
	
	/**
	 * Constructs a PiecePoint from a an array of points and a piece
	 * @param points the initial points
	 * @param p the piece paired to the point
	 */
	public PiecePoint(CubePoint[] points,Piece p) {
		setPoints(points);
		setPiece(p);
	}
	
	/**
	 * Constructs a PiecePoint at the point (0,0,0) with a defined Piece
	 * @param p the paired {@link Piece}
	 */
	public PiecePoint(Piece p) {
		this.points = new CubePoint[] {
				rotations[0][0],
				new CubePoint(0,0,0),
				rotations[0][1]
		};
		setPiece(p);
	}
	
	/**
	 * Constructs a PiecePoint from a list of points
	 * @param points the original points of the PiecePoint
	 */
	public PiecePoint(CubePoint[] points) {
		setPoints(points);
	}
	
	/**
	 * Constructs a new PiecePoint
	 */
	public PiecePoint() {
		this.points = new CubePoint[] { 
				new CubePoint(),
				new CubePoint(),
				new CubePoint()
		};
	}

	/**
	 * Updates the points of the PiecePoint according to its volcano tile
	 * @param p the new position of the volcano
	 */
	public void updatePointByVolcano(CubePoint p) {
		int rot = getDirection();
		points[1] = p;

		CubePoint cp0 = new CubePoint(
				points[1].getX()+PiecePoint.rotations[rot][0].getX(),
				points[1].getY()+PiecePoint.rotations[rot][0].getY(),
				points[1].getZ()+PiecePoint.rotations[rot][0].getZ()
				);
		
		points[0] = cp0;

		CubePoint cp2 = new CubePoint(
				points[1].getX()+PiecePoint.rotations[rot][1].getX(),
				points[1].getY()+PiecePoint.rotations[rot][1].getY(),
				points[1].getZ()+PiecePoint.rotations[rot][1].getZ()
				);
		
		points[2] = cp2;
		updateTiles();
	}
	
	/**
	 * Sets the current {@link Piece} to a new one
	 * @param p the new {@link Piece}
	 */
	public void setPiece(Piece p) {
		for(int k=0;k<3;k++)
			this.setGround(p.getGround(k),k);
	}
	
	/**
	 * Returns the current direction, represented by an integer between 0 and 5
	 * @return the current direction
	 */
	public int getDirection() {
		//calculate by comparing to initial top position of the volcano ground
		for (int i=0;i<rotations.length;i++) {
			CubePoint[] rotation = rotations[i];
			int nbdirection = 0;
			//volcano point
			CubePoint p = new CubePoint(points[1]);
			//calculate the position
			p.setCoordinates(
					p.getX() + rotation[0].getX(), 
					p.getY() + rotation[0].getY(), 
					p.getZ() + rotation[0].getZ());
			
			//check if the point corresponds
			if(p.equals(points[0])) {
				nbdirection++;
			}
			p = new CubePoint(points[1]);
			
			p.setCoordinates(
					p.getX() + rotation[1].getX(), 
					p.getY() + rotation[1].getY(), 
					p.getZ() + rotation[1].getZ());

			if(p.equals(points[2])) {
				nbdirection++;
			}
			if(nbdirection==2) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Checks if the combination of the points are correct
	 * (next to each other)
	 * @return boolean true if the points are correct, false otherwise
	 */
	public boolean checkPoints() {
		for (CubePoint cubePoint : points) {
			for (CubePoint next : points) {
				if(!next.equals(cubePoint)) {
					if(!cubePoint.isAdjacent(next)){
						return false;
					}
				}
			}
			
		}
		return true;
	}
	
	/**
	 * Sets the feedforward of all the tiles
	 * @param type the {@link FeedforwardType}.
	 */
	public void setFeedforward(FeedforwardType type) {
		for(Tile t: tiles) {
			t.setFeedforward(type);
		}
	}
	
	/**
	 * Sets the points of the PiecePoint to a new array
	 * @param points the new coordinates
	 */
	public void setPoints(CubePoint[] points) {
		CubePoint[] old = this.points;
		this.points = points;
		if(!checkPoints()) {
			this.points = old;
		}
		updateTiles();
	}
	
	/**
	 * Turns the coordinates of the point in the left direction
	 */
	public void turnPoint() {
		int dir = getDirection();
		if(dir==5) {
			dir = 0;
		}else {
			dir += 1;
		}
		
		points[0].setCoordinates(
				points[1].getX()+rotations[dir][0].getX(), 
				points[1].getY()+rotations[dir][0].getY(), 
				points[1].getZ()+rotations[dir][0].getZ());

		points[2].setCoordinates(
				points[1].getX()+rotations[dir][1].getX(), 
				points[1].getY()+rotations[dir][1].getY(), 
				points[1].getZ()+rotations[dir][1].getZ());

		updateTiles();
	}
	
	/**
	 * Returns the graphic outline of the PiecePoint.
	 * @return the graphic outline of the PiecePoint.
	 */
	public Polyline getOutline() {
		Polyline outline = new Polyline();
		ObservableList<Double> points1 = tiles[0].getPoints();
		ObservableList<Double> points2 = tiles[1].getPoints();
		ObservableList<Double> points3 = tiles[2].getPoints();
		List<Double> points = new ArrayList<Double>();
		
		int direction = getDirection() * 2;
		

		// Hexagon n°1: begin at point (10,11)
		for (int i = (10 + direction) % 12; i !=(6 + direction) % 12; i = (i + 2) % 12) {
			points.add(points1.get(i)); 
			points.add(points1.get(i+1)); 
			points.add(points1.get(((i+2) % 12))); 
			points.add(points1.get(((i+3) % 12)));
		}
		
		
		// Hexagon n°3 : begin at 2
		for (int i = (2 + direction) % 12; i !=(10 + direction) % 12; i = (i + 2) % 12) {
			points.add(points3.get(i)); 
			points.add(points3.get(i+1)); 
			points.add(points3.get(((i+2) % 12))); 
			points.add(points3.get(((i+3) % 12)));
		}
		
		
		// Hexagon n°2 : begin at 6
		for (int i = (6 + direction) % 12; i !=(2 + direction) % 12; i = (i + 2) % 12) {
			points.add(points2.get(i)); 
			points.add(points2.get(i+1)); 
			points.add(points2.get(((i+2) % 12))); 
			points.add(points2.get(((i+3) % 12)));
		}
		outline.getPoints().addAll(points);
		outline.setStrokeLineJoin(StrokeLineJoin.ROUND);
		
		Integer minHeight = Integer.MAX_VALUE;
		Tile lowestTile = null;
		for (Tile t : tiles) {
			if (minHeight > t.height) {
				minHeight = t.height;
				lowestTile = t;
			}
		}
		
		outline.setStroke(lowestTile.getHeightColor());
		
		for (Tile t : tiles) {
			t.setOutline(outline);
		}
		
		outline.setStrokeWidth(lowestTile.getOutlineWidth());
		
		return outline;
	}
	
	/**
	 * Updates the tiles's position and rotation
	 */
	public void updateTiles() {
		Point a = points[0].toAxial();
		Point b = points[1].toAxial();
		Point c = points[2].toAxial();
		
		tiles[0].x = a.x;
		tiles[0].y = a.y;
		
		tiles[1].x = b.x;
		tiles[1].y = b.y;
		
		tiles[2].x = c.x;
		tiles[2].y = c.y;
		
		for (Tile t: tiles) {
			t.setRotate(360 - (getDirection() * 60));
		}
	}

	@Override
	public int compareTo(PiecePoint o) {
		Integer minHeight = Integer.MAX_VALUE;
		for (Tile t : tiles) {
			if (minHeight > t.height) {
				minHeight = t.height;
			}
		}
		
		Integer minHeightO = Integer.MAX_VALUE;
		for (Tile t : o.tiles) {
			if (minHeightO > t.height) {
				minHeightO = t.height;
			}
		}
		
		return minHeight.compareTo(minHeightO);
	}

	
	
	@Override
	public String toString() {
		return "PiecePoint [points= p1 " + tiles[0].toString() + " p2 " +  tiles[1].toString() + " p3 " +  tiles[2].toString() + " p4 " + " ]";
	}

	public Object clone() {
		PiecePoint o = null;
		//try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (PiecePoint) super.clone();
			
			o.points=new CubePoint[] {(CubePoint) this.points[0].clone(),(CubePoint) this.points[1].clone(),(CubePoint) this.points[2].clone()};
			//o.points[0]=(CubePoint) this.points[0].clone();
			//o.points[1]=(CubePoint) this.points[1].clone();
			//o.points[2]=(CubePoint) this.points[2].clone();
			
		//} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
		//	cnse.printStackTrace(System.err);
		//}
		// on renvoie le clone
		return o;
	}
	
	
}
