package model;

import java.awt.Point;
import java.io.Serializable;

import javafx.scene.shape.Polygon;

/**
 * Represents an hexagon in axial coordinates, derivated from {@link Polygon}
 * @author Louis Boulanger
 *
 */
public class Hexagon extends Polygon implements Serializable {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -7673339913550503784L;

	protected static double size = 30; // the inner radius from hexagon center to outer corner
    private static double offsetX = 0;
    private static double offsetY = 0;
    private static double movementSpeed = 15;
    
    private static double zoomSpeed = 1.5;
    
    private final static double MAX_SIZE = 60;
    private final static double MIN_SIZE = 10;
    
    protected int x;
    protected int y;
    
    /**
     * Constructs an hexagon from the axial coordinates
     * @param x the x axial coordinate
     * @param y the y axial coordinate
     */
	public Hexagon(int x, int y) {
		this.x = x;
		this.y = y;
		
		double xCoord = getXCoord();
		double yCoord = getYCoord();
        getPoints().addAll(
        		xCoord, yCoord,
        		xCoord, yCoord + size,
                xCoord + getN(), yCoord + size * 1.5,
                xCoord + getTileWidth(), yCoord + size,
                xCoord + getTileWidth(), yCoord,
                xCoord + getN(), yCoord - size * 0.5
        );
       
	}
	
	/**
	 * Updates the polygon's points after a resize, camera move, or anything that modifies
	 * the polygon's position and size.
	 */
	public void updatePoints() {
		double xCoord = getXCoord();
		double yCoord = getYCoord();
		
		getPoints().clear();
		getPoints().addAll(
				xCoord, yCoord,
        		xCoord, yCoord + size,
                xCoord + getN(), yCoord + size * 1.5,
                xCoord + getTileWidth(), yCoord + size,
                xCoord + getTileWidth(), yCoord,
                xCoord + getN(), yCoord - size * 0.5
        );
	}
	
	/**
	 * Transforms coordinates on the screen to an axial {@link Point}
	 * @param x the x pixel coordinate
	 * @param y the y pixel coordinate
	 * @return the corresponding {@link Point}.
	 */
	public static Point pixelToHex(float x, float y) {
		float q = (float)(((Math.sqrt(3)/3 * x) - (1./3 * y)) / size);
		float s = (float)((2./3 * y) / size);
			
		return CubePoint.roundAxial(q,s);
	}
	
	/**
	 * Transforms pixel coordinates to a {@link CubePoint}
	 * @param x the x pixel coordinate
	 * @param y the y pixel coordinate
	 * @return the corresponding {@link CubePoint}
	 */
	public static CubePoint pixelToCube(float x, float y) {
		float q = (float)(((Math.sqrt(3)/3 * x) - (1./3 * y)) / size);
		float s = (float)((2./3 * y) / size);
			
		return CubePoint.round(q,s);
	}
	
	/**
	 * Rotates the hexagon counter-clockwise
	 */
	public void rotateCounterClockwise() {
		setRotate(getRotate() - 60);
	}
	
	/**
	 * Returns the hexagons's size
	 * @return the hexagons's size
	 */
	public static double getSize() {
		return size;
	}
	
	/**
	 * Returns the x axial coordinate
	 * @return the x axial coordinate
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns the y axial coordinate
	 * @return the y axial coordinate
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the x axial coordinate to another coordinate
	 * @param other the new x axial coordinate
	 */
	public void setX(int other) {
		x = other;
	}
	
	/**
	 * Sets the y axial coordinate to another coordinate
	 * @param other the new y axial coordinate
	 */
	public void setY(int other) {
		y = other;
	}
	
	/**
	 * Returns the x pixel coordinate on the screen.
	 * @return the x pixel coordinate on the screen.
	 */
	public double getXCoord() {
		return y * getN() + (x * getTileWidth()) + offsetX;
	}
	
	/**
	 * Returns the y pixel coordinate on the screen.
	 * @return the y pixel coordinate on the screen.
	 */
	public double getYCoord() {
		return y * getTileHeight() * 0.75 + offsetY;
	}
	
	/**
	 * Returns the hexagons's width in pixels
	 * @return the hexagons's width in pixels
	 */
	public static double getTileWidth() {
		return 2 * getN();
	}
	
	/**
	 * Returns the hexagons's height in pixels
	 * @return the hexagons's height in pixels
	 */
	public static double getTileHeight() {
		return 2 * size;
	}
	
	/**
	 * Returns the x camera offset coordinate
	 * @return the x camera offset coordinate
	 */
	public static double getOffsetX() {
		return offsetX;
	}
	
	/**
	 * Returns the y camera offset coordinate
	 * @return the y camera offset coordinate
	 */
	public static double getOffsetY() {
		return offsetY;
	}
	
	/**
	 * Sets the x camera offset coordinate to a new value
	 * @param other the new value
	 */
	public static void setOffsetX(double other) {
		offsetX = other;
	}
	
	/**
	 * Sets the y camera offset coordinate to a new value
	 * @param other the new value
	 */
	public static void setOffsetY(double other) {
		offsetY = other;
	}
	
	/**
	 * Moves the camera up
	 */
	public static void moveViewUp() {
		offsetY += movementSpeed;
	}
	
	/**
	 * Moves the camera down
	 */
	public static void moveViewDown() {
		offsetY -= movementSpeed;
	}
	
	/**
	 * Moves the camera left
	 */
	public static void moveViewLeft() {
		offsetX += movementSpeed;
	}
	
	/**
	 * Moves the camera right
	 */
	public static void moveViewRight() {
		offsetX -= movementSpeed;
	}
	
	/**
	 * Returns the half-width of the hexagons
	 * @return the half-widths of the hexagons
	 */
	public static double getN() {
		return Math.sqrt(size * size * 0.75);
	}
	
	/**
	 * Zooms the camera in
	 */
	public static void zoomIn() {
		size += zoomSpeed;
		if (size > MAX_SIZE) {
			size = MAX_SIZE;
		}
	}
	
	/**
	 * Zooms the camera out
	 */
	public static void zoomOut() {
		size -= zoomSpeed;
		if (size < MIN_SIZE) {
			size = MIN_SIZE;
		}
	}
}
