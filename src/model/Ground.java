package model;

import java.io.Serializable;

import javafx.scene.image.Image;

/**
 * Ground is the surface of a tile.
 * it is determined by it's type
 * @author Loïc Grabas
 */
public class Ground implements Serializable, Cloneable {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 9219775512454524713L;

	/**
	 * Enumerates the different types of grounds
	 * @author Loïc Grabas
	 */
	public enum GroundType{
		/**
		 * A volcano
		 */
		VOLCANO,
		
		/**
		 * A forest
		 */
		FOREST,
		
		/**
		 * A lake
		 */
		LAKE,
		
		/**
		 * A mountain
		 */
		MOUNTAIN,
		
		/**
		 * A desert
		 */
		DESERT,
		
		/**
		 * A field of grass
		 */
		GRASS,
		
		/**
		 * No type (used for debugging and testing)
		 */
		NONE
	}
	
	private GroundType type;
	
	/**
	 * Constructs the ground from a {@link GroundType}
	 * @param type the {@link GroundType}
	 */
	public Ground(GroundType type) {
		this.type = type;
	}
	
	/**
	 * Returns the ground's {@link GroundType}
	 * @return the ground's {@link GroundType}
	 */
	public GroundType getType(){
		return type;
	}
	
	/**
	 * Fetches and returns the ground's {@link Image}
	 * @return an {@link Image} representing the ground.
	 */
	public Image getImage() {
		ImageLoader il = ImageLoader.getInstance();
		
		switch(type) {
		case DESERT:
			return il.getImage("desert.png");
		case FOREST:
			return il.getImage("forest.png");
		case GRASS:
			return il.getImage("plains.png");
		case LAKE:
			return il.getImage("lake.png");
		case MOUNTAIN:
			return il.getImage("mountain.png");
		case NONE:
			return il.getImage("none.jpg");
		case VOLCANO:
			return il.getImage("volcano.png");
		default:
			return null;
		}
		
	}
	
	
	public Object clone() {
		Ground o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (Ground) super.clone();
			
			o.type=this.getType();
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
	
}
