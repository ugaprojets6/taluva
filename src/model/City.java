package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a collection of {@link Buildings}
 * @author Loïc Grabas
 */
public class City implements Serializable {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -7994896139856339370L;
	
	List<BuildingPoint> buildingPoints;
	Boolean hasTower;
	Boolean hasTemple;
	
	public City(BuildingPoint FirstHutt) {
		this();
		this.addHutt(FirstHutt);
	}
	
	public City() {
		this.setBuildingPoints(new ArrayList<BuildingPoint>());
		this.setTemple(false);
		this.setTower(false);
	}
	
	/**
	 * Returns the list of {@link BuildingPoint}s of the city.
	 * @return the list of {@link BuildingPoint}s of the city.
	 */
	public List<BuildingPoint> getBuildingPoints() {
		return buildingPoints;
	}

	/**
	 * Sets the {@link BuildingPoint}s list to a new one
	 * @param buildingPoints the new list of {@link BuildingPoint}s.
	 */
	private void setBuildingPoints(List<BuildingPoint> buildingPoints) {
		this.buildingPoints = buildingPoints;
	}

	/**
	 * Adds a hutt to the city.
	 * @param hutt the {@link BuildingPoint} of the hutt to add.
	 */
	public void addHutt(BuildingPoint hutt) {
		this.buildingPoints.add(hutt);
	}
	
	/**
	 * Returns the presence of a tower in the city.
	 * @return the presence of a tower in the city.
	 */
	public Boolean getTower() {
		return hasTower;
	}
	
	/**
	 * Sets the presence of a tower in the city
	 * @param value the presence of the tower.
	 */
	public void setTower(Boolean value) {
		hasTower = value;
	}
	
	/**
	 * Returns the presence of a temple in the city.
	 * @return the presence of a temple in the city.
	 */
	public Boolean getTemple() {
		return hasTemple;
	}
	
	/**
	 * Sets the presence of a temple in the city.
	 * @param value the presence of the temple.
	 */
	public void setTemple(Boolean value) {
		hasTemple = value;
	}
	
	/**
	 * Removes a hutt from the city
	 * @param hutt the {@link BuildingPoint} of the hutt.
	 * @return {@code true} if the removal was successful, {@code false} otherwise.
	 */
	public Boolean removeHome(BuildingPoint hutt) {
		if(this.buildingPoints.size() > 1) {
			this.buildingPoints.remove(hutt);
			return true;
		}
		return false;
	}
	
	/**
	 * Adds a tower to the city
	 * @param buildingPoint the {@link BuildingPoint} with the tower to add.
	 */
	public void addTower(BuildingPoint buildingPoint) {
		this.setTower(true);
		this.buildingPoints.add(buildingPoint);
	}
	
	/**
	 * Adds a temple to the city
	 * @param buildingPoint the {@link BuildingPoint} with the temple to add.
	 */
	public void addTemple(BuildingPoint buildingPoint) {
		this.setTemple(true);
		this.buildingPoints.add(buildingPoint);
		
	}
	
	/**
	 * Checks if the city has the specified {@link CubePoint}
	 * @param point the {@link CubePoint} to check
	 * @return {@code true} if the point is contained, {@code false} otherwise.
	 */
	public boolean hasPoint(CubePoint point) {
		for (BuildingPoint buildingPoint : buildingPoints) {
			if (buildingPoint.point.equals(point)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Finds and returns a {@link BuildingPoint} corresponding to a specified {@link CubePoint}.
	 * @param point the {@link CubePoint} to check
	 * @return a {@link BuildingPoint} if the city has the point, {@code null} otherwise.
	 */
	public BuildingPoint getBuildingPoint(CubePoint point) {
		for (BuildingPoint bp : buildingPoints) {
			if (bp.point.equals(point)) {
				return bp;
			}
		}
		return null;
	}

}
