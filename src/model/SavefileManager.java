package model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.EmptyStackException;
import java.util.Stack;

import javax.imageio.ImageIO;

import controller.GameController;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import view.GameViewController;

/**
 * Manages savefiles, enabling saving, loading, deleting savefiles.
 * By default, saves are stored in the home directory, under the folder
 * .taluva-saves.
 * @author Louis Boulanger
 *
 */
public class SavefileManager {
	
	private String savefileName;
	private File saveDirectory;
	
	private static String lastAutoSaveName = null;
	
	private static Stack<byte[]> undoStack = new Stack<byte[]>();
	private static Stack<byte[]> redoStack = new Stack<byte[]>();
	
	/**
	 * Constructs a SavefileManager with a file name
	 * @param savefileName the name of the savefile
	 */
	public SavefileManager(String savefileName) {
		saveDirectory = new File(System.getProperty("user.home") + "/taluva-saves");
		if (!saveDirectory.exists()) {
			saveDirectory.mkdir();
		}
		this.savefileName = saveDirectory.getPath() + "/" + savefileName;
	}
	
	/**
	 * Constructs a SavefileManager without a savefile associated. No operations will be
	 * available until {@link automaticSave} or {@link setSavefileName} is called.
	 */
	public SavefileManager() {
		saveDirectory = new File(System.getProperty("user.home") + "/taluva-saves");
		if (!saveDirectory.exists()) {
			saveDirectory.mkdir();
		}
		savefileName = null;
	}
	
	/**
	 * Resets the autosave name
	 */
	public static void resetAutosave() {
		lastAutoSaveName = null;
	}
	
	/**
	 * Sets the savefile name
	 * @param value the new savefile name
	 */
	public void setSavefileName(String value) {
		savefileName = saveDirectory.getPath() + "/" + value;
	}
	
	/**
	 * Returns the save directory.
	 * @return the save directory.
	 */
	public File getSaveDirectory() {
		return saveDirectory;
	}
	
	/**
	 * Saves as an automatic save, assigning a generated name if the game doesn't have a save associated.
	 * @param gc the GameController to save
	 */
	public void automaticSave(GameController gc) {
		if (lastAutoSaveName == null) {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
			lastAutoSaveName = sdf.format(cal.getTime());
		}
		setSavefileName(lastAutoSaveName + ".tlv");
		save(gc);
	}
	
	/**
	 * Saves the {@link GameController} into the specified savefile
	 * @param gc The {@link GameController} to save.
	 */
	public void save(GameController gc) {
		if (gc.getSaveName() != null) {
			savefileName = gc.getSaveName();
		}
		if (savefileName != null) {			
			try {
				FileOutputStream outputStream = new FileOutputStream(savefileName);
				ObjectOutputStream out = new ObjectOutputStream(outputStream);
				
				gc.setSaveName(savefileName);
				
				out.writeObject(gc);
				out.close();
				outputStream.close();
				
				// Snapshot
				if (gc.getView() != null) {					
					WritableImage snap = gc.getView().getTileMap().snapshot(null, null);
					BufferedImage image = SwingFXUtils.fromFXImage(snap, null);
					File imageFile = new File(savefileName + ".png");
					ImageIO.write(image, "png", imageFile);
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Returns the snapshot image associated with a save
	 * @return the snapshot image associated with a save
	 */
	public Image getImage() {
		if (savefileName != null) {			
			try {
				FileInputStream saveSnapshot = new FileInputStream(savefileName + ".png");
				Image im = new Image(saveSnapshot);
				return im;
			} catch (FileNotFoundException e) {
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Loads a savefile, setting the {@link GameController} in the {@link GameViewController}
	 * and returning the new {@link GameController}.
	 * @param gvc The view in which the {@link GameController} will be loaded.
	 * @return The ne		justPlayed = true;
w {@link GameController}.
	 */
	public GameController load(GameViewController gvc) {
		if (savefileName != null) {			
			GameController gc = null;
			try {
				FileInputStream inputStream = new FileInputStream(savefileName);
				ObjectInputStream in = new ObjectInputStream(inputStream);
				
				gc = (GameController) in.readObject();
				in.close();
				inputStream.close();
				
				gc.generateTransientData(gvc);
				gvc.setGameController(gc, true);
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return gc;
		}
		return null;
	}
	
	/**
	 * Loads a {@link GameController} from a file and returns it without loading it into
	 * a view.
	 * @return the {@link GameController}.
	 */
	public GameController load() {
		if (savefileName != null) {			
			GameController gc = null;
			try {
				FileInputStream inputStream = new FileInputStream(savefileName);
				ObjectInputStream in = new ObjectInputStream(inputStream);
				
				gc = (GameController) in.readObject();
				in.close();
				inputStream.close();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return gc;
		}
		return null;
	}
	
	/**
	 * Renames a savefile to a new one.
	 * @param newName the new name.
	 */
	public void rename(String newName) {
		if (savefileName != null) {			
			File current = new File(savefileName);
			File next = new File(saveDirectory.getPath() + "/" + newName);
			
			if (next.exists()) {
				// TODO: prompt user to check what to do (the file already exists)
			} else {
				current.renameTo(next);
				savefileName = next.getAbsolutePath();
			}
		}
	}
	
	/**
	 * Deletes the savefile.
	 */
	public void delete() {
		if (savefileName != null) {			
			File current = new File(savefileName);
			if (current.exists()) {
				current.delete();
			}
			File currentSnap = new File(current.getAbsolutePath() + ".png");
			if (currentSnap.exists()) {
				currentSnap.delete();
			}
		}
	}
	
	private void saveToStack(GameController gc, Stack<byte[]> stack) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			
			gc.setSaveName(savefileName);
			
			out.writeObject(gc);
			out.close();
			
			stack.add(bos.toByteArray());
			
			bos.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private GameController loadFromStack(Stack<byte[]> stack) {
		GameController gc = null;
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(stack.pop());
			ObjectInputStream in = new ObjectInputStream(bis);
			
			gc = (GameController)in.readObject();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EmptyStackException e) {
			
		}
		return gc;
	}
	
	public void saveSnapshot(GameController gc) {
		redoStack.clear();		
		saveToStack(gc, undoStack);
	}
	
	public GameController undo(GameController current) {
		GameController gc = null;
		if (!undoStack.empty()) {
			saveToStack(current, redoStack);
			gc = loadFromStack(undoStack);
		}
		return gc;
	}
	
	public GameController redo(GameController current) {
		GameController gc = null;
		if (!redoStack.empty()) {			
			saveToStack(current, undoStack);
			gc = loadFromStack(redoStack);
		}
		return gc;
	}
}
