package model;

import java.io.Serializable;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import taluva.Configuration;

/**
 * Represents a building, of a certain type and color.
 * @author Loïc Grabas
 *
 */
public class Building implements Serializable{
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -1800611537332388385L;

	/**
	 * Enumerates the different building types
	 * @author Loïc Grabas
	 *
	 */
	public enum BuildingType {
		/**
		 * A simple hutt, can be placed on height 1 hexagons, and can extend cities multiple times.
		 */
		HUTT,
		
		/**
		 * A temple, can be placed in cities of size 3 or more. 
		 */
		TEMPLE,
		
		/**
		 * A tower, can be placed in cities on a hexagon of height 3 or more.
		 */
		TOWER
	}
	
	private BuildingType type;
	private transient Color color;
	private String colorName;
	

	/**
	 * Creates a new building
	 * @param type type of the building
	 * @param color color of the building
	 * @param colorName the name of the color
	 */
	public Building(BuildingType type,Color color, String colorName) {
		this.type = type;
		this.color = color;
		this.colorName = colorName;
	}
	
	/**
	 * Returns the color name of the building
	 * @return the color name.
	 */
	public String getColorName() {
		return colorName;
	}
	
	/**
	 * Regenerates the color from the color name, used after deserialization since
	 * {@link Color} cannot be serialized.
	 */
	public void generateColor() {
		Configuration conf = Configuration.instance();
		this.color = conf.lisCouleur(this.colorName);
	}
	
    /**
     * Returns the type of the building
     * @return the building's type.
     */
	public BuildingType getType() {
		return type;
	}
	
    /**
     * Returns the {@link Color} of the building
     * @return the building's {@link Color}.
     */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Fetches and returns an {@link Image} corresponding to the current building.
	 * @param level the hexagon's height
	 * @return an {@link Image} representing the building at a certain height.
	 */
	public Image getImage(int level) {
		ImageLoader il = ImageLoader.getInstance();
		switch(type) {
		case HUTT:
			if (level <= 1)
				return il.getImage("house_tile.png");
			if (level > 6)
				return il.getImage("house_tile_6.png");
			return il.getImage("house_tile_" + level + ".png");
		case TEMPLE:
			return il.getImage("temple_tile.png");
		case TOWER:
			return il.getImage("tower_tile.png");
		default:
			return null;		
		}
	}
	
	
}
