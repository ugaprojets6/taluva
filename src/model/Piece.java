package model;

import java.io.Serializable;

import model.Ground.GroundType;
/**
 * Piece is a composition of 3 grounds that has to have at
 * least one volcano amongst them and the volcano has to be the second
 * 
 * @author Loïc Grabas
 */
public class Piece implements Serializable , Cloneable {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 7772480075766431474L;
	
	public Tile[] tiles;	
	
	/**
	 * Creation of a piece needs to have only one volcano amongst
	 * the grounds
	 * @param g1 a ground to compose the piece
	 * @param g2 a ground to compose the piece
	 */
	public Piece(Ground g1,Ground g2) {
		tiles = new Tile[3];
		tiles[0] = new Tile(g1);
		tiles[1] = new Tile(new Ground(GroundType.VOLCANO));
		tiles[2] = new Tile(g2);
	}
	
	/**
	 * Constructs a piece with default ({@link GroundType.NONE}) grounds.
	 */
	public Piece(){
		tiles = new Tile[] {
				new Tile(new Ground(GroundType.NONE)),
				new Tile(new Ground(GroundType.NONE)),
				new Tile(new Ground(GroundType.NONE))
		};
	}
	
	/**
	 * Sets the {@link Ground} on a given position
	 * @param ground the new {@link Ground}
	 * @param position the new {@link Ground}'s position
	 */
	public void setGround(Ground ground, int position){
		tiles[position].setGround(ground);
		
	}
	
	/**
	 * Returns the {@link Ground} at a certain position
	 * @param position the position to fetch from
	 * @return the {@link Ground}
	 * @throws IndexOutOfBoundsException when the position is out of bounds
	 */
	public Ground getGround(int position) throws IndexOutOfBoundsException {
		if(position>2 || position < 0) {
			throw new IndexOutOfBoundsException("Position must be 0, 1 or 1");
		}
		return tiles[position].getGround();
	}
	
	/**
	 * Gets the ground from the piece that is the volcano
	 * @return the {@link Ground} containing the volcano
	 */
	public Tile getVolcano() {
		return tiles[getVolcanoPosition()];
	}
	
	/**
	 * Gets the position of the ground from the piece that is the volcano
	 * @return 1 (it's always the volcano's position)
	 */
	public int getVolcanoPosition() {
		return 1;
	}
	
	
	
	
	public Object clone() {
		Piece o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (Piece) super.clone();
			
			o.tiles=new Tile[] {(Tile) this.tiles[0].clone(),(Tile) this.tiles[1].clone(),(Tile) this.tiles[2].clone()};
			//o.tiles[0]=(Tile) this.tiles[0].clone();
			//o.tiles[1]=(Tile) this.tiles[1].clone();
			//o.tiles[2]=(Tile) this.tiles[2].clone();
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
			
		}
		// on renvoie le clone
		return o;
	}
	
}
