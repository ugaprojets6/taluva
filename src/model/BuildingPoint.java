package model;

import javafx.scene.paint.Color;

/**
 * A building with an associated {@link CubePoint}.
 * @author Loïc Grabas
 */
public class BuildingPoint extends Building{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -8740654160965870955L;
	
	public CubePoint point;
	
	public BuildingPoint(BuildingType type, Color color,CubePoint point, String colorName) {
		super(type, color, colorName);
		this.point = point;
	}
	
	public BuildingPoint(Building b,CubePoint point) {
		super(b.getType(), b.getColor(),b.getColorName());
		this.point = point;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof BuildingPoint)) {
			return false;
		}
		BuildingPoint p = (BuildingPoint)other;
		return p.point.equals(point) && p.getColor().equals(getColor());
	}
}
