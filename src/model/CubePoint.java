package model;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * CubePoint is a point with 3 coordinates that permits to
 * create an hexagonal grid, respecting the constraint {@code x + y + z = 0}.
 * @see https://www.redblobgames.com/grids/hexagons/?fbclid=IwAR0JFiFckJAVdJVWcy7ZITKxh8pstxWsZqSe9MUMOahkLjYKJMoy5a-Vr4c
 * @author Loïc Grabas
 */
public class CubePoint implements Serializable,Cloneable {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8299092003729489280L;
	
	/**
	 * The coordinates around a cubepoint
	 */
	public static CubePoint[] sides = new CubePoint[] {
			new CubePoint(+1, -1, 0), new CubePoint(+1, 0, -1), new CubePoint(0, +1, -1), 
			new CubePoint(-1, +1, 0), new CubePoint(-1, 0, +1), new CubePoint(0, -1, +1), 
	};
	
	private int x;
	private int y;
	private int z;
	
	/**
	 * Creates a new CubePoint at the coordinates (0,0,0).
	 */
	public CubePoint() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	/**
	 * Contruct with cubic coordinates
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param z the z coordinate
	 * @throws IllegalStateException when the constraint {@¢ode x + y + z = 0} is violated.
	 */
	public CubePoint(int x, int y, int z) throws IllegalStateException {
		if (x + y + z != 0) {
			throw new IllegalStateException("Constraint x + y + z = 0 violated");
		}
		
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Construct the point from axial coordinates
	 * @param x the x axial coordinate
	 * @param z the z axial coordinate
	 */
	public CubePoint(int x, int z) {
		this.x = x;
		this.y = -x-z;
		this.z = z;
	}
	
	/**
	 * Constructs the point from another, copying the coordinates
	 * @param other the other CubePoint to copy.
	 */
	public CubePoint(CubePoint other) {
		this.x = other.x;
		this.y = other.y;
		this.z = other.z;
	}
	
	/**
	 * Returns the x coordinate.
	 * @return the x coordinate.
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns the y coordinate.
	 * @return the y coordinate.
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Returns the z coordinate.
	 * @return the z coordinate.
	 */
	public int getZ() {
		return z;
	}
	
	/**
	 * Sets the point's coordinates a to a new set of coordinates.
	 * @param newX the new x coordinate.
	 * @param newY the new y coordinate.
	 * @param newZ the new z coordinate.
	 * @throws IllegalStateException when the constraint {@code x + y + z = 0} is violated.
	 */
	public void setCoordinates(int newX, int newY, int newZ) throws IllegalStateException {
		if (newX + newY + newZ != 0) {
			throw new IllegalStateException("Constraint x + y + z = 0 violated");
		}
		x = newX;
		y = newY;
		z = newZ;
	}
	
	/**
	 * Checks the coordinate of this point is next to an other
	 * @param other the compared cube point
	 * @return {@code true} if adjacent, {@code false} otherwise
	 */
	public boolean isAdjacent(CubePoint other) {
		if(other.equals(this)) {
			return false;
		}
		CubePoint result = new CubePoint();
		result.x = x - other.x;
		result.y = y - other.y;
		result.z = z - other.z;
		for (CubePoint side : sides) {
			if(side.equals(result)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns a list of cube points corresponding to the 6 neighbours of a cube point.
	 * @return the {@link List} of neighbours.
	 */
	public List<CubePoint> getNeighbours() {
		List<CubePoint> neighbours = new ArrayList<CubePoint>();
		for (CubePoint p : sides) {
			neighbours.add(new CubePoint(
					p.getX() + this.getX(), 
					p.getY() + this.getY(), 
					p.getZ() + this.getZ()));
		}
		return neighbours;
	}
	
	/** 
	 * Returns a cube point given floating CubePoint coordinates
	 * @param x the floating point x coordinate
	 * @param y the floating point y coordinate
	 * @param z the floating point z coordinate
	 * @return a CubePoint rounded from the floating point coordinates.
	 * @throws IllegalStateException when the constraint {@code x + y + z = 0} is violated.
	 */
	public static CubePoint round(float x, float y, float z) throws IllegalStateException {
		int rx = Math.round(x);
		int ry = Math.round(y);
		int rz = Math.round(z);
				
		float xDiff = Math.abs(rx - x);
		float yDiff = Math.abs(ry - y);
		float zDiff = Math.abs(rz - z);
		
		if (xDiff > yDiff && xDiff > zDiff) {
			rx = -ry-rz;
		} else if (yDiff > zDiff) {
	        ry = -rx-rz;
		} else {
	        rz = -rx-ry;
		}
		
		return new CubePoint(rx, ry, rz);
	}
	
	/**
	 * Returns a {@link Point} in axial coordinate corresponding to the CubePoint.
	 * @return the axial {@link Point}.
	 */
	public Point toAxial() {
		return new Point(x,z);
	}
	
	/**
	 * Converts axial coordinates to a CubePoint.
	 * @param x the x axial coordinate
	 * @param z the z axial coordinate
	 * @return the corresponding CubePoint
	 */
	public static CubePoint fromAxial(int x, int z) {
		return new CubePoint(x,-x-z,z);
	}
	
	/**
	 * Returns a cube point given floating axial coordinates
	 * @param q the q axial coordinate
	 * @param r the r axial coordinate
	 * @return a cube point rounded from the floating point coordinates
	 */
	public static CubePoint round(float q, float r) {
		float x = q;
	    float z = r;
	    float y = -x-z;

		CubePoint p = CubePoint.round(x,y,z);
		
		return p;
	}
	
	/**
	 * Returns an axial {@link Point} given floating axial coordinates
	 * @param q the q axial coordinate
	 * @param r the r axial coordinate
	 * @return a {@link Point} in axial coordinates rounded from floating point coordinates
	 */
	public static Point roundAxial(float q, float r) {
		float x = q;
	    float z = r;
	    float y = -x-z;

		CubePoint p = CubePoint.round(x,y,z);
		
		return p.toAxial();
	}
	
	@Override
	public boolean equals(Object obj) {
		CubePoint p;
		if(obj instanceof CubePoint) {
			p = (CubePoint) obj;
			return (p.x == x && p.y == y && p.z == z);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return x+","+y+","+z;
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	
	
	
	public Object clone() {
		CubePoint o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (CubePoint) super.clone();
			
			o.x= this.x;
			o.y= this.y;
			o.z= this.z;
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
}
