package model;

import java.io.InputStream;
import java.util.Hashtable;

import javafx.scene.image.Image;

/**
 * A singleton class for loading image efficiently, acting as a cache.
 * @author Louis Boulanger
 */
public class ImageLoader {
	private Hashtable<String, Image> images;
	private static ImageLoader instance = null;
	
	/**
	 * Returns the singleton's instance
	 * @return the singleton's instance
	 */
	public static ImageLoader getInstance() {
		if (instance == null) {
			instance = new ImageLoader();
		}
		
		return instance;
	}
	
	/**
	 * Constructs the ImageLoader
	 */
	protected ImageLoader() {
		images = new Hashtable<String, Image>();
	}
	
	/**
	 * Fetches and returns an {@link Image} from its name. If the image was not loaded before, it will be
	 * searched in the {@code resources} directory using the {@link ClassLoader}. If it was already
	 * loaded before, it is loaded from the cache
	 * @param name the image's path
	 * @return an {@link Image} corresponding to the name.
	 */
	public Image getImage(String name) {
		if (!images.containsKey(name)) {
			InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(name);
			Image im = new Image(in);
			images.put(name, im);
		}
		return images.get(name);
	}
}
