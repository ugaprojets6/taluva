package model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.Ground.GroundType;

/**
 * PieceDeck is the composition of a deck (stack) of pieces to draw from
 *
 * @author Loïc Grabas
 *
 */
public class PieceDeck implements Serializable, Cloneable{

	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -5892624705954370737L;
	private Stack<Piece> pieces;
	private transient IntegerProperty sizeProperty = new SimpleIntegerProperty();
	private int size;
	
	/*
	 * @param nbPieces the number of {@link Piece}s do create in the deck
	 */
	/**
	 * Constructs a {@link PieceDeck} with a specified size and randomness
	 * @param nbPieces the number of {@link Piece}s in the deck
	 * @param alea if {@code true}, the deck will be random ; if not, the deck
	 * will be a subset of the original deck of the game.
	 */
	public PieceDeck(int nbPieces, boolean alea) {
		if(alea) {
			pieces = new Stack<Piece>();
			sizeProperty.set(nbPieces);
			size = nbPieces;
			createDeckAlea(nbPieces);
		} else {
			pieces = new Stack<Piece>();
			createDeckWithPredefinedPieces();
			sizeProperty.set(nbPieces);
			size = nbPieces;
			Collections.shuffle(pieces);
			pieces = deck(pieces, nbPieces);
		}
	}

	/**
	 * Regenerates the {@link SimpleIntegerProperty} for the deck size
	 * after deserialization
	 */
	public void generateSizeProperty() {
		sizeProperty = new SimpleIntegerProperty();
		sizeProperty.set(size);
	}
	
	/**
	 * Returns a stack of {@link Piece}s representing the deck, used to create a subset of
	 * an entire deck
	 * @param pieces the original deck
	 * @param nb the number of {@link Piece}s in the deck subset
	 * @return A subset of {@code nb} {@link Piece}s of the original deck.
	 */
	private Stack<Piece> deck(Stack<Piece> pieces, int nb) {
		Stack<Piece> deck = new Stack<Piece>();

		for (int i = 0; i < nb; i++) {
			deck.add(pieces.get(i));	
		}

		return deck;
	}

	/**
	 * Returns the adequate number of {@link Piece}s in a deck according to the number
	 * of players in the game
	 * @param nbPlayers the number of players in the deck
	 * @return the correct number of {@link Piece}s in a deck for {@code nbPlayers} players.
	 */
	public static int getNumberPiecesFromPlayers(int nbPlayers) {
		int result = 0;
		if(nbPlayers==2) {
			result = 24;
		}else if(nbPlayers==3) {
			result = 36;
		}else {
			result = 48;
		}
		return result;
	}

	/**
	 * Creates a random deck
	 * @param nbPieces the number of pieces in the deck
	 */
	private void createDeckAlea(int nbPieces) {
		GroundType type = GroundType.NONE;
		Ground[] grounds = new Ground[] {new Ground(type),new Ground(type)};

		for (int i = 0; i < nbPieces; i++) {
			for(int k=0;k<2;k++) {
				type = GroundType.values()[new Random().nextInt(GroundType.values().length-2)+1];
				grounds[k] = new Ground(type);
			}
			Piece p = new Piece(grounds[0],grounds[1]);
			pieces.add(p);	
		}
	}

	/**
	 * Creates a deck from the {@code biomes.txt} file
	 */
	private void createDeckWithPredefinedPieces() {
		GroundType type = GroundType.NONE;
		Ground[] grounds = new Ground[] {new Ground(type),new Ground(type)};

		Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream("biomes.txt"));
		while (scanner.hasNextLine()) {
			String [] arrOfStr = scanner.nextLine().split(";"); 
			for (int i = 0; i < Integer.parseInt(arrOfStr[1]); i++) {
				for(int k=0;k<2;k++) {
					switch (Integer.parseInt(arrOfStr[0].split(",")[k])) {
					case 1:
						type = GroundType.FOREST;
						break;
					case 2:
						type = GroundType.LAKE;
						break;
					case 3:
						type = GroundType.MOUNTAIN;
						break;
					case 4:
						type = GroundType.DESERT;
						break;
					case 5:
						type = GroundType.GRASS;
						break;
					default:
						type = GroundType.NONE;
						break;
					}
					grounds[k] = new Ground(type);
				}
				Piece p = new Piece(grounds[0],grounds[1]);
				pieces.add(p);
			}
		}
		scanner.close();

	}

	/**
	 * Returns the size of the deck.
	 * @return the size of the deck.
	 */
	public int getSize() {
		return pieces.size();
	}

	/**
	 * Returns the {@link IntegerProperty} of the deck's size.
	 * @return the {@link IntegerProperty} of the deck's size.
	 */
	public IntegerProperty getSizeProperty() {
		return sizeProperty;
	}
	
	/**
	 * Pops the stack and effectively draws a {@link Piece} from the deck
	 * @return the {@link Piece} drawn
	 */
	public Piece drawPiece() {
		try {
			sizeProperty.set(sizeProperty.get() - 1);
			size--;
			return pieces.pop();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns whether the deck is empty or not.
	 * @return {@code true} if the deck is empty, {@code false} if not.
	 */
	public boolean isEmpty() {
		return (pieces.isEmpty());
	}
	
	
	@SuppressWarnings("unchecked")
	public Object clone() {
		PieceDeck o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (PieceDeck) super.clone();
			
			o.pieces= (Stack<Piece>) this.pieces.clone();
			
			o.sizeProperty = new SimpleIntegerProperty(this.getSize());
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
	
}
