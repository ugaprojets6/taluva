package model;

import java.io.Serializable;

import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.Effect;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Polyline;
import model.Building.BuildingType;

/**
 * Tile is the information of a hexagonal element.
 * it is composed of a ground, a height and a building
 * 
 *  @author Louis Boulanger, Loïc Grabas
 */
public class Tile extends Hexagon implements Comparable<Tile>, Serializable, Cloneable {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 7846482551293627747L;
	
	private Ground ground;
	public int height;
	private Building building;
	private transient ImageView buildingImageView;
	private transient Polyline outline;
	
	/**
	 * Colors of the border, indicating the height of the tile.
	 */
	private static final Color[] BORDER_COLORS = {
		Color.rgb(89,25,80),
		Color.rgb(163,31,144),
		Color.rgb(255,0,218),
		Color.rgb(255,114,234)
	};
	
	/**
	 * Constructs a Tile with the default height of 1
	 * @param g the ground of the tile
	 */
	public Tile(Ground g) {
		this(g,0,0);
	}
	
	/**
	 * Constructs a Tile with a given {@link Ground}, and coordinates. 
	 * @param g the {@link Ground}.
	 * @param x the x axial coordinate.
	 * @param y the y axial coordinate.
	 */
	public Tile(Ground g, int x, int y) {
		super(x,y);
		ground = g;
		height = 1;
		building = null;
		buildingImageView = new ImageView();
		setEffect(getHeightLighting());
		setFill(new ImagePattern(g.getImage(), 0, 0, 1, 1, true));
	}
	
	
	
	public Tile(Ground g, int x, int y,int h) {
		super(x,y);
		ground = g;
		height = h;
		building = null;
		buildingImageView = null;
		//setEffect(getHeightLighting());
		//setFill(new ImagePattern(g.getImage(), 0, 0, 1, 1, true));
	}
	
	/**
	 * Constructs a tile with a ground, coordinates, a building and a height
	 * @param g the {@link Ground}
	 * @param x the x axial coordinate.
	 * @param y the y axial coordinate.
	 * @param bp the current {@link Building}
	 * @param height the height of the tile.
	 */
	public Tile(Ground g, int x, int y, Building bp, int height) {
		super(x,y);
		ground = g;
		this.height = height;
		buildingImageView = new ImageView();
		if(bp != null)
			setBuilding(bp);
		setEffect(getHeightLighting());
		setFill(new ImagePattern(g.getImage(), 0, 0, 1, 1, true));
	}
	
	/**
	 * Regenerates the building image and other transient fields after
	 * deserialization.
	 */
	public void generateBuildingImage() {
		setFill(new ImagePattern(ground.getImage(), 0, 0, 1, 1, true));
		buildingImageView = new ImageView();
		if (building != null) {
			setBuilding(building);
		}
	}
	
	/**
	 * Returns whether or not the tile contains a {@link Building}.
	 * @return {@code true} if the tile has a {@link Building}, {@code false} otherwise.
	 */
	public boolean hasBuilding() {
		return (building!=null);
	}
	
	/**
	 * Returns whether or not the tile contains a temple.
	 * @return {@code true} if the tile has a temple, {@code false} otherwise.
	 */
	public boolean hasTemple(){
		if(building!=null) {
			if(building.getType() == BuildingType.TEMPLE) {
				return true;
			}
		}
		return false;
    }
	
	/**
	 * Sets the tile's piece outline reference to a new one
	 * @param p the outline.
	 */
	public void setOutline(Polyline p) {
		this.outline = p;
	}

	/**
	 * Returns whether or not the tile contains a tower.
	 * @return {@code true} if the tile has a tower, {@code false} otherwise.
	 */
    public boolean hasTower(){
    	if(building!=null) {
			if(building.getType() == BuildingType.TOWER) {
				return true;
			}
		}
    	
		return false;
    }
    
    /**
     * Returns the outline width.
     * @return the outline width.
     */
    public double getOutlineWidth() {
    	return (height > 1 ? 4 : 2) * (size/30);
    }
	
    /**
     * Returns the tile's piece outline.
     * @return the tile's piece outline.
     */
    public Polyline getOutline() {
    	return outline;
    }
    
    /**
     * Returns the tile's ground.
     * @return the tile's ground.
     */
	public Ground getGround() {
		return ground;
	}
	
	/**
	 * Replaces ground and place the tile to a higher level which 
	 * erases the current building
	 * @param g the new {@link Ground}
	 * @param rotation the tile's rotation
	 */
	public void replaceGround(Ground g, Double rotation) {
		ground = g;
		height++;
		building = null;
		if (buildingImageView!=null)
		{
			buildingImageView.setImage(null);;
		}
		setFill(new ImagePattern(g.getImage(), 0, 0, 1, 1, true));
		setRotate(rotation);
		//setStroke(getHeightColor());
		updatePoints();
		setEffect(getHeightLighting());
		
	}
	
	/**
	 * Returns the border color according to the tile's height
	 * @return the corresponding {@link Color}
	 */
	public Color getHeightColor() {
		if (height == 1) {
			return Color.BLACK;
		}
		
		if (height > 5) {
			return BORDER_COLORS[3];
		}
		
		return BORDER_COLORS[height - 2];
	}
	
	/**
	 * Sets the tile's {@link Ground} to a new one.
	 * @param g the new {@link Ground}
	 */
	public void setGround(Ground g) {
		ground = g;
		setFill(new ImagePattern(g.getImage(), 0, 0, 1, 1, true));
	}
	
	/**
	 * Returns the tile's height
	 * @return the tile's height
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Sets the height to a new value
	 * @param value the new height value
	 */
	public void setHeight(int value) {
		this.height = value;
	}
	
	/**
	 * Returns an {@link Effect} according to the tile's height, whitening
	 * the tile the higher it gets.
	 * @return The tile's {@link Effect}.
	 */
	public Effect getHeightLighting() {
		ColorAdjust monochrome = new ColorAdjust();
        monochrome.setSaturation(-0.3 * (height - 1));
        
        return monochrome;
	}
	
	/**
	 * Returns the tile's current {@link Building}
	 * @return the tile's current {@link Building}
	 */
	public Building getBuilding(){
		return building;
	}
	
	/**
	 * Sets the tile's feedforward
	 * @param type the FeedforwardType.
	 */
	public void setFeedforward(FeedforwardType type) {
		Lighting lighting = new Lighting();
		lighting.setDiffuseConstant(2.0);
		lighting.setSpecularConstant(0.0);
		lighting.setSpecularExponent(0.0);
		lighting.setSurfaceScale(0.0);
		
		switch (type) {
		case CORRECTPLACEMENT:
			lighting.setLight(new Light.Distant(45, 45, Color.GREEN));  
			break;
		case NONE:
			setEffect(getHeightLighting());
			return;
		case WRONGPLACEMENT:
			lighting.setLight(new Light.Distant(45, 45, Color.RED));
			break;
		case CITYSELECTED:
			lighting.setLight(new Light.Distant(45, 45, Color.PURPLE));
		default:
			break;
		}
		setEffect(lighting);
	}
	
	@Override
	public void updatePoints() {
		super.updatePoints();
		if (buildingImageView !=null && buildingImageView.getImage() != null) {
			buildingImageView.setX(getXCoord());
			buildingImageView.setY(getYCoord() - (1.0/4 * getTileHeight()));
			buildingImageView.setFitWidth(getTileWidth());
			buildingImageView.setFitHeight(getTileHeight());
		}
		//setStrokeWidth((height > 1 ? 4 : 1) * (size/30));
	}
	
	/**
	 * Returns the tile's building's {@link ImageView}
	 * @return the tile's building's {@link ImageView}
	 */
	public ImageView getBuildingImage() {
		return buildingImageView;
	}
	
	/**
	 * Sets the tile's {@link Building}
	 * @param b the new {@link Building}
	 */
	public void setBuilding(Building b) {
		building = b;
		if (buildingImageView!=null)
		{
			buildingImageView.setImage(b.getImage(height));
			buildingImageView.setFitWidth(getTileWidth());
			buildingImageView.setFitHeight(getTileHeight());
			buildingImageView.setPreserveRatio(true);
			buildingImageView.setSmooth(true);
			buildingImageView.setX(getXCoord());
			buildingImageView.setY(getYCoord() - (1.0/4 * getTileHeight()));
			buildingImageView.setScaleX(0.5);
			buildingImageView.setScaleY(0.5);
			Lighting lighting = new Lighting();
	        lighting.setDiffuseConstant(2.0);
	        lighting.setSpecularConstant(0.0);
	        lighting.setSpecularExponent(0.0);
	        lighting.setSurfaceScale(0.0);
	        
	        Color c = building.getColor();
	        setEffect(getHeightLighting());
	        lighting.setLight(new Light.Distant(45, 45, c));
	        buildingImageView.setEffect(lighting);
		}
	}

	@Override
	public int compareTo(Tile o) {
		return new Integer(height).compareTo(o.height);
	}
	
	@Override
	public String toString() {
		String s = ground.getType().name() + "(" + height + ")";
		if (building != null) {
			s += " : " + building.getType().name() + " " + building.getColor().toString();
		}
		s += " [" + x + ", " + y + "] " + getRotate();
		return s;
	}
	
	
	public Object clone() {
		Tile o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (Tile) super.clone();
			
			o.x=this.x;
			o.y=this.y;
			
			if (this.building!=null)
			{
				o.building=new Building(this.building.getType(),this.building.getColor(),building.getColorName());
			}
			else o.building=null;
			o.ground=(Ground) this.ground.clone();
			o.height=this.height;
			
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
}
