package AI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import controller.Player;
import javafx.scene.paint.Color;
import model.CubePoint;
import model.Board.ReasonBuilding;
import model.Board.ReasonPiece;
import model.Building;
import model.Building.BuildingType;
import view.InterfaceGraphique;
import model.BuildingPoint;
import model.City;

public class AIR extends Player {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -2790762522312907040L;

	//Artificial Inteligent Random
	public AIR(String name, Color color, String colorName) {
		super(name, color, colorName);
	}

	public void startTurn() {
		super.startTurn();
		InterfaceGraphique.soundAvailable = false;
		gc.drawPieceAction();
		placePieces();
	}
	
	@Override
	public void endTurn() {
		super.endTurn();
	}

	@Override
	public void placePieces() {
		
		ArrayList<CubePoint> listeDePoints = new ArrayList<CubePoint>();
	
		//recuperer les points au hasard deja posé (x,y ,z) et les bordures de ce points
		Object[] crunchifyKeys =  gc.board.tiles.keySet().toArray(); 
		if(crunchifyKeys.length == 0) {
			CubePoint[] positions = new CubePoint[] {
					new CubePoint(0,-3,3),
					new CubePoint(0,-2,2),
					new CubePoint(1,-3,2)
			};
			currentPiece.points =positions;
			super.placePieces();
		}
		else {
			for (int i =0; i<= crunchifyKeys.length -1; i++) {
				listeDePoints.add((CubePoint)crunchifyKeys[i]);
				
			}
			for (int i =0; i < crunchifyKeys.length; i ++) {
				CubePoint key = (CubePoint) crunchifyKeys[i];
				ArrayList<CubePoint> points=ring(key,1);
				for (int j=0;j<points.size();j++)
				{
					if(!listeDePoints.contains(points.get(j)))
						listeDePoints.add(points.get(j));
				}
				
			}
			
			Boolean ready =true;
			while(ready) {
				//choisir un point au hasard
				CubePoint point_hasard = listeDePoints.get(new Random().nextInt(listeDePoints.size()));				
				ArrayList<CubePoint> points_Hexagone_choisie=ring(point_hasard,1);
				int choisi = new Random().nextInt(points_Hexagone_choisie.size());
				int choisi2 = (choisi +1)%points_Hexagone_choisie.size();
				CubePoint[] positions = new CubePoint[] {
						points_Hexagone_choisie.get(choisi),
						point_hasard,points_Hexagone_choisie.get(choisi2)
				};
				
				currentPiece.setPoints(positions);
				
				if (gc.board.canInsertPiece(currentPiece) == ReasonPiece.OK) {
					ready = false;
					super.placePieces();
				}
			}
		}
		
	}
	
	@Override
	public void placeBuildings(){
		ArrayList<CubePoint> listeDePoints = new ArrayList<CubePoint>();
		
		//recuperer les points au hasard deja posé (x,y ,z) et les bordures de ce points
		Object[] crunchifyKeys =  gc.board.tiles.keySet().toArray(); 
		for (int i =0; i<= crunchifyKeys.length -1; i++) {
			listeDePoints.add((CubePoint)crunchifyKeys[i]);
		}
		Collections.shuffle(listeDePoints);
		for(CubePoint p : listeDePoints) {
			BuildingPoint bp = new BuildingPoint(new Building(BuildingType.TEMPLE, getColor(), colorName), p);
			if(gc.board.canPlaceTemple(bp) == ReasonBuilding.OK && hand.getTemples() != 0) {
				currentBuilding=bp;
				super.placeBuildings();
				return;
			}
		}
		
		for(CubePoint p : listeDePoints) {
			BuildingPoint bp = new BuildingPoint(new Building(BuildingType.TOWER, getColor(), colorName), p);
			if(gc.board.canPlaceTower(bp) == ReasonBuilding.OK && hand.getTowers() != 0) {
				currentBuilding=bp;
				super.placeBuildings();
				return;
			}
		}
		
		int randomNum = Math.random() < 0.5 ? 1 : 2;
	    if(randomNum == 1) {
	    	for(CubePoint p : listeDePoints) {
	    		BuildingPoint bp = new BuildingPoint(new Building(BuildingType.HUTT, getColor(), colorName), p);
				if(gc.board.canPlaceHutt(bp) == ReasonBuilding.OK && hand.getHutts() != 0) {
					currentBuilding=bp;
					super.placeBuildings();
					return;
				}
	    	}

	    	for(CubePoint p : listeDePoints) {
	    		BuildingPoint bp = new BuildingPoint(new Building(BuildingType.HUTT, getColor(), colorName), p);
				City c = gc.board.canExtendCity(bp);
				if(c != null && hand.hasEnough(bp, c)) {
					currentCity = c;
					currentBuilding=bp;
					super.placeBuildings();
					return;
				}
	    	}
	    	
	    }
	    
	    
	    if(randomNum == 2) {
	    	for(CubePoint p : listeDePoints) {
	    		BuildingPoint bp = new BuildingPoint(new Building(BuildingType.HUTT, getColor(), colorName), p);
				City c = gc.board.canExtendCity(bp);
				if(c != null&& hand.hasEnough(bp, c)) {
					currentCity = c;
					currentBuilding=bp;
					super.placeBuildings();
					return;
				}
	    	}

	    	for(CubePoint p : listeDePoints) {
	    		BuildingPoint bp = new BuildingPoint(new Building(BuildingType.HUTT, getColor(), colorName), p);
				if(gc.board.canPlaceHutt(bp) == ReasonBuilding.OK && hand.getHutts() != 0) {
					currentBuilding=bp;
					super.placeBuildings();
					return;
				}
	    	}
	    	
	    }
		
	}
	
	public CubePoint cube_Nachbar(CubePoint cube, int direction)
	{
		return cube.getNeighbours().get(direction);
	}
	
	public ArrayList<CubePoint> ring(CubePoint center,int deep ){
		
		ArrayList<CubePoint> points=new ArrayList<CubePoint>();
		if(deep>0) {
						
			CubePoint cube=cube_Nachbar(center,4);
			cube.setCoordinates(cube.getX()*deep, cube.getY()*deep, cube.getZ()*deep);
			
			for (int i = 0;i<6;i++) {
			
				for (int j=0;j<deep;j++)
				{
					points.add(new CubePoint(cube.getX(), cube.getY(), cube.getZ()));
					cube=cube_Nachbar(cube,i);
				}
			}
			
		}
		return points;

	}
	
	public int ringslotfree(CubePoint center){
		
		int count=0;
		ArrayList<CubePoint> points=ring(center,1); //ring recupere un aneau
		for (int i=0;i<points.size();i++)
		{
			if (gc.board.getTile(points.get(i))==null)
				count++;
		}
		return count;
	}
	
}
