package AI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import controller.GameController;
import controller.Playable;
import controller.Player;

import javafx.util.Pair;
import model.CubePoint;
import model.Hand;
import model.Piece;
import model.PiecePoint;
import model.Tile;
import pattern.Tree.Node;
import model.Board.ReasonBuilding;
import model.Board;
import model.Building;
import model.Building.BuildingType;
import view.InterfaceGraphique;
import model.BuildingPoint;
import model.City;
import javafx.scene.paint.Color;

public class MAY  extends Player implements Playable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int DEEP = 1;
	protected int NBPlayer;
	
	private BuildingPoint nextbuilding;
	private HashMap<String,String> plop=new HashMap<String,String>();
	private boolean heuristic = true;
	
	public MAY(String name, Color color, String colorName,boolean heur) {
		super(name, color, colorName);
		heuristic = heur;
		
	}

	@Override
	public void startTurn() {
		NBPlayer=gc.getPlayers().size();
		super.startTurn();
		InterfaceGraphique.soundAvailable = false;
		currentPiece = null;
		nextbuilding=null;
		GameController g=(GameController) gc.clone();
		g.currentPlayer=g.currentPlayer-1;
		Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer> p=recur(DEEP,new Node<GameController>(g),Integer.MIN_VALUE,Integer.MAX_VALUE);		
		gc.drawPieceAction();
		if(p != null) {
			currentPiece.setPoints(p.getKey().getValue().getKey().points);
			nextbuilding=p.getKey().getValue().getValue();
		}else {
			nextbuilding = new BuildingPoint(BuildingType.HUTT,getColor(),currentPiece.points[0],getColorName());
		}
			
		placePieces();
	    
	    
	}
	
	@Override
	public void placePieces() {
		super.placePieces();
	}
	
	@Override
	public void placeBuildings() {
		currentBuilding=this.nextbuilding;
		if (gc.board.canExtendCity(nextbuilding)!=null)
        {
			currentCity=gc.board.canExtendCity(nextbuilding);
        }
		else currentCity=null;
		super.placeBuildings();
	}

	@Override
	public void endTurn() {
		plop=new HashMap<String,String>();
		super.endTurn();
	}
	
	
	
	public Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer> recur(int deep,Node<GameController> node,int alpha, int beta)
	{
		GameController g = node.getData();
		if (deep > 0  && !g.endGameCondition(g.getPlayers(), (g.currentPlayer+1)%NBPlayer, g.getDeck(),2))
		{
			g.currentPlayer=(g.currentPlayer+1)%NBPlayer;
			Piece currentp=g.getDeck().drawPiece();
			PiecePoint p= new PiecePoint();
			ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>> li = new ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>>();
			Iterator<Entry<CubePoint, Tile>> it = g.board.tiles.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<CubePoint, Tile> pair = (Map.Entry<CubePoint, Tile>)it.next();
		        ArrayList<CubePoint> crown = ring((CubePoint)pair.getKey(),1);
				for (int i = 0 ;i<crown.size();i++)
				{					
					p= new PiecePoint();
					p.setPiece((Piece) currentp.clone()); 
					p.points= new CubePoint[] {new CubePoint(crown.get(i).getX()-1,crown.get(i).getY(),crown.get(i).getZ()+1),crown.get(i),new CubePoint(crown.get(i).getX(),crown.get(i).getY()-1,crown.get(i).getZ()+1)};
					
					for (int j=0;j<6;j++)
					{
						p.turnPoint();						
						if(g.board.canInsertPiece((PiecePoint) p.clone())==Board.ReasonPiece.OK)
						{
							if(!plop.containsKey(p.toString()))
							{
								plop.put(p.toString(), "lpop");
								GameController newg=(GameController) g.clone();
							
								if(newg.board.addPiece((PiecePoint)p.clone())==Board.ReasonPiece.OK)
								{
									jouerconstruction(newg,li,(PiecePoint)p.clone(),deep);
								}
							}
						}
					}
				}
		        
		    }
		    
		    Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer> max = new Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>,Integer>(null, Integer.MIN_VALUE);
		    Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer> min = new Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>,Integer>(null, Integer.MAX_VALUE);
		    if (deep%NBPlayer==1)
	    	{
		    	if(li.size() == 0) {
		    		return null;
		    	}
		    	boolean hasFoundSomething = false;

			    for (Pair<GameController, Pair<PiecePoint, BuildingPoint>> current : li) {
			    	Node<GameController> child = node.addChild(current.getKey());
			    	Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>,Integer> recurResult = recur(deep-1,child,alpha,beta);
	    			if(recurResult == null) {
	    				continue;
	    			}
	    			hasFoundSomething = true;

			    	Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>,Integer> result = new Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer> (current,recurResult.getValue());
			    	if (max.getValue() < result.getValue())
				    {
				   		max = result;
				   	}

			    	if(alpha < max.getValue())
			    		alpha=max.getValue();
			    	
			    	if(alpha >= beta) {
			    		break;
			    	}
			    }
			    return hasFoundSomething ? max : null;
	    	}
		    else 
		    {
		    	if(li.size() == 0) {
		    		return null;
		    	}
		    	boolean hasFoundSomething = false;
		    	for (Pair<GameController, Pair<PiecePoint, BuildingPoint>> current : li) {
		    		Node<GameController> child = node.addChild(current.getKey());
		    		Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>,Integer> recurResult = recur(deep-1,child,alpha,beta);
	    			if(recurResult == null) {
	    				continue;
	    			}
	    			hasFoundSomething = true;
	    			Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>,Integer> result = new Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer> (current,recurResult.getValue());
			    	if (min.getValue() > result.getValue())
				    {
				   		min = result;
				   	}
			    	
			    	if(beta > min.getValue())
			    		beta = min.getValue();
			    	if(alpha >= beta)	 {
			    		break;
			    	}	
			    }    
			    return hasFoundSomething ? min : null;
		    }	
		} else {
			//heuristique	
			int heur=0;
			if(heuristic) {
				heur = heur2(node,getColor());
			}else {
				heur = heuristicComplex(node, getColor());
			}
			return new Pair<Pair<GameController,Pair<PiecePoint,BuildingPoint>>, Integer>(new Pair<GameController,Pair<PiecePoint,BuildingPoint>>(g,null),heur);
		}
	}
	
	public int heur2(Node<GameController> leaf,Color c)
	{
		return (
				(Hand.HUTTS - (leaf.getData().getCurrentPlayer().getHand().getHutts())) + 
				(Hand.TEMPLES - (leaf.getData().getCurrentPlayer().getHand().getTemples())) *10 + 
				(Hand.TOWERS - (leaf.getData().getCurrentPlayer().getHand().getTowers())) * 5);
	}
	
	public int heuristicComplex(Node<GameController> leaf,Color c)
	{
		GameController gc = leaf.getData();
		int result = 0;
		if(gc.gameEnded) {
			if(gc.winner == gc.getPlayers().get(gc.currentPlayer))
				result = Integer.MAX_VALUE;
			else {
				result = Integer.MIN_VALUE;
			}
		}else {
			for (Player player : gc.getPlayers()) {
				int hutts =  Hand.HUTTS - player.getHand().getHutts();
				int towers = (Hand.TOWERS - player.getHand().getTowers())*2000;
				int temples = (Hand.TEMPLES - player.getHand().getTemples())*1500;
				if(player == gc.getPlayers().get(gc.currentPlayer)) {
					result = result + towers + temples + hutts;
				}else {
					result = result - towers - temples - hutts;
				}
			}
			
			//compter cités
			for (City city : gc.board.cities) {
				double coef = 1;
				if(city.getBuildingPoints().get(0).getColor() != c) {
					coef = -1;
				}
				if(!city.getTemple()) {
					if(city.getBuildingPoints().size() == 2)
						coef *= 10;
					if(city.getBuildingPoints().size() == 3)
						coef *= 30;
					if(city.getBuildingPoints().size() <= 3) {
						
						result = (int) (result+coef);
					} else {
						result = (int) (result+coef-3);
					}
				}
				
			}
			
		}
 		return result;		
	}
	
	public void jouerconstruction(GameController g,ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>> li,PiecePoint p,int deep)
	{
		Color color = gc.getPlayers().get(g.getCurrentPlayernumber()).getColor();
		String colorName = gc.getPlayers().get(g.getCurrentPlayernumber()).getColorName();
		Iterator<Entry<CubePoint, Tile>> it = g.board.tiles.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<CubePoint, Tile> pair = (Map.Entry<CubePoint, Tile>)it.next();
	        BuildingPoint bp = new BuildingPoint(Building.BuildingType.HUTT, color, (CubePoint)((CubePoint)pair.getKey()),colorName);
	        //normal hutt
	        if (g.board.canPlaceHutt(bp) == ReasonBuilding.OK && g.getCurrentPlayer().getHand().hasEnough(bp, null))
	        {
	        	applique((GameController)g.clone(),bp,p,li);
	        }
	        //extend city
	        City c = g.board.canExtendCity(bp);
	        if (c !=null && g.board.countExtendCityHutts(bp,c)< g.getCurrentPlayer().getHand().getHutts())
	        {
	        	appliquecity((GameController)g.clone(),bp,p,li);
	        }
	        //tower
	        bp = new BuildingPoint(Building.BuildingType.TOWER, color, (CubePoint)pair.getKey(),colorName);
	        if (g.board.canPlaceTower(bp) == ReasonBuilding.OK && g.getCurrentPlayer().getHand().hasEnough(bp, null))
	        {
	        	appliqueTower((GameController)g.clone(),bp,p,li);
	        }
	        //temple
	        bp = new BuildingPoint(Building.BuildingType.TEMPLE, color, (CubePoint)pair.getKey(),colorName);
	        if (g.board.canPlaceTemple(bp) == ReasonBuilding.OK && g.getCurrentPlayer().getHand().hasEnough(bp, null))
	        {
	        	appliqueTemple((GameController)g.clone(),bp,p,li);
	        }
	    }
	}
	
	public void applique(GameController g,BuildingPoint bp,PiecePoint p,ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>> li)
	{
		if (g.board.canPlaceHutt(bp)==ReasonBuilding.OK)
        {
			BuildingPoint newBuilding = new BuildingPoint(bp, (CubePoint)bp.point.clone());
			g.board.addBuilding(newBuilding,null);
			g.getCurrentPlayer().getHand().setHutts(g.getCurrentPlayer().getHand().getHutts()-1);
			li.add(new Pair<GameController,Pair<PiecePoint,BuildingPoint>>(
					g,new Pair<PiecePoint,
					BuildingPoint>(p,newBuilding)
					));
        }
	}
	
	
	public void appliqueTemple(GameController g,BuildingPoint bp,PiecePoint p,ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>> li)
	{
		if (g.board.canPlaceTemple(bp)==ReasonBuilding.OK)
        {
			BuildingPoint newBuilding = new BuildingPoint(bp, (CubePoint)bp.point.clone());
			g.board.addBuilding(newBuilding,null);
			g.getCurrentPlayer().getHand().setTemples(g.getCurrentPlayer().getHand().getTemples()-1);
			li.add(new Pair<GameController,Pair<PiecePoint,BuildingPoint>>(
					g,
					new Pair<PiecePoint,BuildingPoint>(
							p,
							newBuilding)));
        }
	}
	
	public void appliqueTower(GameController g,BuildingPoint bp,PiecePoint p,ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>> li)
	{
		if (g.board.canPlaceTower(bp) == ReasonBuilding.OK)
        {
			BuildingPoint newBuilding = new BuildingPoint(bp, (CubePoint)bp.point.clone());
			g.board.addBuilding(newBuilding,null);
			g.getCurrentPlayer().getHand().setTowers(g.getCurrentPlayer().getHand().getTowers()-1);
			li.add(new Pair<GameController,Pair<PiecePoint,BuildingPoint>>(
					g,
					new Pair<PiecePoint,BuildingPoint>(p,newBuilding)));
        }
	}
	
	
	public void appliquecity(GameController g,BuildingPoint bp,PiecePoint p,ArrayList<Pair<GameController,Pair<PiecePoint,BuildingPoint>>> li)
	{
		City city = g.board.canExtendCity(bp);
		if (city != null)
        {
			g.getCurrentPlayer().getHand().setHutts( g.getCurrentPlayer().getHand().getHutts() - g.board.countExtendCityHutts(bp,city));
			BuildingPoint newBuilding = new BuildingPoint(bp, (CubePoint)bp.point.clone());
			if(g.board.addBuilding(newBuilding,city) == ReasonBuilding.OK)
			{
				li.add(new Pair<GameController,Pair<PiecePoint,BuildingPoint>>(
						g,new Pair<PiecePoint,BuildingPoint>(p,newBuilding)));
			}			
        }
	}
	
	public CubePoint cube_Nachbar(CubePoint cube, int direction)
	{
		return cube.getNeighbours().get(direction);
	}
	
	public ArrayList<CubePoint> ring(CubePoint center,int deep ){
		
		ArrayList<CubePoint> points=new ArrayList<CubePoint>();
		if(deep>0) {
						
			CubePoint cube=cube_Nachbar(center,4);
			cube.setCoordinates(cube.getX()*deep, cube.getY()*deep, cube.getZ()*deep);
			
			for (int i = 0;i<6;i++) {
			
				for (int j=0;j<deep;j++)
				{
					points.add(new CubePoint(cube.getX(), cube.getY(), cube.getZ()));
					cube=cube_Nachbar(cube,i);
				}
			}
			
		}
		return points;
	}
	
}
