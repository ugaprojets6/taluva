package AI;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import controller.GameController;
import controller.Player;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import model.SavefileManager;
import taluva.Configuration;

public class AIStatistics extends Application {

	private int nbGames = 0;
	private int AI1Win = 0;
	private int AI2Win = 0;
	int nbGamesToPlay = 10;
	Timer timer = new Timer();
	
	List<Player> players = new ArrayList<Player>();
	GameController gc = new GameController(players, null);
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		addPlayers();
		
		NextGame();
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	private void addPlayers() {
		players.clear();
//		players.add(new AIR("easy ", Configuration.instance().lisCouleur("purple"),"purple"));
		players.add(new MAY("Moyenne", Configuration.instance().lisCouleur("blue"),"blue",true));
		players.add(new MAY("Difficile", Configuration.instance().lisCouleur("red"),"red",false));
		
	}
	
	public void NextGame() {
		if(gc.gameEnded) {
			if(gc.winner != null) {
				if(gc.winner == players.get(0)) {
					AI1Win++;
				}else if(gc.winner == players.get(1)){
					AI2Win++;
				}
			}
			System.out.flush();  
			double AI1Percent = (double) ((double)((double)AI1Win/(double)nbGamesToPlay)*100.0);
			double AI2Percent = (double) ((double)((double)AI2Win/(double)nbGamesToPlay)*100.0);
			System.out.println(gc.winnerText);
			System.out.println(players.get(0)+ " won "+AI1Win+" times with "+AI1Percent+"% success rate");
			System.out.println(players.get(1)+ " won "+AI2Win+" times with "+AI2Percent+"% success rate");
			System.out.println();
			SavefileManager sm = new SavefileManager("ai_test_" + nbGames + ".tlv");
			gc.setSaveName(sm.getSaveDirectory().getAbsolutePath() + "/ai_test_" + nbGames + ".tlv");
			sm.save(gc);
		}
		
		if(nbGames < nbGamesToPlay) {
			addPlayers();
			gc = new GameController(players, null);
			timer = new Timer();
			TimerTask task = new TimerTask() {
				@Override
				public void run() {
					if(gc.gameEnded) {
						Platform.runLater(new Runnable() {
						      @Override public void run() {
						    	  NextGame();  
						      }
						    });
						timer.cancel();
					}
				}
			};
			timer.schedule(task,0, 1000);
			nbGames++;
			gc.startPlaying();
		}else {
			System.out.println();
			System.out.println();
			System.out.println("End");
			double AI1Percent = (double) ((double)((double)AI1Win/(double)nbGamesToPlay)*100.0);
			double AI2Percent = (double) ((double)((double)AI2Win/(double)nbGamesToPlay)*100.0);
			System.out.println(players.get(0)+ " won "+AI1Win+" times with "+AI1Percent+"% success rate");
			System.out.println(players.get(1)+ " won "+AI2Win+" times with "+AI2Percent+"% success rate");
			timer.cancel();
			Platform.exit();
			System.exit(0);
		}
	}
}
