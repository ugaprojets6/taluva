package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.CubePoint;

class CubePointTest {

	/*
	 * Test to check that a cubepoint is adjacent to an other
	 */
	@Test
	void testAdjacentPoint() {
		CubePoint p1 = new CubePoint(0,0,0);
		CubePoint p2 = new CubePoint(1,-1,0);
		assertTrue(p1.isAdjacent(p2));
	}
	
	/*
	 * Test to check that a cubepoint is not adjacent to an other
	 */
	@Test
	void testNotAdjacentPoint() {
		CubePoint p1 = new CubePoint(0,0,0);
		CubePoint p2 = new CubePoint(8,-8,0);
		assertFalse(p1.isAdjacent(p2));
	}

}
