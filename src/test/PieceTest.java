package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import model.Piece;
import model.Ground;

class PieceTest {
	
	/*
	 * Test to throw a null exception when trying
	 * to access a ground from a piece
	 */
	@Test
	void testGetGround() {
		Piece p;
		p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE));
		assertThrows(IndexOutOfBoundsException.class, () -> {
	        p.getGround(6);
	    });
		assertEquals(Ground.GroundType.GRASS, p.getGround(0).getType());
		
	}

}
