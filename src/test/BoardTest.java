package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import model.Board;
import model.Board.ReasonBuilding;
import model.Board.ReasonPiece;
import model.Building;
import model.Building.BuildingType;
import model.BuildingPoint;
import model.City;
import model.CubePoint;
import model.Ground;
import model.Piece;
import model.PiecePoint;
import model.Tile;
import taluva.Configuration;
import model.Ground.GroundType;

public class BoardTest {

	/*
	 * Test to add a normal piece at point 0,0,0 
	 * when the board is empty
	 */
	@Test
	void testAddPieceEmptyBoard() {
		Board b = new Board();
		
	
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(0,0,0),
						new CubePoint(1,-1,0),
						new CubePoint(0,-1,1)
						},
				new Piece(
						new Ground(Ground.GroundType.GRASS),
						new Ground(Ground.GroundType.LAKE))
				);
		ReasonPiece result = b.addPiece(
					point
				);
		assertEquals(ReasonPiece.OK, result);
		try {
			assertEquals(GroundType.VOLCANO, b.getTile(point.points[1]).getGround().getType());
		} catch (NullPointerException e) {
			fail("tile does not exist");
		}
		
	}
	
	/*
	 * Test checking insertion of a piece that it's position
	 * covers part of an existing piece on the board ( in position 0,0,0)
	 */
	@Test
	void testCanInsertWrongPosition() {
		Board b = new Board();
		
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(0,0,0),
						new CubePoint(1,-1,0),
						new CubePoint(0,-1,1)
						},
				new Piece(
						new Ground(Ground.GroundType.GRASS),
						new Ground(Ground.GroundType.LAKE))
				);
		b.addPiece(point);
		ReasonPiece result =  b.canInsertPiece(
				new PiecePoint(
							new CubePoint[] {
							new CubePoint(1,-1,0),
							new CubePoint(0,-1,1),
							new CubePoint(1,-2,1)
						},
						new Piece(
							new Ground(Ground.GroundType.GRASS),
							new Ground(Ground.GroundType.LAKE))
				));
		assertEquals(ReasonPiece.VOLCANO_WRONG_PLACE, result);
	
	}
	
	/*
	 * Test to check a normal piece at point 0,0,0 
	 * when the board is empty
	 */
	@Test
	void testCanInsertPieceEmpty() {
		Board b = new Board();
		PiecePoint point = new PiecePoint(new CubePoint[] {
				new CubePoint(0,0,0),
				new CubePoint(1,-1,0),
				new CubePoint(0,-1,1)
				});
	
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE));
		point.setPiece(p);
		ReasonPiece result = b.canInsertPiece(point);
		assertEquals(ReasonPiece.OK, result);
			
		
	}
	
	/*
	 * Test to check that a piece that has the volcano
	 * in the same direction as the board that cannot be placed
	 */
	@Test
	void testCanInsertPieceVolcanoSameDirection() {
		Board b = new Board();
		PiecePoint point = new PiecePoint(new CubePoint[] {
				new CubePoint(0,0,0),
				new CubePoint(1,-1,0),
				new CubePoint(0,-1,1)
				});
		
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE));
		point.setPiece(p);
		b.addPiece(point);
		point.setPiece(new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.GRASS)));
		ReasonPiece result = b.canInsertPiece(
				point
				);
		assertEquals(ReasonPiece.VOLCANO_WRONG_DIRECTION, result);
	}
		
	
	/*
	 * Test to check that a piece that has the volcano
	 * in a different direction as the board that can be placed
	 */
	@Test
	void testCanInsertPieceVolcanoDifferentDirection() {
		Board b = new Board();
		PiecePoint pointInit = new PiecePoint(new CubePoint[] {
				new CubePoint(-1,0,1),
				new CubePoint(0,0,0),
				new CubePoint(0,-1,1)
				});
		PiecePoint pointAdjacent = new PiecePoint(new CubePoint[] {
				new CubePoint(1,-2,1),
				new CubePoint(1,-1,0),
				new CubePoint(2,-2,0)
				});
		PiecePoint point = new PiecePoint(new CubePoint[] {
				new CubePoint(0,-1,1),
				new CubePoint(1,-1,0),
				new CubePoint(1,-2,1)
				});
		
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE));
		Piece p2 = new Piece(
			new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.GRASS));
		pointInit.setPiece(p);
		pointAdjacent.setPiece(p2);
		b.addPiece(pointInit);
		b.addPiece(pointAdjacent);
		point.setPiece(p2);
		ReasonPiece result = b.canInsertPiece(point);
		point.setPiece(p2);
		assertEquals(result, b.addPiece(point));
		assertEquals(ReasonPiece.OK, result);
		
		
	}
	
	/*
	 * Test to not let a piece that is not adjacent to a
	 * tile in the board
	 */
	@Test
	void testHasNoAdgency() {
		Board b = new Board();
	
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(0,0,0),
						new CubePoint(1,-1,0),
						new CubePoint(0,-1,1)
						},
				new Piece(
						new Ground(Ground.GroundType.GRASS),
						new Ground(Ground.GroundType.LAKE))
				);
		b.addPiece(point);
		ReasonPiece result =  b.canInsertPiece(
				new PiecePoint(
						new CubePoint[] {
							new CubePoint(-1,3,-2),
							new CubePoint(-2,3,-1),
							new CubePoint(-1,2,-1)
						},
						new Piece(
							new Ground(Ground.GroundType.GRASS),
							new Ground(Ground.GroundType.LAKE))
				));
		assertEquals(ReasonPiece.NOT_ADJACENT, result);
		
	}
	
	/*
	 * Test to let a piece that is adjacent to a
	 * tile in the board
	 */
	@Test
	void testHasAdgency() {
		Board b = new Board();
		
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(0,0,0),
						new CubePoint(1,-1,0),
						new CubePoint(0,-1,1)
						},
				new Piece(
						new Ground(Ground.GroundType.GRASS),
						new Ground(Ground.GroundType.LAKE))
				);
		b.addPiece(point);
		ReasonPiece result =  b.canInsertPiece(
				new PiecePoint(
						new CubePoint[] {
							new CubePoint(0,2,-2),
							new CubePoint(0,1,-1),
							new CubePoint(1,1,-2)
						},
						new Piece(
							new Ground(Ground.GroundType.GRASS),
							new Ground(Ground.GroundType.LAKE))
						)
				);
		assertEquals(ReasonPiece.OK, result);
	}
	
	/*
	 * testing for phase 2
	 */
	
	/*
	 * Tests one hutt placement
	 */
	@Test
	void PlaceOneHutt() {
		Board b = new Board();
		
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(0,0,0),
						new CubePoint(1,-1,0),
						new CubePoint(0,-1,1)
						},
				new Piece(
						new Ground(Ground.GroundType.GRASS),
						new Ground(Ground.GroundType.LAKE))
				);
		b.addPiece(point);
		assertTrue(b.addBuilding(new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(0,0,0), "blue"),
				null)==ReasonBuilding.OK);
	}
	
	/*
	 * Tests one hutt placement
	 */
	@Test
	void PlaceTower() {
		Board b = createBoardForTower();
		b.addBuilding(new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(0,-1,1), "blue"),
				null);
		BuildingPoint bp = new BuildingPoint(
				BuildingType.TOWER,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(-1,0,1), "blue");
		BuildingPoint bphutt = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(0,-1,1), "blue");
		assertTrue(b.addBuilding(bp,
				b.getCityFromBuildingPoint(bphutt))==ReasonBuilding.OK);
	}
	
	@Test
	void PlaceTemple() {
		Board b = new Board();
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.GRASS)
		);
		
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,0,1),
						new CubePoint(0,0,0),
						new CubePoint(0,-1,1)
						},
						p);

		b.addPiece(point);
		
		point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-3,2,1),
						new CubePoint(-2,2,0),
						new CubePoint(-2,1,1)
						},
						p);
		b.addPiece(point);
		
		b.getTile(new CubePoint(0,-1,1)).setBuilding(new Building(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), "blue"));
		b.getTile(new CubePoint(-1,0,1)).setBuilding(new Building(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), "blue"));
		b.getTile(new CubePoint(-2,1,1)).setBuilding(new Building(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), "blue"));
	
		City c = new City(new BuildingPoint(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), new CubePoint(0,-1,1), "blue"));
		c.addHutt(new BuildingPoint(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), new CubePoint(-1,0,1), "blue"));
		c.addHutt(new BuildingPoint(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), new CubePoint(-2,1,1), "blue"));
		
		b.cities.add(c);
		
		assertTrue(b.addBuilding(new BuildingPoint(BuildingType.TEMPLE, Configuration.instance().lisCouleur("blue"), new CubePoint(-3,2,1), "blue"), c)==ReasonBuilding.OK);
	}
	
	@Test
	void PlaceTempleCityTooLittle() {
		Board b = new Board();
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.GRASS)
		);
		
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,0,1),
						new CubePoint(0,0,0),
						new CubePoint(0,-1,1)
						},
						p);

		b.addPiece(point);
		
		point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-3,2,1),
						new CubePoint(-2,2,0),
						new CubePoint(-2,1,1)
						},
						p);
		b.addPiece(point);
		
		b.getTile(new CubePoint(0,-1,1)).setBuilding(new Building(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), "blue"));
		b.getTile(new CubePoint(-2,1,1)).setBuilding(new Building(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), "blue"));
	
		City c = new City(new BuildingPoint(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), new CubePoint(0,-1,1), "blue"));
		c.addHutt(new BuildingPoint(BuildingType.HUTT, Configuration.instance().lisCouleur("blue"), new CubePoint(-2,1,1), "blue"));
		
		b.cities.add(c);
		
		assertFalse(b.addBuilding(new BuildingPoint(BuildingType.TEMPLE, Configuration.instance().lisCouleur("blue"), new CubePoint(-3,2,1), "blue"), c)==ReasonBuilding.OK);
	}
	
	@Test
	void PlaceExtendsCity() {
		Board b = new Board();
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.GRASS)
		);
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,0,1),
						new CubePoint(0,0,0),
						new CubePoint(0,-1,1)
						},
						p);
		b.addPiece(point);
	
		point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,-1,2),
						new CubePoint(-2,0,2),
						new CubePoint(-2,-1,3)
						},
						p);
		b.addPiece(point);
		
		BuildingPoint bpinit = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(0,-1,1), "blue");
		b.addBuilding(bpinit,null);
		BuildingPoint bp = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(-1,0,1), "blue");
		assertTrue(b.addBuilding(bp,b.getCityFromBuildingPoint(bpinit))==ReasonBuilding.OK);
		assertTrue(b.getTile(new CubePoint(-1,-1,2)).hasBuilding());
		
	}
	
	public static Board createBoardForTower() {
		Board b = new Board();
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE)
		);
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,0,1),
						new CubePoint(0,0,0),
						new CubePoint(0,-1,1)
						},
						p);
		b.addPiece(point);
		Tile t = b.getTile(new CubePoint(-1,0,1));
		t.height = 3;
		return b;
	}
	
	@Test
	void mergeCities() {
		Board b = new Board();
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE)
		);
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,0,1),
						new CubePoint(0,0,0),
						new CubePoint(0,-1,1)
						},
						p);
		b.addPiece(point);
		
		point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(1,-2,1),
						new CubePoint(1,-1,0),
						new CubePoint(2,-2,0)
						},
						p);
		b.addPiece(point);
		
		BuildingPoint bpinit = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(-1,0,1), "blue");
		
		BuildingPoint bpinit2 = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(1,-2,1), "blue");
		
		BuildingPoint bpverif = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(0,-1,1), "blue");
		
		b.addBuilding(bpinit,null);
		b.addBuilding(bpinit2,null);
		
		assertTrue(b.addBuilding(bpverif,b.getCityFromBuildingPoint(bpinit2))==ReasonBuilding.OK);
		ArrayList<City> cities = b.getCitiesOfColor(Configuration.instance().lisCouleur("blue"));
		assertEquals(1, cities.size());
	}
	
	@Test
	void exploreCityTest() {
		Board b = new Board();
		Piece p = new Piece(
				new Ground(Ground.GroundType.GRASS),
				new Ground(Ground.GroundType.LAKE)
		);
		PiecePoint point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(-1,0,1),
						new CubePoint(0,0,0),
						new CubePoint(0,-1,1)
						},
						p);
		b.addPiece(point);
		
		point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(1,-2,1),
						new CubePoint(1,-1,0),
						new CubePoint(2,-2,0)
						},
						p);
		b.addPiece(point);
		
		BuildingPoint bpinit = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(-1,0,1), "blue");
		
		BuildingPoint bpinit2 = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(0,-1,1), "blue");
		
		BuildingPoint bpinit3 = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(1,-2,1), "blue");
		
		BuildingPoint bpinit4 = new BuildingPoint(
				BuildingType.HUTT,
				Configuration.instance().lisCouleur("blue"),
				new CubePoint(2,-2,0), "blue");
		
		b.addBuilding(bpinit, null);
		b.addBuilding(bpinit2, b.getCityFromBuildingPoint(bpinit));
		b.addBuilding(bpinit3, b.getCityFromBuildingPoint(bpinit));
		b.addBuilding(bpinit4, b.getCityFromBuildingPoint(bpinit));
		
		point = new PiecePoint(
				new CubePoint[] {
						new CubePoint(0,-1,1),
						new CubePoint(1,-1,0),
						new CubePoint(1,-2,1)
						},
						p);
		b.addPiece(point);
		
		assertEquals(b.cities.size(),2);
	}
	
}
