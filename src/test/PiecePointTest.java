package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.CubePoint;
import model.PiecePoint;

class PiecePointTest {

	/*
	 * Tests that a piece in the center of the board
	 * is turnable
	 */
	@Test
	void testTurnPoint() {
		PiecePoint p = new PiecePoint(new CubePoint[] {
				new CubePoint(-1, 0, 1),
				new CubePoint(0, 0, 0),
				new CubePoint(0, -1, 1)
		});
		p.turnPoint();
		assertTrue(p.checkPoints());
	}
	
	/*
	 * Checks that a piece not at the center of the board
	 * is turnable
	 */
	@Test
	void testTurnPointNotNormalPoint() {
		PiecePoint p = new PiecePoint(new CubePoint[] {
				new CubePoint(0, -1, 1),
				new CubePoint(1, -1, 0),
				new CubePoint(1, -2, 1)
		});
		p.turnPoint();
		assertTrue(p.checkPoints());
	}
}
