package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Piece;
import model.PieceDeck;

class PieceDeckTest {

	/*
	 * Test to create a random deck and draw it
	 */
	@Test
	void testCreateDeck() {
		PieceDeck deck = new PieceDeck(100, true);
		while(!deck.isEmpty()) {
			Piece p = deck.drawPiece();
			System.out.println(p.getGround(0).getType());
			System.out.println(p.getGround(1).getType());
			System.out.println(p.getGround(2).getType());
			System.out.println();
		}
		assertTrue(true);
	}

}
