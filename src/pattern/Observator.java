package pattern;

public interface Observator {
	void update();
}
