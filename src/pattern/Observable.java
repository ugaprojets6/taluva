package pattern;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Observable {
	List<Observator> observators;

	public Observable() {
		observators = new ArrayList<>();
	}

	public void addObservator(Observator o) {
		observators.add(o);
	}

	public void update() {
		Iterator<Observator> it;

		it = observators.iterator();
		while (it.hasNext()) {
			Observator o = it.next();
			o.update();
		}
	}
}
