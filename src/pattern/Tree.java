package pattern;

import java.util.ArrayList;
import java.util.List;


public class Tree<T> {
    private Node<T> root;

    public Tree(T rootData) {
        root = new Node<T>(rootData);
        root.data = rootData;
        root.children = new ArrayList<Node<T>>();
    }

    public Node<T> getRoot(){
        return root;
    }
    
    public static class Node<T> {
        private T data;
        private Node<T> parent;
        private List<Node<T>> children;
        
        public Node(T first) {
            children = new ArrayList<Node<T>>();
            data = first;
		}
    
        public List<Node<T>> getChildren(){
            return children;
        }
        
        public Node<T> getParent(){
        	return parent;
        }

        public T getData(){
            return data;
        }
        
        public Node<T> addChild(T child) {
        	Node<T> node = new Node<T>(child);
        	node.parent = this;
        	children.add(node);
        	return node;
        }
    }
}
