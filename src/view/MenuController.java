package view;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import taluva.Configuration;

public class MenuController implements ControlledScreen{
	ScreensController controller;

	@FXML
	public Button parametersButton;

	@FXML
	public Button loadGameButton;

	@FXML
	public GridPane menuPane;

	@FXML
	public Button quitButton;

	@FXML
	public Button tutorialButton;

	@FXML
	public Button newGameButton;

	/**
     * Set the screen to New Game menu view
     * @param event the click {@link ActionEvent event}
     */
	@FXML
	void newGameAction(ActionEvent event) {
		// Start new game
		controller.setScreen(InterfaceGraphique.newGame);
	}
	
	/**
     * Set the screen to load game view
     * @param event the click {@link ActionEvent event}
     */
	@FXML
    void loadGameAction(ActionEvent event) {
		// Load Game
		controller.setScreen(InterfaceGraphique.loadGame);
		LoadGameController.fillSavedGames();	
	}

	/**
     * Set the screen to rules view
     * @param event the click {@link ActionEvent event}
     */
	@FXML
	void tutorialAction(ActionEvent event){
		// Show the tutorial menu
		controller.setScreen(InterfaceGraphique.tutorial);
	}

	/**
     * Set the screen to settings view
     * @param event the click {@link ActionEvent event}
     */
	@FXML
	void parametersAction(ActionEvent event) {
		// Access to parameters
		controller.setScreen(InterfaceGraphique.settings);
	}

	/**
     * Quit the game
     * @param event the click {@link ActionEvent event}
     */
	@FXML
	void quitAction(ActionEvent event) {
		// Quit the Game
        Platform.exit();
        System.exit(0);
	}

	/**
	 * Set parent Screen
	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
	}

	/**
	 * Update the view
	 */
	@Override
	public void update() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Change the language of elements in the view
		parametersButton.setText(bundle.getString("settings"));
		loadGameButton.setText(bundle.getString("loadGame"));
		quitButton.setText(bundle.getString("quit"));
		tutorialButton.setText(bundle.getString("rules"));
		newGameButton.setText(bundle.getString("newGame"));
		
	}

}

