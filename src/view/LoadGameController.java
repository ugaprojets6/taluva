package view;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.GameController;
import controller.Player;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.SavefileManager;
import taluva.Configuration;

public class LoadGameController implements ControlledScreen, Initializable{
	ScreensController controller;
	
	boolean pause = false;
	
    @FXML
    private VBox VBox_LoadGame;
    @FXML
    private ImageView image_load;
    
    @FXML
    private Button Button_return;
    @FXML
    private Button button_delete;
    @FXML
    private Button button_load;

    @FXML
    private ListView<String> ListView_Infos;

    @FXML
    private HBox HBox_LoadGame;

    @FXML
    private ListView<String> ListView_SavedGames;

    @FXML
    private HBox HBox_LoadGameButtons;
    
    @FXML
    public static void fillSavedGames() {
    	
    }
    
	/**
     * Set the view during the initialization
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initializeListView();
		
		ListView_SavedGames.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				if (newValue != null) {					
					SavefileManager m = new SavefileManager(newValue + ".tlv");
					GameController gc = m.load();
					
					ListView_Infos.getItems().clear();
					ListView_Infos.getItems().add(newValue);
					ListView_Infos.getItems().add(gc.getPlayers().size() + " players : ");
					
					for (Player p : gc.getPlayers()) {					
						ListView_Infos.getItems().add("- " + p.getName());
					}
					
					Image im = m.getImage();
					if (im != null) {
						image_load.setImage(im);
					}
				}
			}
			
		});
	}
	
	/**
	 * Initialize the loaded game's listView
	 */
	private void initializeListView() {
		File directoryPath = new SavefileManager().getSaveDirectory();
		File[] files=directoryPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".tlv");
			}
		});
		ListView_SavedGames.getItems().clear();
		for (File file : files) {
			ListView_SavedGames.getItems().add(file.getName().replaceFirst("[.][^.]+$", ""));
		}
	}
    
	/**
     * Set the screen to main menu view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void backToMenu(ActionEvent event) {
    	if(pause) {
    		// Return to pause menu
        	controller.setScreen(InterfaceGraphique.pause);
    	} else {
    		// Return to main menu
        	controller.setScreen(InterfaceGraphique.main);
    	}
    	
    }

    /**
     * load the choose game
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void loadGame(ActionEvent event) {

    	int selectedIdx = ListView_SavedGames.getSelectionModel().getSelectedIndex();
    	if (selectedIdx != -1) {
    		String name =ListView_SavedGames.getSelectionModel().getSelectedItem();
    		SavefileManager manager = new SavefileManager(name + ".tlv");
    		GameViewController gvc = (GameViewController)controller.getControllerScreen("Game");
    		manager.load(gvc);
    		
    		// Lunch game music
    		if (InterfaceGraphique.soundAvailable && InterfaceGraphique.playSounds) { 
    			Configuration c = Configuration.instance();
        		InterfaceGraphique.music.stop();
        		String musicFile = "resources/music/m2.mp3";
        		
        		Media sound = new Media(new File(musicFile).toURI().toString());
        		InterfaceGraphique.music = new MediaPlayer(sound);
        		InterfaceGraphique.music.setVolume(Double.parseDouble(c.lis("Musique")) / 100);
        		
        		InterfaceGraphique.music.setAutoPlay(true);
        		InterfaceGraphique.music.setCycleCount(MediaPlayer.INDEFINITE);
        		InterfaceGraphique.music.play();
        	}
    		
    		controller.setScreen("Game");
    	}
    }

    /**
     * Delete the choose game
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void deleteGame(ActionEvent event) {
    	Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		int selectedIdx = ListView_SavedGames.getSelectionModel().getSelectedIndex();
        if (selectedIdx != -1) {
        	String name =ListView_SavedGames.getSelectionModel().getSelectedItem();
        	//File file = new File(System.getProperty("user.dir")+"/"+name); 
        	
        	Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle(bundle.getString("saveDialog"));
        	alert.setHeaderText(null);
        	alert.setContentText(bundle.getString("deleteDialog"));
        	Optional <ButtonType> action =alert.showAndWait();
        	
            if (action.get() == ButtonType.OK) {
            	SavefileManager sm = new SavefileManager(name + ".tlv");
            	sm.delete();
            	ListView_SavedGames.getItems().remove(selectedIdx);
            	controller.updateScreens();
            	ListView_Infos.getItems().clear();
            	image_load.setImage(null);
            } 
        }      
    }

    /**
	 * Set parent Screen
	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
	}

	/**
	 * Update the view
	 */
	@Override
	public void update() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Change the language of elements in the view
		Button_return.setText(bundle.getString("back"));
		button_load.setText(bundle.getString("delete"));
		button_delete.setText(bundle.getString("load"));
		initializeListView();
		
	}



}


