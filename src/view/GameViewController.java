package view;

import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import controller.GameController;
import controller.HumanPlayer;
import controller.Player;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import model.Board;
import model.Building;
import model.BuildingPoint;
import model.City;
import model.CubePoint;
import model.FeedforwardType;
import model.Hexagon;
import model.ImageLoader;
import model.PiecePoint;
import model.SavefileManager;
import model.Tile;
import taluva.Configuration;
import view.AnnotationBar.MessageType;
import model.Board.ReasonPiece;
import model.Building.BuildingType;

public class GameViewController implements ControlledScreen, Initializable {
	
	public ScreensController controller;
	
	private Board board;
	private Pane tileMap;
	private VBox playerList;
	private ScrollPane playerListContainer;
	private HBox toolbox;
	private HBox deckContainer;
	private AnnotationBar annotationBar;
	
	@FXML
    private StackPane main;
	
	private ImageView huttImage;
	private ImageView towerImage;
	private ImageView templeImage;
	
	private Lighting playerColorLighting;

	private Text huttText;
	private Text towerText;
	private Text templeText;
	private Text deckText;
	

	private double mouseDragX = 0;
	private double mouseDragY = 0;
	private double offsetDragX = 0;
	private double offsetDragY = 0;
	private double lastOffsetDragX = 0;
	private double lastOffsetDragY = 0;
	
	private Button pauseMenu;

	private final double toolboxSize = 150;
	private final double playerListSize = 250;
	
	private HashMap<Player, PlayerBadge> badges;
	
	private GameController gc;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		VBox mainContainer = new VBox();
		HBox tileMapContainer = new HBox();
		playerListContainer = new ScrollPane();
		main.getChildren().addAll(mainContainer, tileMapContainer);
		
		badges = new HashMap<Player, PlayerBadge>();
		
		createPlayerList();
		createToolBox();
		createTileMap();
		createPauseMenu();

		//tile map container
		tileMapContainer.getChildren().add(playerListContainer);
		tileMapContainer.getChildren().add(tileMap);
		tileMapContainer.getChildren().add(pauseMenu);

		mainContainer.getChildren().add(tileMapContainer);
		mainContainer.getChildren().add(toolbox);

		HBox.setHgrow(tileMapContainer, Priority.ALWAYS);
		VBox.setVgrow(tileMapContainer, Priority.ALWAYS);
		
		//then you set to your node
		main.setBackground(new Background(new BackgroundFill(Color.web("#77b5fe"), null, null)));
		
		setGameController(LocalGameController.players);
		
		handleKeyboard();
		handleScrolling();
		handleDragMap();
		handleClick();
		handleMouseOver();
		
		highlightToolbox();
		
		// Close timer on exit
		InterfaceGraphique.pS.setOnHiding(e -> {
			annotationBar.getTimer().cancel();
		});
	}
	
	public GameController getGameController() {
		return gc;
	}
	
	public void setBoard(Board b) {
		this.board = b;
	}
	
	public Pane getTileMap() {
		return tileMap;
	}
	
	private void createPauseMenu() {
		//pause menu button creation
		ImageLoader il = ImageLoader.getInstance();
		ImageView hamb = new ImageView(il.getImage("hamburger.png"));
		pauseMenu = new Button("", hamb);
		pauseMenu.setBackground(Background.EMPTY);
		pauseMenu.setPickOnBounds(true);
		
		pauseMenu.setOnMouseClicked(e -> {
			try {
				setPause();
			} catch (IOException e1) {
				e1.printStackTrace();
		    }
		});
	}
	
	public void setGameController(List<Player> players) {
		setGameController(new GameController(LocalGameController.players, this), false);
	}
	
	public void setGameController(GameController gc, boolean resumePlaying) {
		this.gc = gc;
		huttText.textProperty().bind(gc.getCurrentPlayer().getHand().getHuttsProperty().asString());
		templeText.textProperty().bind(gc.getCurrentPlayer().getHand().getTemplesProperty().asString());
		towerText.textProperty().bind(gc.getCurrentPlayer().getHand().getTowersProperty().asString());
		deckText.textProperty().bind(gc.getDeck().getSizeProperty().asString());
		playerColorLighting.setLight(new Light.Distant(45,45,gc.getCurrentPlayer().getColor()));
		
		// Clean up old players
		if (playerList.getChildren().size() > 0) {
			playerList.getChildren().clear();
		}
		
		gc.getPlayers().forEach(p -> {
			PlayerBadge badge = new PlayerBadge(p);
			badges.put(p, badge);
			playerList.getChildren().add(badge);
		});
		
		playerList.setSpacing(15);
		if (!gc.gameEnded) {			
			if (resumePlaying) {
				gc.resumePlaying();
			} else {			
				gc.startPlaying();
			}
		}
		
		highlightCurrentPlayer();
		update();
	}

	private void createPlayerList() {
		playerList = new VBox();
		//playerList.setStyle("-fx-background-color: #FF0000");
		
		playerList.setPrefWidth(playerListSize);
		playerList.setMinWidth(playerListSize);
		playerListContainer.setContent(playerList);
		playerListContainer.setFitToHeight(true);
		playerListContainer.setFitToWidth(true);
		playerListContainer.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		
		playerList.setBackground(new Background(new BackgroundFill(Color.web("#5f90cb"), null, null)));
		playerListContainer.setStyle("-fx-background-color: #5f90cb;");
		playerListContainer.setMinWidth(playerListSize);
		playerListContainer.setPrefWidth(playerListSize);
		
	}
	
	private void setPause() throws IOException {
		controller.setScreen(InterfaceGraphique.pause);
	}
	
	public void setAnnotationText(String s,MessageType type) {
		if (!gc.gameEnded) {			
			if (type == MessageType.ERROR)
				annotationBar.setTemporaryMessage(s, type);
			else
				annotationBar.setMessage(s, type);
		}
	}
	
	private void createAnnotationBar() {
		annotationBar = new AnnotationBar(tileMap);
	}

	private void createTileMap() {
		tileMap = new Pane();
		createAnnotationBar();
		tileMap.getChildren().add(annotationBar);

		tileMap.maxHeightProperty().bind(main.heightProperty());
		tileMap.setMaxWidth(Double.MAX_VALUE);
		
		
		HBox.setHgrow(tileMap, Priority.ALWAYS);
		VBox.setVgrow(tileMap, Priority.ALWAYS);

		// Since JavaFX does not clip the content of the pane by default,
		// we need to explicitly and manually clip the content of the pane
		// (otherwise, the hexagons will leak out of the pane onto the interface !!)
		clipChildren(tileMap, 0);
	}
	
	private HBox createToolboxDeck() {
		deckContainer = new HBox();
		deckContainer.setPrefWidth(125);
		
		deckContainer.setOnMouseClicked(e -> {
			if(gc.gameEnded) {
				return;
			}
			if (gc.canDraw()) {				
				gc.drawPieceAction();
			} else {
				Configuration conf = Configuration.instance();
				ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
				setAnnotationText(bundle.getString("alreadyDrawn"), MessageType.ERROR);
			}
		});
		
		deckText = new Text();
		deckText.setFill(Color.WHITE);
		
		HBox.setMargin(deckContainer, new Insets(0,0,0,20));
		deckContainer.setPadding(new Insets(10,37,10,37));
		deckContainer.setAlignment(Pos.CENTER);

		deckText.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		
		deckContainer.getChildren().add(deckText);
		
		return deckContainer;
	}
	
	private HBox createBuildingButtons() {
		ImageLoader il = ImageLoader.getInstance();
		playerColorLighting = new Lighting();
		playerColorLighting.setDiffuseConstant(2.0);
		playerColorLighting.setSpecularConstant(0.0);
		playerColorLighting.setSpecularExponent(0.0);
		playerColorLighting.setSurfaceScale(0.0);
		
		
		// Buildings
		HBox buildingContainer = new HBox();
		buildingContainer.setAlignment(Pos.CENTER);
		HBox.setHgrow(buildingContainer, Priority.ALWAYS);
		
		/// House
		VBox huttContainer = new VBox();
		Image im = il.getImage("house_tile.png");
		huttImage = new ImageView(im);
		huttImage.setPreserveRatio(true);
		huttImage.setFitHeight(100);
		
		huttImage.setOnMouseClicked(e -> {
			gc.selectBuildingHuttAction();
		});
		huttText = new Text();
		huttText.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		
		huttImage.setEffect(playerColorLighting);

		huttContainer.getChildren().add(huttImage);
		huttContainer.getChildren().add(huttText);
		
		huttContainer.setAlignment(Pos.CENTER);
		
		buildingContainer.getChildren().add(huttContainer);
		
		
		/// Tower
		VBox towerContainer = new VBox();
		
		towerImage = new ImageView(il.getImage("tower_tile.png"));
		towerImage.setPreserveRatio(true);
		towerImage.setFitHeight(100);
		towerImage.setOnMouseClicked(e -> {
			gc.selectBuildingTowerAction();
		});
		
		towerText = new Text();
		towerText.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		
		towerContainer.getChildren().add(towerImage);
		towerContainer.getChildren().add(towerText);
		

		towerContainer.setAlignment(Pos.CENTER);
		
		towerImage.setEffect(playerColorLighting);
		
		buildingContainer.getChildren().add(towerContainer);
		
		/// Temple
		VBox templeContainer = new VBox();
		
		templeImage = new ImageView(il.getImage("temple_tile.png"));
		templeImage.setPreserveRatio(true);
		templeImage.setFitHeight(100);
		
		templeImage.setOnMouseClicked(e -> {
			gc.selectBuildingTempleAction();
		});

		templeText = new Text();
		
		templeText.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		templeImage.setEffect(playerColorLighting);
		
		templeContainer.getChildren().add(templeImage);
		templeContainer.getChildren().add(templeText);
		

		templeContainer.setAlignment(Pos.CENTER);
		
		buildingContainer.getChildren().add(templeContainer);
		
		buildingContainer.setSpacing(30);
		
		return buildingContainer;
	}

	private void createToolBox() {
		toolbox = new HBox();
		
		toolbox.setStyle("-fx-background-color: #5f90cb;");
		
		BackgroundSize bs = new BackgroundSize(
				BackgroundSize.AUTO, 
				BackgroundSize.AUTO, 
				false, 
				false, 
				true, 
				false);
		
		BackgroundImage bi = new BackgroundImage(
				ImageLoader.getInstance().getImage("plank.png"), 
				BackgroundRepeat.NO_REPEAT, 
				BackgroundRepeat.NO_REPEAT, 
				BackgroundPosition.CENTER,
				bs);
		
		BackgroundImage bi2 = new BackgroundImage(
				ImageLoader.getInstance().getImage("deck.png"), 
				BackgroundRepeat.NO_REPEAT, 
				BackgroundRepeat.NO_REPEAT, 
				BackgroundPosition.CENTER,
				bs);
		
		//toolbox.setStyle("-fx-background-color: #FFFF00");
		toolbox.setMinHeight(toolboxSize);
		toolbox.setMaxHeight(toolboxSize);
		toolbox.setPrefHeight(toolboxSize);
		
		toolbox.setAlignment(Pos.CENTER_LEFT);
		VBox.setVgrow(toolbox, Priority.ALWAYS);
		
		// Deck
		HBox deck = createToolboxDeck();
		deck.setBackground(new Background(bi2));
		toolbox.getChildren().add(deck);
		
		HBox buildings = createBuildingButtons();
		
		buildings.setBackground(new Background(bi));
		
		toolbox.getChildren().add(buildings);
	}
	
	private void handleMouseOver() {
		tileMap.setOnMouseMoved(e -> {
			if(gc.gameEnded || !(gc.getCurrentPlayer() instanceof HumanPlayer )) {
				return;
			}
			PiecePoint pieceInHand = gc.getCurrentPlayer().getCurrentPiecePoint();
			if (pieceInHand != null) {
				// To get the exact pixel we're on, we need to substract the view offset,
				// but also a half of the hexagon's width and hexagon's size ; this is because
				// we need to consider that the origin is on a hexagon center, and not on the
				// top left corner.
				Point2D p = new Point2D((
						e.getX() - lastOffsetDragX - (Hexagon.getTileWidth() / 2)), 
						e.getY() - lastOffsetDragY - (Hexagon.getSize() / 2));
				Point q = Hexagon.pixelToHex((float)p.getX(), (float)p.getY());
				pieceInHand.getVolcano().setX(q.x);
				pieceInHand.getVolcano().setY(q.y);
				
				pieceInHand.updatePointByVolcano(CubePoint.fromAxial(q.x, q.y));
				
				update();
			} else if (gc.getCurrentPlayer().getCurrentBuilding() != null) {
				
				BuildingPoint bp = gc.getCurrentPlayer().getCurrentBuilding();
				
				Point2D p = new Point2D((
						e.getX() - lastOffsetDragX - (Hexagon.getTileWidth() / 2)), 
						e.getY() - lastOffsetDragY - (Hexagon.getSize() / 2));
				CubePoint q = Hexagon.pixelToCube((float)p.getX(), (float)p.getY());
				
				CubePoint last = new CubePoint();
				last.setCoordinates(bp.point.getX(), bp.point.getY(), bp.point.getZ());
				
				bp.point.setCoordinates(
						q.getX(),
						q.getY(),
						q.getZ());
				
				FeedforwardType fftype = gc.canPlaceCurrentBuilding() 
						? FeedforwardType.CORRECTPLACEMENT
						: FeedforwardType.WRONGPLACEMENT;
				
				
				if (!bp.point.equals(last)) {
					gc.setFeedforward(last, FeedforwardType.NONE);
				}
				
				gc.setFeedforward(bp.point, fftype);
				
				update();
			} else if (gc.getCurrentPlayer().getCurrentCity() != null) {
				gc.resetFeedforward();
				City c = gc.getCurrentPlayer().getCurrentCity();
				highlightCity(c);
				
				Point2D p = new Point2D((
						e.getX() - lastOffsetDragX - (Hexagon.getTileWidth() / 2)), 
						e.getY() - lastOffsetDragY - (Hexagon.getSize() / 2));
				CubePoint q = Hexagon.pixelToCube((float)p.getX(), (float)p.getY());
				
				List<BuildingPoint> pointsToHighlight = board.getExtendPointFromCityAndTile(c, 
						new BuildingPoint(new Building(BuildingType.HUTT, gc.getCurrentPlayer().getColor(), gc.getCurrentPlayer().getColorName()), q));
				
				pointsToHighlight.forEach(bp -> gc.setFeedforward(bp.point, FeedforwardType.CORRECTPLACEMENT));
				
				update();
			}
		});
	}
	
	public void highlightCity(City c) {
		if(!(Configuration.instance().lis("Aide").equals("none") || Configuration.instance().lis("Aide").equals("expert"))) {
			for (BuildingPoint bp : c.getBuildingPoints()) {
				gc.setFeedforward(bp.point, FeedforwardType.CITYSELECTED);
			}
		}
	}
	
	public void unHighlightCity(City c) {
		if(!(Configuration.instance().lis("Aide").equals("none") || Configuration.instance().lis("Aide").equals("expert"))) {
			for (BuildingPoint bp : c.getBuildingPoints()) {
				gc.setFeedforward(bp.point, FeedforwardType.NONE);
			}
		}
	}
	
	private void handleClick() {
		
		tileMap.setOnMouseClicked(e -> {
			if(gc.gameEnded) {
				return;
			}
			if (e.getButton() == MouseButton.SECONDARY) {
				rotatePieceInHand();
			}else if(e.getButton() == MouseButton.PRIMARY){
				PiecePoint pieceInHand = gc.getCurrentPlayer().getCurrentPiecePoint();
				if (gc.getCurrentPlayer().getCurrentCity() != null) {
					
					Point2D p = new Point2D((
							e.getX() - lastOffsetDragX - (Hexagon.getTileWidth() / 2)), 
							e.getY() - lastOffsetDragY - (Hexagon.getSize() / 2));
					CubePoint q = Hexagon.pixelToCube((float)p.getX(), (float)p.getY());
					Tile t = gc.board.getTile(q);
					
					if (t == null) {						
						unHighlightCity(gc.getCurrentPlayer().getCurrentCity());
						gc.getCurrentPlayer().unselectCurrentCity();
						update();
					}
					
				}
				if (pieceInHand != null) {					
					gc.selectPointAction(null);
				}
			}
		});
		
	}

	private void handleKeyboard() {
		main.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.UP) {
				Hexagon.moveViewUp();
			}

			if (e.getCode() == KeyCode.DOWN) {
				Hexagon.moveViewDown();
			}

			if (e.getCode() == KeyCode.LEFT) {
				Hexagon.moveViewLeft();
			}

			if (e.getCode() == KeyCode.RIGHT) {
				Hexagon.moveViewRight();
			}
			
			if (e.getCode() == KeyCode.F11) {
				
				SavefileManager sm = new SavefileManager();
				GameController newGc = null;
				do {
					newGc = sm.undo(this.gc);
					if (newGc == null) {
						break;
					} else {
						newGc.generateTransientData(this);
					}
				} while(!(newGc.getCurrentPlayer() instanceof HumanPlayer));
				if (newGc != null) {					
					setGameController(newGc, true);
				}
				
			}
			
			if (e.getCode() == KeyCode.F12) {
				
				SavefileManager sm = new SavefileManager();
				GameController newGc = null;
				do {
					newGc = sm.redo(this.gc);
					if (newGc == null) {
						break;
					} else {
						newGc.generateTransientData(this);
					}
				} while(!(newGc.getCurrentPlayer() instanceof HumanPlayer));
				if (newGc != null) {					
					setGameController(newGc, true);
				}
				
			}

			lastOffsetDragX = Hexagon.getOffsetX();
			lastOffsetDragY = Hexagon.getOffsetY();
			update();
			e.consume();
		});
	}

	private void handleScrolling() {
		main.setOnScroll(e ->  {
			if (e.getDeltaY() > 0) {
				Hexagon.zoomIn();
			} else {
				Hexagon.zoomOut();
			}
			update();
		});
	}

	private void handleDragMap() {
		tileMap.setOnDragDetected(e -> {
			/* drag was detected, start a drag-and-drop gesture*/
			/* allow any transfer mode */
			Dragboard db = tileMap.startDragAndDrop(TransferMode.ANY);

			/* Put a string on a dragboard */
			ClipboardContent content = new ClipboardContent();
			content.putString("");
			db.setContent(content);

			mouseDragX = e.getX();
			mouseDragY = e.getY();
			offsetDragX = Hexagon.getOffsetX();
			offsetDragY = Hexagon.getOffsetY();

			e.consume();
		});

		tileMap.setOnDragOver(e -> {
			offsetDragX = e.getX() - mouseDragX + lastOffsetDragX;
			offsetDragY = e.getY() - mouseDragY + lastOffsetDragY;

			Hexagon.setOffsetX(offsetDragX);
			Hexagon.setOffsetY(offsetDragY);

			e.acceptTransferModes(TransferMode.ANY);
			e.consume();
			update();
		});

		tileMap.setOnDragDropped(e -> {
			lastOffsetDragX = offsetDragX;
			lastOffsetDragY = offsetDragY;
		});
	}
	
	public void setPieceInHand(PiecePoint p) {
		handleMouseOver();
	}

	public void update() {
		if (gc.getCurrentPlayer() == null) {
			return;
		}
		
		tileMap.getChildren().clear();
		
		Iterator<PiecePoint> it = board.getPieceIterator();
		
		while(it.hasNext()) {
			PiecePoint p = it.next();
						
			for (Tile t : p.tiles) {				
				t.updatePoints();
				if (tileMap.getChildren().contains(t)) {
					tileMap.getChildren().remove(t);
					if (t.getBuildingImage() != null) {				
						tileMap.getChildren().remove(t.getBuildingImage());
					}
				}
				tileMap.getChildren().add(t);
				if (t.getBuildingImage() != null) {				
					tileMap.getChildren().add(t.getBuildingImage());
				}
			}
		
			tileMap.getChildren().add(p.getOutline());
			
		}
		
		PiecePoint pieceInHand = gc.getCurrentPlayer().getCurrentPiecePoint();

		if (pieceInHand != null) {
			ReasonPiece reason = board.canInsertPiece(pieceInHand);
			if(!(Configuration.instance().lis("Aide").equals("none"))) {
				if (reason == ReasonPiece.OK) {
					pieceInHand.setFeedforward(FeedforwardType.CORRECTPLACEMENT);
				} else if (reason == ReasonPiece.NOT_ADJACENT) {
					pieceInHand.setFeedforward(FeedforwardType.NONE);
				} else {
					pieceInHand.setFeedforward(FeedforwardType.WRONGPLACEMENT);
				}
			}
			
			for (Tile t: pieceInHand.tiles) {
				t.updatePoints();
				tileMap.getChildren().add(t);
				
			}
			tileMap.getChildren().add(pieceInHand.getOutline());
		}
		
		if(Configuration.instance().lis("Aide").equals("novice") ||
		Configuration.instance().lis("Aide").equals("middle") ||
		gc.gameEnded) {
			if(gc.gameEnded) {
				annotationBar.setAlwaysShow(true);
			}
			tileMap.getChildren().add(annotationBar);
		}
	}
		
	private void rotatePieceInHand() {
		PiecePoint pieceInHand = gc.getCurrentPlayer().getCurrentPiecePoint();

		if (pieceInHand != null) {
			pieceInHand.turnPoint();
		}
		update();
	}
	
	public void highlightToolbox() {
		if(gc.phase <= 1 || gc.gameEnded) {
			huttImage.setOpacity(0.5);
			towerImage.setOpacity(0.5);
			templeImage.setOpacity(0.5);
			if(gc.gameEnded || gc.getCurrentPlayer().getCurrentPiecePoint() != null) {
				deckContainer.setOpacity(0.5);
			}else {
				deckContainer.setOpacity(1);
			}
		}else {
			deckContainer.setOpacity(0.5);
			if(gc.getCurrentPlayer().getCurrentBuilding() != null) {
				switch (gc.getCurrentPlayer().getCurrentBuilding().getType()) {
				case HUTT:
					huttImage.setOpacity(0.5);
					towerImage.setOpacity(1);
					templeImage.setOpacity(1);
					break;
				case TOWER:
					huttImage.setOpacity(1);
					towerImage.setOpacity(0.5);
					templeImage.setOpacity(1);
					break;
				case TEMPLE:
					huttImage.setOpacity(1);
					towerImage.setOpacity(1);
					templeImage.setOpacity(0.5);
					break;

				default:
					huttImage.setOpacity(1);
					towerImage.setOpacity(1);
					templeImage.setOpacity(1);
					break;
				}
			}else {
				huttImage.setOpacity(1);
				towerImage.setOpacity(1);
				templeImage.setOpacity(1);
			}
		}
	}
	
	private void highlightCurrentPlayer() {
		if (badges != null) {
			badges.values().forEach(b -> {
				b.endTurn();
			});
			
			
			if (gc != null && gc.getCurrentPlayer() != null) {				
				PlayerBadge badge = badges.get(gc.getCurrentPlayer());
				
				if (badge != null) {
					badge.startTurn();
				}
			}
		}
	}
	
	public void nextPlayer() {
		huttText.textProperty().bind(gc.getCurrentPlayer().getHand().getHuttsProperty().asString());
		playerColorLighting.setLight(new Light.Distant(45,45,gc.getCurrentPlayer().getColor()));
		templeText.textProperty().bind(gc.getCurrentPlayer().getHand().getTemplesProperty().asString());
		towerText.textProperty().bind(gc.getCurrentPlayer().getHand().getTowersProperty().asString());
		highlightCurrentPlayer();
	}
	
	static void clipChildren(Region region, double arc) {

		final Rectangle outputClip = new Rectangle();
		outputClip.setArcWidth(arc);
		outputClip.setArcHeight(arc);
		region.setClip(outputClip);

		region.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
			outputClip.setWidth(newValue.getWidth());
			outputClip.setHeight(newValue.getHeight());
		});        
	}

	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;	
	}

}
