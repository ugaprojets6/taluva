package view;

import controller.HumanPlayer;
import controller.Player;
import javafx.beans.property.IntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import model.ImageLoader;

public class PlayerBadge extends VBox {
	private Player player;
	private Font font;
	private ImageView playerIcon;
	private final String defaultStyle = "-fx-padding: 10;" + "-fx-border-style: solid inside;"
			+ "-fx-border-insets: 5;"
	        + "-fx-border-radius: 5;" + "-fx-border-color: purple;";
	
	
	public PlayerBadge(Player player) {
		super();
		this.player = player;
		this.font = Font.font("Arial", FontWeight.BOLD, 20);
	
		
		getChildren().addAll(createNameTag(), createBuildingContainer());
		//setStyle("-fx-padding: 15px; -fx-background-color: #BABABA");
	}
	
	private Lighting getPlayerLighting() {
		Lighting playerColor = new Lighting();
		playerColor.setDiffuseConstant(2.0);
		playerColor.setSpecularConstant(0.0);
		playerColor.setSpecularExponent(0.0);
		playerColor.setSurfaceScale(0.0);
		playerColor.setLight(new Light.Distant(45,45,player.getColor()));
		
		return playerColor;
	}
	
	private HBox createNameTag() {
		HBox nameTag = new HBox();

		Text playerName = new Text(player.getName());
		playerName.setFont(font);
		
		nameTag.getChildren().addAll(playerName);
		nameTag.setAlignment(Pos.CENTER);
		
		return nameTag;
	}
	
	private HBox createBuildingContainer() {
		HBox infoContainer = new HBox();
		
		infoContainer.getChildren().addAll(createPlayerIcon(), createBuildingCount());
		
		return infoContainer;
	}

	private VBox createPlayerIcon() {
		VBox leftContainer = new VBox();
		
		ImageLoader il = ImageLoader.getInstance();
		Image im;
		
		if (player instanceof HumanPlayer) {
			im = il.getImage("human.png");
		} else {
			im = il.getImage("ai.png");
		}
		
		Lighting playerLighting = getPlayerLighting();
		playerIcon = new ImageView(im);
		playerIcon.setPreserveRatio(true);
		playerIcon.setFitWidth(75);
		playerIcon.setEffect(playerLighting);
		
		leftContainer.getChildren().addAll(playerIcon);
		leftContainer.setAlignment(Pos.CENTER);
		
		return leftContainer;
	}
	
	private VBox createBuildingCount() {
		VBox rightContainer = new VBox();

		HBox huttContainer = createBuildingText(
				player.getHand().getHuttsProperty(),
				"/20",
				"house_tile.png");
		
		HBox towerContainer = createBuildingText(
				player.getHand().getTowersProperty(),
				"/2",
				"tower_tile.png");
		
		HBox templeContainer = createBuildingText(
				player.getHand().getTemplesProperty(),
				"/3",
				"temple_tile.png");
				
		rightContainer.getChildren().addAll(huttContainer, towerContainer, templeContainer);
		rightContainer.setPadding(new Insets(0, 15, 0, 0));
		HBox.setHgrow(rightContainer, Priority.ALWAYS);
		
		return rightContainer;
	}
	
	private HBox createBuildingText(IntegerProperty property, String staticText, String imageName) {
		HBox textContainer = new HBox();
		ImageLoader il = ImageLoader.getInstance();

		
		Text buildingCount = new Text();
		buildingCount.textProperty().bind(property.asString());
		buildingCount.setFont(font);
		Text buildingMax = new Text(staticText);
		buildingMax.setFont(font);
		ImageView buildingIcon = new ImageView(il.getImage(imageName));
		buildingIcon.setPreserveRatio(true);
		buildingIcon.setFitWidth(50);
		buildingIcon.setEffect(getPlayerLighting());
		
		textContainer.setAlignment(Pos.CENTER_RIGHT);
		HBox.setHgrow(textContainer, Priority.ALWAYS);
		
		textContainer.getChildren().addAll(buildingCount, buildingMax, buildingIcon);
		
		return textContainer;
	}
	
	public void startTurn() {
		setStyle(defaultStyle + "-fx-border-width: 5;");
	}
	
	public void endTurn() {
		setStyle(defaultStyle + "-fx-border-width: 0;");
	}
}
