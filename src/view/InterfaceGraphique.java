package view;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import taluva.Configuration;

public class InterfaceGraphique extends Application{

	// Name of screens
	public static String main = "Menu";
	public static String newGame = "NewGame";
	public static String localGame = "LocalGame";
	public static String settings = "Settings";
	public static String loadGame = "LoadGame";
	public static String onlineGame = "OnlineGame";
	public static String tutorial = "Tutorial";
	public static String game = "Game";
	public static String pause = "PauseMenu";
	public static String saveGame = "SaveGame";
	
	public static Stage pS = null;
	public static Scene scene = null;
	public static Configuration c = Configuration.instance();
	public static MediaPlayer music;
	public static MediaPlayer effects;
	public static boolean soundAvailable;
	public static boolean playSounds;
	
	/**
	 * Put the game in fullscreen
	 */
	public static void fullscreen() {
		if(c.lis("Fullscreen").equals("true")) {
			pS.setFullScreen(true);
		}else {
			pS.setFullScreen(false);
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		pS = primaryStage;
		
		// Set min height to the stage
		primaryStage.setMinHeight(600);
		primaryStage.setMinWidth(1000);
		
		// Set fullscreen if it's enable
		if(c.lis("Fullscreen").equals("true")) {
			primaryStage.setFullScreen(true);
		}
		
		// add every view to the container mainContainer
		ScreensController mainContainer = new ScreensController();
		mainContainer.loadScreen(main);
		mainContainer.loadScreen(newGame);
		mainContainer.loadScreen(localGame);
		mainContainer.loadScreen(settings);
		mainContainer.loadScreen(loadGame);
		mainContainer.loadScreen(tutorial);
		mainContainer.loadScreen(game);
		mainContainer.loadScreen(pause);
		mainContainer.loadScreen(saveGame);
		
		// Set the mainscreen as the first screen
		mainContainer.setScreen(main);
		
		// Play music
		Media sound = new Media(getClass().getClassLoader().getResource("music/main2.mp3").toString());
		try {			
			music = new MediaPlayer(sound);
			music.play();
			music.setVolume(Double.parseDouble(c.lis("Musique"))/100);
			music.setAutoPlay(true);
			music.setCycleCount(MediaPlayer.INDEFINITE);
			soundAvailable = true;
		} catch (MediaException e) {
			soundAvailable = false;
		}
		
		// Create a StackPane 
		StackPane root = (StackPane) FXMLLoader.load(getClass().getResource("Background.fxml"));
		
		// Set background Image
		ImageView imgBack = (ImageView) root.lookup("#taluvaBackground");
		imgBack.fitWidthProperty().bind(root.widthProperty());
		imgBack.fitHeightProperty().bind(root.heightProperty());
		
		// Set Title image
		Pane p = (Pane) root.lookup("#paneTitle");
		ImageView imgTitle = (ImageView) root.lookup("#taluvaTitle");
		imgTitle.fitWidthProperty().bind(p.widthProperty());
		imgTitle.fitHeightProperty().bind(p.heightProperty());
			
		// Add all the view to the StackPane
		root.getChildren().addAll(mainContainer);
		
		// Launch the application
		scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Inform if we can play sound
		if (!soundAvailable) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(bundle.getString("soundError"));
			alert.setHeaderText(null);
			alert.setContentText(bundle.getString("soundErrorText"));
			alert.showAndWait();
		}
		
		
	}
}
