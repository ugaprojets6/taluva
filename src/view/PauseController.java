package view;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import taluva.Configuration;

public class PauseController implements ControlledScreen {

	ScreensController controller;

    @FXML
    private Button settingButton;

    @FXML
    private Button loadButton;

    @FXML
    private Button backToGameButton;

    @FXML
    private StackPane pane;

    @FXML
    private Button saveButton;

    @FXML
    private Button backToMenuButton;

    @FXML
    private Button newGameButton;

    /**
     * Set the screen to main menu view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void backToGame(ActionEvent event) {
    	// Back to game
    	controller.setScreen(InterfaceGraphique.game);
    }

    /**
     * Set the screen to save view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void save(ActionEvent event) {
    	controller.setScreen(InterfaceGraphique.saveGame);
    }
    
    /**
     * Set the screen to load view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void loadGame(ActionEvent event) {
    	// Go to load screen
    	((LoadGameController) controller.getControllerScreen(InterfaceGraphique.loadGame)).pause = true;
    	controller.setScreen(InterfaceGraphique.loadGame);
    }

    /**
     * Set the screen to settings view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void goToSettings(ActionEvent event) {
    	// Go to settings screen
    	((SettingsController) controller.getControllerScreen(InterfaceGraphique.settings)).pause = true;
    	controller.setScreen(InterfaceGraphique.settings);
    }
    
    /**
     * Set the screen to newGame view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void goToNewGame(ActionEvent event) {
    	Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Confirmation before quit the partie
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle(bundle.getString("backNewGame"));
    	alert.setHeaderText(null);
    	alert.setContentText(bundle.getString("backDialog"));
    	Optional <ButtonType> action =alert.showAndWait();
    	
        if (action.get() == ButtonType.OK) {
        	controller.setScreen(InterfaceGraphique.localGame);
        } 
    }

    /**
     * Set the screen to main menu view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void backToMenu(ActionEvent event) {
    	Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Confirmation before quit the partie
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle(bundle.getString("backMenu"));
    	alert.setHeaderText(null);
    	alert.setContentText(bundle.getString("backDialog"));
    	Optional <ButtonType> action =alert.showAndWait();
    	
        if (action.get() == ButtonType.OK) {
       
	    	// Back to menu
	    	controller.setScreen(InterfaceGraphique.main);
	    	
	    	// Change music
			if (InterfaceGraphique.soundAvailable && InterfaceGraphique.playSounds) { 
				Configuration c = Configuration.instance();
	    		InterfaceGraphique.music.stop();
	    		
	    		Media sound = new Media(getClass().getClassLoader().getResource("music/main2.mp3").toString());
	    		InterfaceGraphique.music = new MediaPlayer(sound);
	    		InterfaceGraphique.music.setVolume(Double.parseDouble(c.lis("Musique")) / 100);
	    		
	    		InterfaceGraphique.music.setAutoPlay(true);
	    		InterfaceGraphique.music.setCycleCount(MediaPlayer.INDEFINITE);
	    		InterfaceGraphique.music.play();
	    	}
		}

    }

    /**
   	 * Set parent Screen
   	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
	}

	/**
	 * Update the view
	 */
	@Override
	public void update() {
		
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Change the language of elements in the view
		settingButton.setText(bundle.getString("settings"));
		loadButton.setText(bundle.getString("loadGame"));
		backToGameButton.setText(bundle.getString("backGame"));
		saveButton.setText(bundle.getString("saveGame"));
		backToMenuButton.setText(bundle.getString("backMenu"));
		newGameButton.setText(bundle.getString("new_Game"));
		
	}

}
