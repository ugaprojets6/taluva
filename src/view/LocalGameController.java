package view;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import AI.AIR;
import AI.MAY;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import taluva.Configuration;
import controller.HumanPlayer;
import controller.Player;

public class LocalGameController implements ControlledScreen, Initializable {
	ScreensController controller;
	
	// player's number
	int nbJoueur = 2;

	public static final int ADD = 0;
	public static final int BLUE = 1;
	public static final int RED = 2;
	public static final int GREEN = 3;
	public static final int YELLOW = 4;
	public static final int ORANGE = 5;
	public static final int PURPLE = 6;
	public static final int WHITE = 7;
	public static final int SKY = 8;
	
	// player list 
	public static List<Player> players = new ArrayList<Player>();
	
	// list to initialize type combo box
	ObservableList<String> list_playerType;
	
	// save the color of each player
	int[] choosen = { BLUE, RED, ADD, ADD };

	@FXML
	private Button imgPlayer1;
	
	@FXML
	private Button imgPlayer2;

	@FXML
	private Button imgPlayer3;
	
	@FXML
	private Button imgPlayer4;
	
	@FXML
	private TextField player1Text;

	@FXML
	private TextField player2Text;

	@FXML
	private TextField player3Text;

	@FXML
	private TextField player4Text;
	
	@FXML
	private ImageView img1;

	@FXML
	private ImageView img2;
	
	@FXML
	private ImageView img3;
	
	@FXML
	private ImageView img4;
	
	@FXML
	private GridPane localGamePane;

	@FXML
	private Button backButton;
	
    @FXML
    private Button deletePlay3Button;
    
    @FXML
    private VBox vbox2;
    
    @FXML
    private Button deletePlay4Button;
    
    @FXML
    private Button playButton;
    
    @FXML
    private ComboBox<String> choosePlayer1;
    
    @FXML
    private ComboBox<String> choosePlayer2;
    
    @FXML
    private ComboBox<String> choosePlayer3;
    
    @FXML
    private ComboBox<String> choosePlayer4;

    /**
     * Change color of player 1
     * @param event the click {@link ActionEvent event}
     */
	@FXML
	void clickedOn1(ActionEvent event) {
		changeColor(1);
	}

	/**
	 * Change color of player 2
	 * @param event the click {@link ActionEvent event}
	 */
	@FXML
	void clickedOn2(ActionEvent event) {
		changeColor(2);
	}

	/**
	 * Change color of player 3 or add a player 3
	 * @param event the click {@link ActionEvent event}
	 */
	@FXML
	void clickedOn3(ActionEvent event) {
		if (nbJoueur == 2) {
			addPlayer();
		} else {
			changeColor(3);
		}
	}

	/**
	 * Change color of player 4 or add a player 3 or 4 if 3 already exist
	 * @param event the click {@link ActionEvent event}
	 */
	@FXML
	void clickedOn4(ActionEvent event) {
		if (nbJoueur > 3) {
			changeColor(4);
		} else {
			addPlayer();
		}
	}
	
	/**
	 * delete player 3 or 4 if 4 exist
	 * @param event the click {@link ActionEvent event}
	 */
	@FXML
    void deletePlay3(ActionEvent event) {
		deletePlayer();
    }

	/**
	 * delete 4 if exist
	 * @param event the click {@link ActionEvent event}
	 */
    @FXML
    void deletePlay4(ActionEvent event) {
    	deletePlayer();
    }
    
    /**
     * Set the screen to new game menu view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
	void backToMenu(ActionEvent event) {

    	// reset choosen
		choosen[0] = BLUE;
		choosen[1] = RED; 
		choosen[2] = ADD;
		choosen[3] = ADD;
		
		InputStream in =  Configuration.charge("Images/add.png");
		Image i = new Image(in);
		
		enablePlayer(true, 0, player3Text, choosePlayer3, deletePlay3Button);
		img3.setImage(i);
		
		enablePlayer(true, 0, player4Text, choosePlayer4, deletePlay4Button);
		img4.setImage(i);
		
		nbJoueur = 2;
		
		// back to new Game
		controller.setScreen(InterfaceGraphique.newGame);
	}
    
    /**
     * lunch the game with the choose configuration
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void play(ActionEvent event) {
    	Configuration c = Configuration.instance();
    	players = new ArrayList<Player>();
    	
    	// create player
    	for (int i = 1; i <= nbJoueur; i++) {
    		players.add(createPlayer(i));	
		}

    	// Get the GameViewController
    	((GameViewController) controller.getControllerScreen(InterfaceGraphique.game)).setGameController(players);

    	// Set the game's screen
    	controller.setScreen(InterfaceGraphique.game);	
    	
    	// Launch game music
    	if (InterfaceGraphique.soundAvailable && InterfaceGraphique.playSounds) {    		
    		InterfaceGraphique.music.stop();
    		String musicFile = "resources/music/m2.mp3";
    		
    		Media sound = new Media(new File(musicFile).toURI().toString());
    		InterfaceGraphique.music = new MediaPlayer(sound);
    		InterfaceGraphique.music.setVolume(Double.parseDouble(c.lis("Musique")) / 100);
    		
    		InterfaceGraphique.music.setAutoPlay(true);
    		InterfaceGraphique.music.setCycleCount(MediaPlayer.INDEFINITE);
    		InterfaceGraphique.music.play();
    	}

    }
    
    /**
     * Create a player thanks to the view information
     * @param numPlayer the {@link Integer numplayer} is the num of the created player
     * @return a set {@link Player}
     */
    Player createPlayer(int numPlayer) {
    	switch (numPlayer) {
			case 1:
				return chooseType(choosePlayer1, player1Text, numPlayer);
			case 2:
				return chooseType(choosePlayer2, player2Text, numPlayer);
			case 3:
				return chooseType(choosePlayer3, player3Text, numPlayer);
			case 4:
				return chooseType(choosePlayer4, player4Text, numPlayer);
			default:
				return null;
    	}    	
    }
    
    /**
     * Choose the type of a Player 
     * @param combo {@link ComboBox combo} helps to get the type of the player ( Human, AIR or MAY)
     * @param name {@link TextField name} helps to get the name of the player
     * @param player {@link Integer player} for know which is the player created to get his color
     * @return a set {@link Player}
     */
    Player chooseType(ComboBox<String> combo, TextField name, int player) {
    	Configuration conf = Configuration.instance();

    	ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
    	if(combo.getSelectionModel().getSelectedItem().equals(bundle.getString("player"))) 
    	{   
			return new HumanPlayer(name.getText(), chooseColor(player), chooseColorName(player));
    	}
    	else if(combo.getSelectionModel().getSelectedItem().equals(bundle.getString("ordi1")))
			return new AIR(name.getText(), chooseColor(player), chooseColorName(player));
		else if(combo.getSelectionModel().getSelectedItem().equals(bundle.getString("ordi2")))
			return new MAY(name.getText(), chooseColor(player), chooseColorName(player),true);
		else if(combo.getSelectionModel().getSelectedItem().equals(bundle.getString("ordi3")))
			return new MAY(name.getText(), chooseColor(player), chooseColorName(player),false);
		else
			return null;
    }
    
    /**
     * Select color for a player
     * @param player {@link Integer player} for know which is the player created to get his color
     * @return the player's {@link Color}
     */
    Color chooseColor(int player) {
    	Configuration conf = Configuration.instance();
    	switch (choosen[player-1]) {
			case ADD:
				return null;
			case BLUE:
				return conf.lisCouleur("blue");
			case RED:
				return conf.lisCouleur("red");
			case GREEN:
				return conf.lisCouleur("green");
			case YELLOW:
				return conf.lisCouleur("yellow");
			case ORANGE:
				return conf.lisCouleur("orange");
			case PURPLE:
				return conf.lisCouleur("purple");
			case WHITE:
				return conf.lisCouleur("white");
			case SKY:
				return conf.lisCouleur("sky");
			default:
				return null;
    	}
    }
    
    /**
     * Get the color name of a player
     * @param player {@link Integer player} for know which is the player created to get his color
     * @return {@link String color name}
     */
    String chooseColorName(int player) {
    	switch (choosen[player-1]) {
		case ADD:
			return null;
		case BLUE:
			return "blue";
		case RED:
			return "red";
		case GREEN:
			return "green";
		case YELLOW:
			return "yellow";
		case ORANGE:
			return "orange";
		case PURPLE:
			return "purple";
		case WHITE:
			return "white";
		case SKY:
			return "sky";
		default:
			return "";
    	}
    }

    /**
     * Disable a player 3 or 4
     */
    void deletePlayer() {
    	InputStream in =  Configuration.charge("Images/add.png");
    	Image i = new Image(in);
    	if (nbJoueur == 3)
	    {
    		enablePlayer(true, 0, player3Text, choosePlayer3, deletePlay3Button);
	    	img3.setImage(i);
	    }
    	if (nbJoueur == 4)
	    {
    		enablePlayer(true, 0, player4Text, choosePlayer4, deletePlay4Button);
	    	img4.setImage(i);
	    }
    	choosen[nbJoueur-1] = ADD;
    	nbJoueur = nbJoueur - 1;
    }

    /**
     * Change the color of a player
     * @param player {@link Integer player} the number of the player whose color you want to change
     */
	void changeColor(int player) {

		int img = choosen[player-1];
		ImageView imgV = new ImageView();
		if(player == 1) {		
			imgV = img1;
		}
		else if(player == 2) {
			imgV = img2;
		}
		else if(player == 3) {
			imgV = img3;
		}
		else if(player == 4) {
			imgV = img4;
		}
		boolean present = true;
		while(present) {
			if(img + 1 > 8)
				img = 0;
			img++;
			present = false;
			for (int i = 0; i < choosen.length; i++) {
				if ( img == choosen[i])
					present = true;
			}
			
		}
		choosen[player-1] = img;
		
		InputStream in =  Configuration.charge("Images/white.png");
		Image i = new Image(in);
		imgV.setImage(i);
		imgV.setEffect(getLighting(chooseColor(player)));
		choosen[player-1] = img;
	}

	/**
	 * Add a player when the number of player is < 4
	 */
	void addPlayer() {
		if (nbJoueur == 2) {
			nbJoueur++;
			changeColor(3);
			enablePlayer(false, 1, player3Text, choosePlayer3, deletePlay3Button);
		}
		else if (nbJoueur == 3) {
			nbJoueur++;
			changeColor(4);
			enablePlayer(false, 1, player4Text, choosePlayer4, deletePlay4Button);
		}
	}
	
	/**
	 * Enable or disable a player
	 * @param disable {@link Boolean} to know if we want disable or enable
	 * @param opacity {@link Integer} to know if we want disable or enable
	 * @param t the {@link TextField} to disable or enable
	 * @param c the {@link ComboBox} to disable or enable
	 * @param b the {@link Button} to disable or enable
	 */
	private void enablePlayer(boolean disable, int opacity, TextField t, ComboBox<String> c, Button b) {
		if(t != null) {
			t.setDisable(disable);
			t.setOpacity(opacity);
		}
		if(c != null) {
			c.setDisable(disable);
			c.setOpacity(opacity);
		}
		if(b != null) {
			b.setDisable(disable);
			b.setOpacity(opacity);
		}
	}

	/**
	 * Set parent Screen
	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
	}
	
	/**
	 * Return the lighting in function of the color of a player
	 * @param c the {@link Color} of the player
	 * @return {@link Lighting}
	 */
	public Lighting getLighting (Color c) {
		Lighting lighting = new Lighting();
		lighting.setDiffuseConstant(2.0);
		lighting.setSpecularConstant(0.0);
		lighting.setSpecularExponent(0.0);
		lighting.setSurfaceScale(0.0);
		lighting.setLight(new Light.Distant(45, 45, c));
		
		return lighting;
	}

	/**
     * Set the view during the initialization
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		Configuration config = Configuration.instance();
		InputStream in =  Configuration.charge("Images/white.png");
		Image i = new Image(in);
		
		// Initialize color for player 1
		img1.setImage(i);
		img1.setEffect(getLighting(chooseColor(1)));
		
		players.add(new HumanPlayer("Player 1", config.lisCouleur("red"), "red"));
		
		// Initialize color for player 2
		img2.setImage(i);
		img2.setEffect(getLighting(chooseColor(2)));
	
		players.add(new HumanPlayer("Player 2", config.lisCouleur("blue"), "blue"));
		
		// Initialize color for player 3 and 4
		in =  Configuration.charge("Images/add.png");
		i = new Image(in);
		img3.setImage(i);
		img4.setImage(i);
		
		// Update player
		updateChoosePlayer();
	
	}

	/**
	 * Update the view
	 */
	@Override
	public void update() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		// Change the language of elements in the view
		
		// Button
		backButton.setText(bundle.getString("back"));
		playButton.setText(bundle.getString("play"));
		
		// TextView
		player1Text.setText(bundle.getString("name1"));
		player2Text.setText(bundle.getString("name2"));
		player3Text.setText(bundle.getString("name3"));
		player4Text.setText(bundle.getString("name4"));
		
		updateChoosePlayer();

	}
	
	/**
	 * Update the different combobox in function of the language
	 */
	private void updateChoosePlayer() {
		Configuration config = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(config.lis("Langue")));
		
		list_playerType = FXCollections.observableArrayList(bundle.getString("player"),bundle.getString("ordi1"),bundle.getString("ordi2"), bundle.getString("ordi3"));
		
		choosePlayer1.setItems(list_playerType);
		choosePlayer1.setValue(bundle.getString("player"));
		choosePlayer2.setItems(list_playerType);
		choosePlayer2.setValue(bundle.getString("ordi1"));
		choosePlayer3.setItems(list_playerType);
		choosePlayer3.setValue(bundle.getString("ordi1"));
		choosePlayer4.setItems(list_playerType);
		choosePlayer4.setValue(bundle.getString("ordi1"));
	}

}
