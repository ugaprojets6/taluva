package view;

public interface ControlledScreen {
	
	public void setParentScreen(ScreensController page);
	
	public void update ();
	
}
