package view;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.SavefileManager;
import taluva.Configuration;

public class SaveGameController implements ControlledScreen, Initializable{
	ScreensController controller; 

    @FXML
    private VBox VBox_LoadGame;

    @FXML
    private Button Button_return;
    @FXML
    private Button button_save;

    @FXML
    private Label Label_enter_name;

    @FXML
    private TextField TextField_nameGame;

    @FXML
    private HBox HBox_LoadGame;

    @FXML
    private ListView<String> ListView_SavedGames;

    @FXML
    private HBox HBox_LoadGameButtons;

    /**
     * Set the screen to main menu
     * @param event the click {@link ActionEvent}
     */
    @FXML
    void backToMenu(ActionEvent event) {
    	controller.setScreen(InterfaceGraphique.pause);
    }
    
    /**
     * Save the game
     * @param event the click {@link ActionEvent}
     */
    @FXML
    void saveGame(ActionEvent event) {
    	
    	if (!TextField_nameGame.getText().isEmpty()) {   
    		Configuration config = Configuration.instance();
    		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(config.lis("Langue")));
			
    		SavefileManager manager = new SavefileManager(TextField_nameGame.getText() + ".tlv");
    		GameViewController gvc = (GameViewController)controller.getControllerScreen("Game");
    		
    		manager.save(gvc.getGameController());
    		
    		Alert alert = new Alert(AlertType.INFORMATION);
        	alert.setTitle(bundle.getString("saveDialog"));
        	alert.setHeaderText(null);
        	alert.setContentText(bundle.getString("saveDialogText"));
        	
        	Optional <ButtonType> action =alert.showAndWait();
        	
            if (action.get() == ButtonType.OK) {
            	controller.updateScreens();
            	controller.setScreen(InterfaceGraphique.pause);
            } 
    	}
    }

    /**
     * Set the view during the initialization
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initializeListView();
		
		ListView_SavedGames.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				TextField_nameGame.setText(newValue);
			}
			
		});
	}
	
	/**
	 * Initialize the saved game's listView
	 */
	private void initializeListView() {
	  	File directoryPath = new SavefileManager().getSaveDirectory();
		File[] files=directoryPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".tlv");
			}
		});
		ListView_SavedGames.getItems().clear();
		for (File file : files) {
			ListView_SavedGames.getItems().add(file.getName().replaceFirst("[.][^.]+$", ""));
		}
	}

	/**
	 * Set parent Screen
	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
		
	}

	/**
	 * Update the view
	 */
	@Override
	public void update() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		Button_return.setText(bundle.getString("back"));
		Label_enter_name.setText(bundle.getString("saveName"));
		button_save.setText(bundle.getString("save"));
		initializeListView();
	}

}
