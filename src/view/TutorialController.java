package view;

import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import taluva.Configuration;

public class TutorialController implements ControlledScreen, Initializable{ 
	
	// The ScreenController
	ScreensController controller;
	
	int count =0;
    @FXML
    private Button button_exit;

    @FXML
    private ImageView ImageView_images;

    @FXML
    private Pane Pane_Image;

    @FXML
    private Button button_next;

    @FXML
    private Button button_previous;
    
    @FXML
    private VBox Vbox_image;

    @FXML
    private HBox HBox_buttons;

    @FXML
    private ScrollPane ScrollPane_Image;
    
    final DoubleProperty zoomProperty = new SimpleDoubleProperty(200);
    
    /**
     * Set the screen to main menu view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void backToMenu(ActionEvent event) {
    	// Back to menu
    	controller.setScreen("Menu");
    }
    
    /**
     * Set the previous rules image
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void setPreviousImage(ActionEvent event) {
    	// Previous Image
    	if(count >= 1 && count <= 3) {
    		count = count-1;
    	} else {
    		count = 3;
    	}
    	
    	setImage(count);
		ImageView_images.setFitWidth(800);	
    }

    /**
     * Set the next rules image
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void setNextImage(ActionEvent event) {

    	// Next Image
    	if(count < 3) {
    		count = count+1;
    	} else {
    		count =0;
    	}
    	
    	setImage(count);
    }
    
    /**
     * Change the rules image in function of count
     * @param count the {@link Integer} count of the next image to set
     */
    private void setImage(int count){
    	Configuration c = Configuration.instance();
    	String l = c.lis("Langue");
		if(l.equals("al"))
			l = "en";
		
		InputStream in = Configuration.charge("regles/taluva_"+l+"/"+ count +".png");
		Image i = new Image(in);
		ImageView_images.setImage(i);
    }
    
    /**
	 * Set parent Screen
	 */
    @Override
   	public void setParentScreen(ScreensController page) {
   		this.controller = page;
   	}

    /**
     * Set the view during the initialization
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		setImage(0);
		zoomProperty.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable arg0) {
            	ImageView_images.setFitWidth(zoomProperty.get() * 4);
            	ImageView_images.setFitHeight(zoomProperty.get() * 3);
            }
        });
		
		ScrollPane_Image.addEventFilter(ScrollEvent.ANY, new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                if (event.getDeltaY() > 0) {
                    zoomProperty.set(zoomProperty.get() * 1.1);
                } else if (event.getDeltaY() < 0) {
                    zoomProperty.set(zoomProperty.get() / 1.1);
                }
            }
        });

		ImageView_images.preserveRatioProperty().set(true);
		ScrollPane_Image.setContent(ImageView_images);
	}

	/**
	 * Update the view
	 */
	@Override
	public void update() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Change the language of elements in the view
		button_exit.setText(bundle.getString("back"));	
		
		setImage(0);
	}

}
