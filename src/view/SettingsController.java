package view;
import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import taluva.Configuration;
import javafx.collections.ObservableList;

public class SettingsController implements ControlledScreen, Initializable{

	ScreensController controller;
	
	private Configuration conf;;
	private ResourceBundle bundle;
	
	ObservableList<String> list_langues;
	ObservableList<String> list_help_levels;
	
	Boolean pause = false;

    @FXML
    private Label label_fullscreen;
    
    @FXML
    private Label label_help;
    
    @FXML
    private Label label_soundEffect;

    @FXML
    private Tab label_game;
    
    @FXML
    private Label label_autosave;

    @FXML
    private Label label_langues;

    @FXML
    private Tab label_sound;

    @FXML
    private Label label_misuc;

    @FXML
    private ComboBox<String> langComboBox;

    @FXML
    private Button button_Default;

    @FXML
    private Slider noiseEffectSlider;

    @FXML
    private CheckBox fullScreenBox;

    @FXML
    private ComboBox<String> helpComboBox;

    @FXML
    private Button button_Apply;

    @FXML
    public Button backButton;

    @FXML
    private CheckBox autoSaveBox;

    @FXML
    private Slider musicSlider;

    /**
     * Set the view during the initialization
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		conf = Configuration.instance();
		bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		list_langues = FXCollections.observableArrayList(bundle.getString("fr"),bundle.getString("en"),bundle.getString("al"));
		list_help_levels = FXCollections.observableArrayList(bundle.getString("novice"), bundle.getString("middle"), bundle.getString("expert"), bundle.getString("none"));
		
		/**** Game Tab ****/
		
		// Language
		changeLangue();

		// AutoSave
		if(conf.lis("SaveAuto").equals("true"))
			autoSaveBox.setSelected(true);
		else
			autoSaveBox.setSelected(false);
		
		// FullScreen
		if(conf.lis("Fullscreen").equals("true"))
			fullScreenBox.setSelected(true);
		else
			fullScreenBox.setSelected(false);
		
		/**** Sound Tab ****/
		
		// Music Slider
		musicSlider.setMax(100);
		musicSlider.setMin(0);	
		musicSlider.setValue(Double.parseDouble(conf.lis("Musique")));
		musicSlider.setShowTickLabels(true);
		musicSlider.setShowTickMarks(true);
		
		musicSlider.valueProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				if (InterfaceGraphique.soundAvailable) {
					InterfaceGraphique.music.setVolume(musicSlider.getValue() / 100);
				}
			}
		});
		
		// Noise Effect Slider
		noiseEffectSlider.setValue(Double.parseDouble(conf.lis("EffetSonore")));
		noiseEffectSlider.setShowTickLabels(true);
		noiseEffectSlider.setShowTickMarks(true);
		noiseEffectSlider.setMax(100);
		noiseEffectSlider.setMin(0);
		
		noiseEffectSlider.valueProperty().addListener(new InvalidationListener() {
			
			@Override
			public void invalidated(Observable observable) {
				String musicFile = "music/templeSound.mp3";

				if (InterfaceGraphique.soundAvailable) {					
					Media sound = new Media(getClass().getClassLoader().getResource(musicFile).toString());
					InterfaceGraphique.effects = new MediaPlayer(sound);
					InterfaceGraphique.effects.play();
					
					InterfaceGraphique.effects.setVolume(noiseEffectSlider.getValue() / 100);		
				}
			}
		});
	}

	/**
     * Set the screen to main menu view or pause view in function of {@link Boolean pause}
     * @param event the click {@link ActionEvent event}
     */
	@FXML
	void backToMenu(ActionEvent event) {
		conf = Configuration.instance();
		if (InterfaceGraphique.soundAvailable) {			
			InterfaceGraphique.music.setVolume(Double.parseDouble(conf.lis("Musique")) / 100);		
		}
		if(pause) {
			// Back to pause menu
			controller.setScreen(InterfaceGraphique.pause);
			pause = false;
		} else {
			// Back to menu
			controller.setScreen(InterfaceGraphique.main);
		}
	}
	
	/**
	 * Set ParentScreen
	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
	}

	/**
	 * Allow to restore the defaults settings
	 * @param event the clicked {@link ActionEvent} event
	 */
	@FXML
	void restore_Defaults(ActionEvent event) {

		// Delete the user config file in the home 
		File file = new File(System.getProperty("user.home")+"/"+"newSettings.cfg"); 
		file.delete();

		// Get the default configuration
		conf =  Configuration.instance();
		Configuration.chargerProprietes(conf.getProp(),Configuration.charge("defaut.cfg"), "defaut.cfg");
		bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));

		/**** Game Tab ****/
		
		// Set default language
		changeLangue();

		if(conf.lis("SaveAuto").equals("true"))
			autoSaveBox.setSelected(true);
		else
			autoSaveBox.setSelected(false);
		
		// Set default fullscreen
		if(conf.lis("Fullscreen").equals("true"))
			fullScreenBox.setSelected(true);
		else
			fullScreenBox.setSelected(false);

		InterfaceGraphique.fullscreen();

		/**** Sound Tab ****/
		
		musicSlider.setValue(Double.parseDouble(conf.lis("Musique")));
		noiseEffectSlider.setValue(Double.parseDouble(conf.lis("EffetSonore")));	
		
		InterfaceGraphique.music.setVolume(Double.parseDouble(conf.lis("Musique")) / 100);
		
		
		// Alert to inform the default was restored
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(bundle.getString("toDefaultTitle"));
		alert.setHeaderText(null);
		alert.setContentText(bundle.getString("toDefaultText"));
		
		// update all screen
		controller.updateScreens();
		alert.showAndWait();

	}
	
	/**
	 * Save the settings' change when you clicked on the button_apply
	 * @param event the {@link ActionEvent event}
	 */
	@FXML
	void applyChanges(ActionEvent event) {
		
		conf = Configuration.instance();
		bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));

		/**** Game Tab ****/
		
		// Change Langue
		String langues= langComboBox.getSelectionModel().getSelectedItem();
		String res = "fr";
		if( langues.equals(bundle.getString("en")) )
			res = "en";
		if( langues.equals(bundle.getString("al")) )
			res = "al";
		if( langues.equals(bundle.getString("fr")) )
			res="fr";
		
		conf.changeProperty("Langue",res);
		
		// Change auto save
		if(autoSaveBox.isSelected())
			conf.changeProperty("SaveAuto","true");
		else 
			conf.changeProperty("SaveAuto","false");

		// Change Help
		String help = helpComboBox.getSelectionModel().getSelectedItem();

		if( help.equals(bundle.getString("expert")) ) 
			conf.changeProperty("Aide","expert");
		else if( help.equals(bundle.getString("none")) )
			conf.changeProperty("Aide","none");
		else if( help.equals(bundle.getString("novice")) ) 
			conf.changeProperty("Aide","novice");
		else if( help.equals(bundle.getString("middle")) ) 
			conf.changeProperty("Aide","middle");
		
		if(fullScreenBox.isSelected())
			conf.changeProperty("Fullscreen","true");
		else
			conf.changeProperty("Fullscreen","false");
		InterfaceGraphique.fullscreen();
		
		/**** Sound Tab ****/
		
		conf.changeProperty("Musique",Double.toString(musicSlider.getValue()));
		conf.changeProperty("EffetSonore",Double.toString(noiseEffectSlider.getValue()));
		
		
		// Validation alert
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(bundle.getString("success"));
		alert.setHeaderText(null);
		alert.setContentText(bundle.getString("successContent"));
		alert.showAndWait();
		
		// Update all screen
		controller.updateScreens();
	}
	
	/**
	 * Update the view
	 */
	@Override
	public void update() {
		
		bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		
		// Change the language of elements in the view
		
		// button
		button_Default.setText(bundle.getString("default"));		
	    button_Apply.setText(bundle.getString("apply"));
	    backButton.setText(bundle.getString("back"));
	    
	    // label
	    label_fullscreen.setText(bundle.getString("fullscreen"));
	    label_help.setText(bundle.getString("help"));
	    label_soundEffect.setText(bundle.getString("noise"));
	    label_game.setText(bundle.getString("game"));
	    label_autosave.setText(bundle.getString("autoSave"));
	    label_langues.setText(bundle.getString("lang"));
	    label_sound.setText(bundle.getString("sound"));
	    label_misuc.setText(bundle.getString("music"));	
	    changeLangue();
	}
	
	/**
	 * Change language of {@link ComboBox langComboBox} and {@link ComboBox helpComboBox}
	 */
	private void changeLangue()
	{
		// Set the langComboBox with the selected language
		list_langues = FXCollections.observableArrayList(bundle.getString("fr"),bundle.getString("en"),bundle.getString("al"));
		langComboBox.setItems(list_langues);
		
		// Set the helpComboBox with the selected language
		list_help_levels = FXCollections.observableArrayList(bundle.getString("novice"), bundle.getString("middle"), bundle.getString("expert"), bundle.getString("none"));
		helpComboBox.setItems(list_help_levels);
		
		// Get the selected language
		String language = conf.lis("Langue");
		// In function of the language, select the good value
		if (language.equals("en")) {
			langComboBox.setValue(bundle.getString("en"));
		}
		else if (language.equals("fr")) {
			langComboBox.setValue(bundle.getString("fr"));
		}
		else if (language.equals("al")) {
			langComboBox.setValue(bundle.getString("al"));
		}
		helpComboBox.setValue(bundle.getString(conf.lis("Aide")));
	}

}
