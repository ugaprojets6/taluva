package view;

import java.util.Timer;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class AnnotationBar extends HBox {
	public static enum MessageType{
		ERROR,
		INFO
	}

	private MessageType type;
	private Label annotationBarText;
	private Timer timer = new Timer();
	private boolean alwaysShow;

	private String currentText = "";
	private Timeline timeline;

	/**
	 * 
	 * @param tileMap
	 */
	public AnnotationBar(Pane tileMap) {
		annotationBarText = new Label();
		annotationBarText.setTextFill(Color.WHITE);
		annotationBarText.setFont(Font.font(25));
		annotationBarText.setText(currentText);

		getChildren().add(annotationBarText);

		setAlignment(Pos.CENTER);
		setOpacity(0.3);
		HBox.setHgrow(annotationBarText, Priority.ALWAYS);
		setMinHeight(100);
		prefWidthProperty().bind(tileMap.widthProperty());
		annotationBarText.setWrapText(true);
		setVisible(false);
	}
	
	/**
	 * 
	 * @param temporaryMessage
	 * @param messageType
	 */
	public void setTemporaryMessage(String temporaryMessage, MessageType messageType) {
		if (timeline != null) {
			timeline.stop();
		}
		timeline = new Timeline(new KeyFrame(
				Duration.millis(5000),
				ae -> {
					setMessage(currentText, MessageType.INFO);
					timeline = null;
				}
				));
		setMessage(temporaryMessage, messageType);
		timeline.play();
	}

	/**
	 * 
	 * @return
	 */
	public boolean isAlwaysShow() {
		return alwaysShow;
	}

	/**
	 * 
	 * @param alwaysShow
	 */
	public void setAlwaysShow(boolean alwaysShow) {
		this.alwaysShow = alwaysShow;
		setVisible(alwaysShow);
	}

	/**
	 * 
	 * @return
	 */
	public MessageType getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(MessageType type) {
		this.type = type;
	}

	/**
	 * 
	 * @return
	 */
	public String getMessage() {
		return annotationBarText.getText();
	}

	/**
	 * 
	 * @param message
	 * @param type
	 */
	public void setMessage(String message,MessageType type) {
		this.type = type;
		switch (type) {
		case ERROR:
			setStyle("-fx-background-color: red");
			annotationBarText.setTextFill(Color.WHITE);
			break;

		case INFO:
			setStyle("-fx-background-color: white");
			annotationBarText.setTextFill(Color.BLACK);
			break;

		default:
			break;
		}

		annotationBarText.setText(message);
		if (type == MessageType.INFO)
			currentText = message;
		setVisible(true);
	}

	/**
	 * 
	 * @return
	 */
	public Timer getTimer() {
		return timer;
	}

	/**
	 * 
	 * @param timer
	 */
	public void setTimer(Timer timer) {
		this.timer = timer;
	}
}
