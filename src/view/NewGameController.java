package view;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import taluva.Configuration;


public class NewGameController implements ControlledScreen{

	ScreensController controller;
	
    @FXML
    private Button backButton;
    @FXML
    private Button localButton;

    /**
     * Set the screen to local game view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void goToLocalGame(ActionEvent event) {
    	// Go to local game
    	controller.setScreen(InterfaceGraphique.localGame);
    }

    /**
     * Set the screen to main menu view
     * @param event the click {@link ActionEvent event}
     */
    @FXML
    void backToMenu(ActionEvent event) {
    	// Back to menu
    	controller.setScreen(InterfaceGraphique.main);
    }
	
    /**
	 * Set parent Screen
	 */
	@Override
	public void setParentScreen(ScreensController page) {
		this.controller = page;
	}
	
	/**
	 * Update the view
	 */
	@Override
	public void update() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));

		// Change the language of elements in the view
		backButton.setText(bundle.getString("back"));
		localButton.setText(bundle.getString("local"));
	}
}
