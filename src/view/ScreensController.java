package view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import taluva.Configuration;

public class ScreensController extends StackPane {

	private HashMap<String, Node> screens = new HashMap<String, Node>();
	private HashMap<String, ControlledScreen> controllerScreen = new HashMap<String, ControlledScreen>();

	public ControlledScreen getControllerScreen(String name) {
		return controllerScreen.get(name);
	}
	
	private Iterator<ControlledScreen> getScreenIterator() {
		return controllerScreen.values().iterator();
	}
	
	public void updateScreens() {
		Iterator<ControlledScreen> screens = getScreenIterator();
		
		while(screens.hasNext()) {
			screens.next().update();
		}
	}

	public void addControllerScreen(String name, ControlledScreen controller) {
		controllerScreen.put(name, controller);
	}

	public ScreensController() {
		super();
	}

	public Node getScreens(String name) {
		return screens.get(name);
	}

	public void addScreen(String name, Node screen) {
		screens.put(name, screen);
	}

	public boolean loadScreen(String name) {
		Configuration config = Configuration.instance();
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(config.lis("Langue")));
			FXMLLoader myLoad = new FXMLLoader(getClass().getResource("/view/"+name+".fxml"), bundle);
			Parent loadScreen = myLoad.load();
			ControlledScreen myScreenC = ((ControlledScreen) myLoad.getController());
			addControllerScreen(name, myScreenC);
			myScreenC.setParentScreen(this);
			addScreen(name, loadScreen);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean setScreen(final String name) {
		if (screens.get(name) != null) {   //screen loaded
			final DoubleProperty opacity = opacityProperty();

			if (!getChildren().isEmpty()) {    //if there is more than one screen
				Timeline fade = new Timeline(
						new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
						new KeyFrame(new Duration(500), new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent t) {
								getChildren().remove(0);                    //remove the displayed screen
								getChildren().add(0, screens.get(name));     //add the screen
								Timeline fadeIn = new Timeline(
										new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
										new KeyFrame(new Duration(200), new KeyValue(opacity, 1.0)));
								fadeIn.play();
							}
						}, new KeyValue(opacity, 0.0)));
				fade.play();

			} else {
				setOpacity(0.0);
				getChildren().add(screens.get(name));       //no one else been displayed, then just show
				Timeline fadeIn = new Timeline(
						new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
						new KeyFrame(new Duration(1000), new KeyValue(opacity, 1.0)));
				fadeIn.play();
			}
			return true;
		} else {
			return false;
		}

	}


	public boolean unloadScreen(String name) {
		if(screens.remove(name) == null) {
			return false;
		} else {
			return true;
		}
	}

}
