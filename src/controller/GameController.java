package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Platform;
import model.Board;
import model.Board.ReasonBuilding;
import model.Building;
import model.PieceDeck;
import model.PiecePoint;
import model.SavefileManager;
import model.Tile;
import view.AnnotationBar.MessageType;
import view.GameViewController;
import model.Building.BuildingType;
import taluva.Configuration;
import model.BuildingPoint;
import model.City;
import model.CubePoint;
import model.FeedforwardType;

/*
 * Controller of the game
 */
public class GameController implements Serializable,Cloneable {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4337760526725597481L;

	private List<Player> players;
	private transient GameViewController view;
	public int currentPlayer;
	private PieceDeck deck;
	public Board board;
	public int phase;
	public boolean gameEnded;
	public String winnerText;
	private String saveName;
	public Player winner;

	/*
	 * 
	 */
	public GameController(List<Player> players, GameViewController view) {
		this.players = players;
		this.currentPlayer = 0;
		this.phase = 1;
		this.gameEnded = false;
		this.winnerText = "";
		this.saveName = null;

		this.deck = new PieceDeck(PieceDeck.getNumberPiecesFromPlayers(players.size()), false);
		board = new Board();
		board = new Board();
		for (Player player : players) {
			player.setGameController(this);
		}

		this.view = view;
		if(view != null) {
			view.setBoard(board);
			updateView();
		}
	}

	public void setSaveName(String value) {
		saveName = value;
	}

	public String getSaveName() {
		return saveName;
	}

	public void generateTransientData(GameViewController view) {
		this.view = view;
		for (City c : board.cities) {
			for (BuildingPoint bp : c.getBuildingPoints()) {
				bp.generateColor();
			}
		}

		for (PiecePoint p : board.drawablePieces) {
			for (int i = 0; i < 3; i++) {
				Tile t = p.tiles[i];
				t.generateBuildingImage();

				t.setOnMouseClicked(ev -> {
					selectPointAction(t);
				});
				t.getBuildingImage().setOnMouseClicked(ev -> {
					selectPointAction(t);
				});
			}
			p.updateTiles();
		}

		for (Enumeration<CubePoint>e = board.tiles.keys(); e.hasMoreElements();) {
			CubePoint p = e.nextElement();
			Tile t = board.tiles.get(p);
			t.generateBuildingImage();

			t.setOnMouseClicked(ev -> {
				selectPointAction(t);
			});
			t.getBuildingImage().setOnMouseClicked(ev -> {
				selectPointAction(t);
			});
		}

		for (Enumeration<CubePoint>e = board.pieces.keys(); e.hasMoreElements();) {
			CubePoint point = e.nextElement();
			PiecePoint piecepoint = board.pieces.get(point);
			for (int i = 0; i < 3; i++) {
				Tile t = piecepoint.tiles[i];
				t.generateBuildingImage();
				t.setOnMouseClicked(ev -> {
					selectPointAction(t);
				});
				t.getBuildingImage().setOnMouseClicked(ev -> {
					selectPointAction(t);
				});
			}
		}
		deck.generateSizeProperty();
		board.generateTransient();

		for (Player p : players) {
			p.getHand().generateProperties();
		}
		if(view!=null)
			view.setBoard(board);
	}

	public PieceDeck getDeck() {
		return deck;
	}

	public GameViewController getView() {
		return view;
	}

	public void resetFeedforward() {
		Iterator<Tile> it = board.getIterator();
		while(it.hasNext()) {
			it.next().setFeedforward(FeedforwardType.NONE);
		}
	}

	public void startPlaying() {
		players.get(currentPlayer).startTurn();
	}

	public void resumePlaying() {
		getCurrentPlayer().endTurn();
		if (phase == 1) {
			getCurrentPlayer().startTurn();
		} else if (phase == 2) {
			getCurrentPlayer().placeBuildings();
			showHelp();
		}
		if(view!=null)
			getView().highlightToolbox();
		updateView();
	}

	public Player getCurrentPlayer() {
		if (players == null || players.size() == 0)
			return null;
		return players.get(currentPlayer);
	}

	public int getCurrentPlayernumber() {
		if (players == null || players.size() == 0)
			return -1;
		return currentPlayer;
	}

	private void nextPlayer() {
		currentPlayer++;
		currentPlayer %= players.size();
		if(view!=null)
			view.nextPlayer();
		phase=0;
		nextPhase();
	}

	public List<Player> getPlayers() {
		return players;
	}

	public boolean canDraw() {
		return players.get(currentPlayer).currentPiece == null && phase == 1;
	}

	public boolean canPlaceCurrentBuilding() {
		BuildingPoint bp = getCurrentPlayer().getCurrentBuilding();

		if (bp == null) {
			return false;
		}

		if (board.getTile(bp.point) == null) {
			return false;
		}

		if (getCurrentPlayer().getCurrentCity() != null) {
			return board.canExtendCity(bp) != null;
		}

		switch(bp.getType()) {
		case HUTT:
			return (board.canPlaceHutt(bp) == ReasonBuilding.OK);
		case TEMPLE:
			return (board.canPlaceTemple(bp) == ReasonBuilding.OK);
		case TOWER:
			return (board.canPlaceTower(bp) == ReasonBuilding.OK);
		default:
			return false;

		}
	}

	public void setFeedforward(CubePoint point, FeedforwardType type) {
		if(!(Configuration.instance().lis("Aide").equals("none"))) {
			Tile t = board.getTile(point);
			if (t != null)
				t.setFeedforward(type);
		}
	}

	/*
	 * 
	 */
	public boolean endGameCondition(List<Player> players,int currentPlayer,PieceDeck deck,int phase) {
		//show winner depending on the condition
		//win by having finished 2 of the buildings
		if(players.get(currentPlayer).getHand().hasWon()) {
			winnerText = players.get(currentPlayer).name+" won by buildings";
			this.winner = players.get(currentPlayer);
			return true;
		}
		if (phase == 2) {
			//win by emptying deck
			if(deck.isEmpty()) {
				for (Player player : players) {
					int winCount = 0;
					for (Player player2 : players) {
						if(!player.equals(player2)) {
							if(player.compareTo(player2) == 1) {
								winCount++;
							}
						}
					}

					if(winCount == players.size()-1) {
						winnerText = player.name+" won by score";
						this.winner = player;
						return true;
					}else {
						if(winCount == 0) {
							winnerText = "Draw by score";
						}
					}
				}
				return true;
			}
		} else if (phase == 1) {
			// win by checkmate ( no possible move from all the players )
			int playersLost = 0;
			Player winner = null;
			for (Player player : players) {
				if(player.hasLost()) {
					playersLost++;
				}else {
					winner = player;
				}
			}
			if(playersLost == players.size()-1) {
				this.winner = winner;
				winnerText = winner.name+" won by domination";
				return true;
			}else if(playersLost == players.size()){
				winnerText = "Draw by domination";
				return true;
			}
		}
		return false;
	}

	public void showHelp() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		if (Configuration.instance().lis("Aide").equals("novice")) {
			if(view==null) {
				return;
			}
			if (phase == 1)
				getView().setAnnotationText(bundle.getString("beginnerDraw"), MessageType.INFO);
			if (phase == 2)
				getView().setAnnotationText(bundle.getString("beginnerBuilding"), MessageType.INFO);
		}
	}

		/*
		 * Plays the next phase of the game
		 */
		public void nextPhase() {
			if(endGameCondition(players,currentPlayer,deck,phase)) {
				//game has finished
				if(view!=null)
					getView().setAnnotationText(winnerText+"\nGame has finished", MessageType.INFO);
				gameEnded = true;
				updateView();
				return;
			}else {
				resetFeedforward();
				if(phase == 0) {
					phase = 1;
					SavefileManager sm = new SavefileManager();
					if (Configuration.instance().lis("SaveAuto").equals("true")) {
						sm.automaticSave(this);
						if(view!=null)
							getView().controller.updateScreens();
					}
					sm.saveSnapshot(this);

					players.get(currentPlayer).startTurn();


					updateView();
				}else if(phase == 1) {
					phase = 2;
					if (getCurrentPlayer().hasLost()) {
						getCurrentPlayer().endTurn();
						nextPlayer();
					} else {
						players.get(currentPlayer).endTurn();

						SavefileManager sm = new SavefileManager();
						sm.saveSnapshot(this);

						players.get(currentPlayer).placeBuildings();
						showHelp();
						updateView();
					}
				}else {
					players.get(currentPlayer).endTurn();
					nextPlayer();
				}
			}		
		}

		/*
		 * 
		 */
		public void drawPieceAction() {
			if(!gameEnded) {
				players.get(currentPlayer).setCurrentPiece(deck.drawPiece());
				if(getView()!=null)
					view.setPieceInHand(players.get(currentPlayer).getCurrentPiecePoint());
				updateView();
			}
			if(view!=null)
				getView().highlightToolbox();
		}

		/*
		 * chose building
		 */
		public void selectBuildingHuttAction() {
			if(!gameEnded) {
				if(phase==2) {
					resetFeedforward();
					if (getCurrentPlayer().getCurrentBuilding() != null && getCurrentPlayer().getCurrentBuilding().getType() == BuildingType.HUTT) {
						getCurrentPlayer().unselectCurrentBuilding();
					} else {			
						if (getCurrentPlayer().getCurrentCity() != null) {
							getView().unHighlightCity(getCurrentPlayer().getCurrentCity());
							getCurrentPlayer().unselectCurrentCity();
						}
						players.get(currentPlayer).setCurrentBuilding(
								new Building(
										BuildingType.HUTT,
										players.get(currentPlayer).getColor(), 
										getCurrentPlayer().getColorName()));
					}
				}
			}
			if(view!=null)
				getView().highlightToolbox();
		}

		public void selectBuildingTempleAction() {
			if(!gameEnded) {
				if(phase==2) {
					resetFeedforward();
					if (getCurrentPlayer().getCurrentBuilding() != null && getCurrentPlayer().getCurrentBuilding().getType() == BuildingType.TEMPLE) {
						getCurrentPlayer().unselectCurrentBuilding();
					} else {
						if (getCurrentPlayer().getCurrentCity() != null) {
							getView().unHighlightCity(getCurrentPlayer().getCurrentCity());
							getCurrentPlayer().unselectCurrentCity();
						}
						players.get(currentPlayer).setCurrentBuilding(new Building(BuildingType.TEMPLE,players.get(currentPlayer).getColor(),getCurrentPlayer().getColorName()));
					}
				}
			}
			if(view!=null)
				getView().highlightToolbox();
		}

		public void selectBuildingTowerAction() {
			if(!gameEnded) {
				if(phase==2) {
					resetFeedforward();
					if ( getCurrentPlayer().getCurrentBuilding() != null &&getCurrentPlayer().getCurrentBuilding().getType() == BuildingType.TOWER) {
						getCurrentPlayer().unselectCurrentBuilding();
					} else {
						if (getCurrentPlayer().getCurrentCity() != null) {
							if(view!=null)
								getView().unHighlightCity(getCurrentPlayer().getCurrentCity());
							getCurrentPlayer().unselectCurrentCity();
						}
						players.get(currentPlayer).setCurrentBuilding(new Building(BuildingType.TOWER,players.get(currentPlayer).getColor(),getCurrentPlayer().getColorName()));
					}
				}
			}
			if(view!=null)
				getView().highlightToolbox();
		}

		public void handleCitySelection(Tile selectedTile) {
			// If the player selects a city, we need to check that a city was not
			// selected before. If city was null, set the current city to the one that
			// was selected. If the city was already selected, we need to check some things :
			// - If the player clicked on the same city, unselect it
			// - If the player clicked on another of their city, select it instead
			// - If the player clicked on a valid tile for expansion, expand it
			// - If the player clicked on a miscellaneous tile, unselect the city

			if (selectedTile == null) {
				return;
			}

			City currentCity = getCurrentPlayer().getCurrentCity();

			CubePoint selectedPoint = CubePoint.fromAxial(selectedTile.getX(), selectedTile.getY());
			BuildingPoint selectedBuildingPoint = new BuildingPoint(
					BuildingType.HUTT, 
					getCurrentPlayer().getColor(), 
					selectedPoint,getCurrentPlayer().getColorName());
			City selectedCity = board.getCityFromBuildingPoint(selectedBuildingPoint);

			if (selectedCity != null) {
				if(currentCity != null) {
					if (selectedCity == currentCity) {
						getCurrentPlayer().unselectCurrentCity();
						if(view!=null)
							getView().unHighlightCity(currentCity);
					} else {					
						getCurrentPlayer().unselectCurrentCity();
						if(view!=null)
							getView().unHighlightCity(currentCity);
						getCurrentPlayer().setCurrentCity(selectedBuildingPoint);
						if(view!=null)
							view.highlightCity(selectedCity);
					}

				}else {
					getCurrentPlayer().setCurrentCity(selectedBuildingPoint);
					if(view!=null)
						view.highlightCity(selectedCity);
				}			
			} else if (selectedCity == null && currentCity != null) {
				if (board.canExtendCity(selectedBuildingPoint) != null) {
					getCurrentPlayer().setCurrentBuilding(new Building(BuildingType.HUTT,players.get(currentPlayer).getColor(),getCurrentPlayer().getColorName()));
					getCurrentPlayer().getCurrentBuilding().point = selectedPoint;
					getCurrentPlayer().placeBuildings();
					getCurrentPlayer().unselectCurrentCity();
					if(view!=null)
						getView().unHighlightCity(currentCity);
				} else {
					getCurrentPlayer().unselectCurrentCity();
					if(view!=null)
						getView().unHighlightCity(currentCity);
				}
			}
			updateView();
		}

		/*
		 * 
		 */
		public void selectPointAction(Tile clickedTile) {
			if(!gameEnded) {
				if(phase==1) {
					if(players.get(currentPlayer).getCurrentPiecePoint()!=null) {
						players.get(currentPlayer).placePieces();
					}
				}else {
					BuildingPoint currentBuildingPoint = getCurrentPlayer().getCurrentBuilding();
					if (currentBuildingPoint == null) {
						handleCitySelection(clickedTile);
					} else {
						players.get(currentPlayer).placeBuildings();
					}
				}
				updateView();
			}
			if(view!=null)
				getView().highlightToolbox();
		}

		/*
		 * 
		 */
		public void turnCurrentPieceAction() {
			if(!gameEnded) {
				if(phase==1) {
					if(players.get(currentPlayer).getCurrentPiecePoint()!=null) {
						players.get(currentPlayer).getCurrentPiecePoint().turnPoint();
						for (Tile t : players.get(currentPlayer).getCurrentPiecePoint().tiles) {
							t.rotateCounterClockwise();
						}
					}
				}
			}
		}

		public void updateView() {
			if(view==null) {
				return;
			}
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					view.update();
				}

			});
		}

		public Object clone() {
			GameController o = null;
			try {
				// On r�cup�re l'instance � renvoyer par l'appel de la 
				// m�thode super.clone()
				o = (GameController) super.clone();

				o.board=this.board.getConfiguration();
				o.currentPlayer=this.currentPlayer;
				o.deck=(PieceDeck) this.deck.clone();
				o.phase=this.phase;
				o.players=new ArrayList<Player>();
				for (int i=0;i<this.players.size();i++)
				{
					Player p = (Player)this.players.get(i).clone();
					p.getHand().setBoard(o.board);
					o.players.add(p);
				}

				o.gameEnded=this.gameEnded;
				o.winner=this.winner;
				o.view=null;

			} catch(CloneNotSupportedException cnse) {
				// Ne devrait jamais arriver car nous impl�mentons 
				// l'interface Cloneable
				cnse.printStackTrace(System.err);
			}
			// on renvoie le clone
			return o;
		}
	}
