package controller;

public interface Playable {
	/*
	 * initiate elements to start a turn
	 */
	void startTurn();
	/*
	 * play phase 1, place a piece on the grid
	 */
	void placePieces();
	/*
	 * play phase 2, place buildings according to a type
	 */
	void placeBuildings();
	/*
	 * end initialize elements
	 */
	void endTurn();
}
