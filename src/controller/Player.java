package controller;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.scene.paint.Color;
import model.Board;
import model.Building;
import model.Building.BuildingType;
import model.BuildingPoint;
import model.City;
import model.CubePoint;
import model.FeedforwardType;
import model.Hand;
import model.Piece;
import model.PiecePoint;
import model.Tile;
import taluva.Configuration;
import view.AnnotationBar.MessageType;
import view.InterfaceGraphique;
import model.Board.ReasonBuilding;
import model.Board.ReasonPiece;

public abstract class Player implements Playable, Comparable<Player>, Serializable,Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8308261387745210914L;
	protected Hand hand;
	protected String name;
	protected GameController gc;
	protected PiecePoint currentPiece;
	protected BuildingPoint currentBuilding;
	protected City currentCity;
	protected boolean hasLost;
	protected String colorName;
	
	/*
	 * Compares players to see who wins by returning 1, 0 if draw, -1 if lose
	 */
	@Override
	public int compareTo(Player o) {
		int myTemples = getHand().getTemples();
		int myTowers = getHand().getTowers();
		int myHutts = getHand().getHutts();
		int othersTemples = o.getHand().getTemples();
		int othersTowers = o.getHand().getTowers();
		int othersHutts = o.getHand().getHutts();
		
		if (myTemples == othersTemples) {
			if (myTowers == othersTowers) {
				if (myHutts == othersHutts) {
					return 0;
				} else if (myHutts < othersHutts) {
					return 1;
				} else {
					return -1;
				}
			} else if (myTowers < othersTowers) {
				return 1;
			} else {
				return -1;
			}
		} else if (myTemples < othersTemples) {
			return 1;
		} else {
			return -1;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Player)) {
			return false;
		}
		Player p = (Player)obj;
		return (getColor() == p.getColor());
	}
	
	public void setHand(Hand h) {
		hand = h;
	}
	
	public Player(String name, Color color, String colorName) {
		setName(name);
		this.colorName = colorName;
		hand = new Hand(color, colorName);
	}
	
	public void setCurrentPiece(Piece p) {
		currentPiece = new PiecePoint(p);
	}
	
	public PiecePoint getCurrentPiecePoint() {
		return currentPiece;
	}
	
	public void setCurrentBuilding(Building b) {
		currentBuilding = new BuildingPoint(b,new CubePoint());
	}
	
	public BuildingPoint getCurrentBuilding() {
		return currentBuilding;
	}
	
	public void setCurrentCity(BuildingPoint b) {
		for (City city : gc.board.getCitiesOfColor(b.getColor())) {
			if(city.getBuildingPoints().contains(b)) {
				currentCity = city;
				return;
			}
		}
	}
	
	public void unselectCurrentCity() {
		currentCity = null;
	}
	
	public void unselectCurrentBuilding() {
		currentBuilding = null;
	}
	
	public City getCurrentCity() {
		return currentCity;
	}
	
	public void setGameController(GameController gc) {
		this.gc = gc;
		this.hand.setBoard(gc.board);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getColor() {
		return hand.getColor();
	}

	public void setColor(Color color) {
		this.hand.setColor(color);
	}
	

	public Hand getHand() {
		return hand;
	}
	
	@Override
	public void startTurn() {
		currentPiece = null;
		currentBuilding = null;
		currentCity = null;
		InterfaceGraphique.playSounds = false;
		if(gc.getView()!=null)
			gc.getView().setAnnotationText(getName()+"'s turn",MessageType.INFO);
	}

	@Override
	public void placePieces() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		if(currentPiece != null) {
			Board.ReasonPiece result = gc.board.addPiece(currentPiece);
			if(result == ReasonPiece.OK) {	
				for (Tile t : currentPiece.tiles) {
					t.setOnMouseClicked(ev -> {
						gc.selectPointAction(t);
					});
					t.getBuildingImage().setOnMouseClicked(ev -> {
						gc.selectPointAction(t);
					});
				}
				currentPiece.setFeedforward(FeedforwardType.NONE);
				currentPiece = null;
				gc.nextPhase();
			}
			else {
				if(gc.getView()!=null)
					gc.getView().setAnnotationText(bundle.getString("wrongPlace")+bundle.getString(result.toString()),MessageType.ERROR);
			}
		}else {
			if(gc.getView()!=null)
				gc.getView().setAnnotationText(bundle.getString("before"),MessageType.ERROR);
		}
	}

	@Override
	public void placeBuildings() {
		Configuration conf = Configuration.instance();
		ResourceBundle bundle = ResourceBundle.getBundle("lang.UIResources",new Locale(conf.lis("Langue")));
		if(currentBuilding != null) {
			Tile t = gc.board.getTile(currentBuilding.point);
			if(t!=null) {
				if(hand.hasEnough(currentBuilding,currentCity)) {
					int buildingToDelete = 1;
					if(currentBuilding.getType() == BuildingType.HUTT) {
						if(currentCity!=null)
							buildingToDelete = hand.board.countExtendCityHutts(currentBuilding,currentCity);
					}
					ReasonBuilding result = gc.board.addBuilding(currentBuilding,currentCity);
					if(result == ReasonBuilding.OK) {
						hand.deleteBuildings(currentBuilding.getType(), buildingToDelete);
						gc.nextPhase();
					}
					else {
						if(gc.getView()!=null)
							gc.getView().setAnnotationText(bundle.getString("wrongBuilding")+bundle.getString(result.toString()),MessageType.ERROR);
					}
				}else {
					if(gc.getView()!=null)
						gc.getView().setAnnotationText(bundle.getString("notEnoughtPiece"),MessageType.ERROR);
				}
			}else {
				if(gc.getView()!=null)
					gc.getView().setAnnotationText(bundle.getString("tileSelected"),MessageType.ERROR);
			}
		}
	}
	
	public boolean hasLost() {
		if(!hasLost) {
			if(hand.hasLost()) {
				hasLost = true;
				return hasLost;
			}
		}
		return hasLost;
	}
	
	public String getColorName() {
		return colorName;
	}

	public void setHasLost(boolean value) {
		hasLost = value;
	}

	@Override
	public void endTurn() {
		currentPiece = null;
		currentBuilding = null;
		currentCity = null;
	}
	
	
	public Object clone() {
		Player o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (Player) super.clone();
			
			o.hand=(Hand) this.hand.clone();
			o.hasLost=this.hasLost;
			
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
}
