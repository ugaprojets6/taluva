package controller;

import javafx.scene.paint.Color;
import view.InterfaceGraphique;

public class HumanPlayer extends Player{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6049487183701710167L;

	public HumanPlayer(String name, Color color, String colorName) {
		super(name, color, colorName);
	}

	@Override
	public void startTurn() {
		super.startTurn();
		InterfaceGraphique.playSounds = true;
		gc.showHelp();
	}

	@Override
	public void placePieces() {
		super.placePieces();
	}

	@Override
	public void placeBuildings() {
		super.placeBuildings();
	}

	@Override
	public void endTurn() {
		super.endTurn();
	}
}
